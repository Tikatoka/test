package com.thehutgroup.accelerator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void HashPasswordTest()
    {
        String password = "HelloWorld";
        String hash = HashPassword.getHash(password);
        assertTrue(HashPassword.verify(hash, password));
        String wrongHash = hash + "1";
        assertTrue(!HashPassword.verify(wrongHash, password));
    }

    @Test
    public void PriorityQueueTest(){
        Student tom = new Student("tom", "unkown", 1.1f, 5.5f);
        Student tony = new Student("tony", "unkown", 2.1f, 4.5f);
        Student mike = new Student("mike", "unkown", 3.1f, 3.5f);
        Student jame = new Student("jame", "unkown", 4.1f, 2.5f);
        Student jack = new Student("jack", "unkown", 5.1f, 1.5f);

        PriorityQueue<Student> queueA = new PriorityQueue<>();
        ArrayList<Student> list = new ArrayList<>(Arrays.asList(tom, tony, mike, jame, jack));
        for(Student one: list){
            queueA.add(one, one.getCurrentGPA());
        }
        PriorityElement<Student> first = queueA.peek();
        assertEquals(first.getElement().getName(), "jack");
        first = queueA.peek();
        assertEquals(first.getElement().getName(), "jack");

        first = queueA.poll();
        assertEquals(first.getElement().getName(), "jack");

        first = queueA.poll();
        assertEquals(first.getElement().getName(), "jame");
    }
}
