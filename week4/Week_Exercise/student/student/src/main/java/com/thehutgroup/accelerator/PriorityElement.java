package com.thehutgroup.accelerator;

public class PriorityElement<T> {
  private T element;

  public T getElement() {
    return element;
  }

  public double getPriority() {
    return priority;
  }

  private double priority;
  public PriorityElement(T originElement, double priority) {
    this.element = originElement;
    this.priority = priority;
  }
}
