package com.thehutgroup.accelerator;

public class Student {
  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  private String name;
  private String address;
  private float cumulativeGPA;
  private float currentGPA;

  public float getCumulativeGPA() {
    return cumulativeGPA;
  }

  public void setCumulativeGPA(float cumulativeGPA) {
    this.cumulativeGPA = cumulativeGPA;
  }

  public float getCurrentGPA() {
    return currentGPA;
  }

  public void setCurrentGPA(float currentGPA) {
    this.currentGPA = currentGPA;
  }

  public Student(String name, String address, float cumulativeGPA, float currentGPA) {
    this.name = name;
    this.address = address;
    this.cumulativeGPA = cumulativeGPA;
    this.currentGPA = currentGPA;
  }
}
