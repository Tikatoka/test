package com.thehutgroup.accelerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PriorityQueue <T>{
  private List<PriorityElement<T>> list = new ArrayList<>();


  public PriorityQueue(){
  }

  public void add(T element, double priorityValue){
    PriorityElement pe = new PriorityElement<>(element, priorityValue);
    list.add(pe);
    Collections.sort(list, new Compar());
  }

  public PriorityElement<T> peek(){
    if (list.isEmpty()){
      System.out.println("The queue is empty");
      return null;
    } else {
      return list.get(0);
    }
  }

  public PriorityElement<T> poll(){
    if (list.isEmpty()){
      System.out.println("The queue is empty");
      return null;
    }
    return list.remove(0);
  }

  class Compar implements Comparator<PriorityElement>
  {
    // Used for sorting in ascending order of
    // roll number
    public int compare(PriorityElement a, PriorityElement b)
    {
      if (a.getPriority() > b.getPriority()) return 1;
      if (a.getPriority() == b.getPriority()) return 0;
      return -1;
    }
  }
}

// change to logn insertion implement