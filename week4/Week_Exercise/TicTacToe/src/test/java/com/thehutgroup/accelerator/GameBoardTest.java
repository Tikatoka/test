package com.thehutgroup.accelerator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameBoardTest {

  @Test
  public void peekPositionTest() {
    GameBoard board = new GameBoard();
    assertEquals("Initial map, should has all zeros. Peek by coordinates", Side.Empty, board.peekPosition(1, 1));
    assertEquals("Out of range coordinates. Return null", null, board.peekPosition(4, 1));
    Position p_1 = new Position(1, 1);
    assertEquals("Initial map, should has all zeros. Peek by position", Side.Empty, board.peekPosition(p_1));
  }

  @Test
  public void updateMapTest() {
    GameBoard board = new GameBoard();
    Position p_1 = new Position(1, 1);
    board.updateMap(p_1, Side.Cross);
    assertEquals("Update position 1,1 to 1", Side.Cross, board.peekPosition(p_1));
  }
}
