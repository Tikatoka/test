package com.thehutgroup.accelerator.Players;

import com.thehutgroup.accelerator.GameManager.SaveManager;
import com.thehutgroup.accelerator.Side;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PlayerFactory will return a player object by given token
 */
public class PlayerFactory {
  private static final Logger log = LoggerFactory.getLogger(PlayerFactory.class);

  /**
   * getPlayerFromString generates a player object by string
   *
   * @param token the given string
   * @param side  the given side
   * @return a Player object
   */
  public static Player getPlayerFromString(String token, Side side) {
    if (token.contains("Human")) {
      return new HumanPlayer(side);
    }
    if (token.contains("AI")) {
      try {
        float difficulty = Float.valueOf(token.substring(2));
        return new AIPlayer(side, difficulty);
      } catch (Exception e) {
        log.warn("Can restore an AI player with token:" + token, e);
        System.out.println("Invalid token!");
        return new AIPlayer(side, 1);
      }
    }
    return null;
  }
}

// player factory functionality should be in this class. to avoid cross - class
//no under sty
// make a map for testing
// more tests
