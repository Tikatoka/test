package com.thehutgroup.accelerator.GameManager;

import com.thehutgroup.accelerator.GameBoard;
import com.thehutgroup.accelerator.Players.Player;
import com.thehutgroup.accelerator.Side;

/**
 * GameProcess class defines the state of a whole game.
 * It contains: the game record of cross win V.S. circle win,
 *              the turn count,
 *              the current board,
 *              the cross player and circle play.
 */
public class GameProcess {
  public GameBoard getBoard() {
    return board;
  }

  public int getCrossWin() {
    return crossWin;
  }

  public int getCircleWin() {
    return circleWin;
  }

  public Side getTurn() {
    return turn;
  }

  private GameBoard board;
  private int crossWin;
  private int circleWin;
  private Side turn;
  private String crossPlayer;

  public String getCrossPlayer() {
    return crossPlayer;
  }

  public String getCirclePlayer() {
    return circlePlayer;
  }

  private String circlePlayer;

  public static class Builder {
    private GameBoard board;
    private int crossWin;
    private int circleWin;
    private float difficulty;
    private Side turn;
    private String crossPlayer;
    private String circlePlayer;

    public Builder() {
    }

    public Builder board(GameBoard board) {
      this.board = board;
      return this;
    }

    public Builder crossWin(int crossWin) {
      this.crossWin = crossWin;
      return this;
    }

    public Builder circlePlayer(String player) {
      this.circlePlayer = player;
      return this;
    }

    public Builder crossPlayer(String player) {
      this.crossPlayer = player;
      return this;
    }

    public Builder circleWin(int circleWin) {
      this.circleWin = circleWin;
      return this;
    }

    public Builder turn(Side turn) {
      this.turn = turn;
      return this;
    }

    public GameProcess build() {
      return new GameProcess(this);
    }
  }

  private GameProcess(Builder builder) {
    this.board = builder.board;
    this.crossWin = builder.crossWin;
    this.circleWin = builder.circleWin;
    this.turn = builder.turn;
    this.crossPlayer = builder.crossPlayer;
    this.circlePlayer = builder.circlePlayer;
  }
}
