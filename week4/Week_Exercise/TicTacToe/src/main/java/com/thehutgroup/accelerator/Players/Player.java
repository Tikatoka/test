package com.thehutgroup.accelerator.Players;

import com.thehutgroup.accelerator.GameBoard;
import com.thehutgroup.accelerator.Position;

public interface Player {
  public Position makeOneMove(GameBoard board);
  public String saveToString();
}
