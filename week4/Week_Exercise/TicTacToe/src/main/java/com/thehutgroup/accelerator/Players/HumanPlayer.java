package com.thehutgroup.accelerator.Players;

import com.thehutgroup.accelerator.GameBoard;
import com.thehutgroup.accelerator.GameManager.GameRender;
import com.thehutgroup.accelerator.Players.Player;
import com.thehutgroup.accelerator.Position;
import com.thehutgroup.accelerator.Side;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * A human console player
 */
public class HumanPlayer implements Player {
  private Side id;
  private static final Logger log = LoggerFactory.getLogger(AIPlayer.class);

  public HumanPlayer(Side id) {
    this.id = id;
  }

  public String saveToString() {
    return "Human";
  }

  /**
   * makeOneMove will render the board on console and ask player to enter a move
   *
   * @param board the current board
   * @return a position specified by player
   */
  @Override
  public Position makeOneMove(GameBoard board) {
    GameRender.printBoard(board);
    System.out.print("\nPlease select one position for your move:");
    do {
      try {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        String[] choice = s.split(" ");
        int x = Integer.parseInt(choice[0]);
        int y = Integer.parseInt(choice[1]);
        return new Position(x, y);
      } catch (Exception e) {
        log.debug("Human player with side " + id + " makes an invalid input");
        System.out.print("Invalid input! Please try again:");
      }
    } while (true);
  }

}
