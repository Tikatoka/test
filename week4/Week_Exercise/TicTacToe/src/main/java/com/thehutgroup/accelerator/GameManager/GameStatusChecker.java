package com.thehutgroup.accelerator.GameManager;

import com.thehutgroup.accelerator.GameBoard;
import com.thehutgroup.accelerator.GameResult;
import com.thehutgroup.accelerator.Side;

import java.util.ArrayList;
import java.util.List;

/**
 * GameStatusChecker class provide method to check a board status
 * or extract board information from a given board.
 */
public class GameStatusChecker {

  /**
   * getBoardColoumns returns a list of columns of a given board
   * @param board the board to be checked
   * @return Sider[][] list of columns in 2D array
   */
  public Side[][] getBoardColoumns(GameBoard board) {
    Side[][] columns = new Side[3][3];
    for (int i = 0; i < board.getSize(); i++) {
      for (int j = 0; j < board.getSize(); j++) {
        columns[i][j] = board.peekPosition(i, j);
      }
    }
    return columns;
  }

  /**
   * getRows returns a list of rows of a given board
   * @param board the board to be checked
   * @return Sider[][] list of rows in 2D array
   */
  public Side[][] getRows(GameBoard board) {
    Side[][] rows = new Side[3][3];
    for (int i = 0; i < board.getSize(); i++) {
      for (int j = 0; j < board.getSize(); j++) {
        rows[i][j] = board.peekPosition(j, i);
      }
    }
    return rows;
  }

  /**
   * getRows returns a list of getDiagonals of a given board
   * @param board the board to be checked
   * @return Sider[][] list of diagonals in 2D array
   */
  public Side[][] getDiagonals(GameBoard board) {
    Side[][] diagonals = new Side[2][3];
    for (int i = 0; i < board.getSize(); i++) {
      diagonals[0][i] = board.peekPosition(i, i);
      diagonals[1][i] = board.peekPosition(2 - i, i);
    }
    return diagonals;
  }

  /**
   * gameOverCheck evalues a board and then return the current states of a given board
   * @param board the board to be checked
   * @return GameResult indicates the state of current board
   */
  public GameResult gameOverCheck(GameBoard board) {
    Side[][] columns = getBoardColoumns(board);
    Side[][] rows = getRows(board);
    Side[][] diagonals = getDiagonals(board);
    // check for winning
    for (int i = 0; i < board.getSize(); i++) {
      if (checkLineFullFilled(columns[i], Side.Cross)) {
        return GameResult.CrossWin;
      }
      if (checkLineFullFilled(columns[i], Side.Circle)) {
        return GameResult.CircleWin;
      }
      if (checkLineFullFilled(rows[i], Side.Cross)) {
        return GameResult.CrossWin;
      }
      if (checkLineFullFilled(rows[i], Side.Circle)) {
        return GameResult.CircleWin;
      }
    }
    for (int i = 0; i < 2; i++) {
      if (checkLineFullFilled(diagonals[i], Side.Circle)) {
        return GameResult.CircleWin;
      }
      if (checkLineFullFilled(diagonals[i], Side.Cross)) {
        return GameResult.CrossWin;
      }
    }
    // check for draw
    boolean emptySpace = false;
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (board.peekPosition(i, j) == Side.Empty) {
          emptySpace = true;
        }
      }
    }
    if (emptySpace) {
      return GameResult.Continue;
    } else {
      return GameResult.Draw;
    }
  }

  /**
   * checkFilled checks if a given line is filled by a specified side by a given count
   * @param line the line to be checked. It is an array of Side object
   * @param symbol target side for checking
   * @param countRequired the count limit of the exact repeats of the side in the given line
   * @return true if the give side repeats by 'countRequired' times and false otherwise
   */
  private boolean checkFilled(Side[] line, Side symbol, int countRequired) {
    int count = 0;
    for (int i = 0; i < line.length; i++) {
      if (line[i] == symbol) {
        count++;
      }
    }
    return (count == countRequired);
  }

  /**
   * checkLineFullFilled checks if a line is full filed by a side
   * @param line the line to be checked
   * @param symbol the side to be checked
   * @return true if the line is full filled by the side
   */
  private boolean checkLineFullFilled(Side[] line, Side symbol) {
    return checkFilled(line, symbol, 3);
  }
}
