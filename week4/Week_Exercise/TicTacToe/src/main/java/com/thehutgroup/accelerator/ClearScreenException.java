package com.thehutgroup.accelerator;

public class ClearScreenException extends RuntimeException{
  public ClearScreenException(String message) {
    super(message);
  }

  public ClearScreenException(String message, Throwable cause) {
    super(message, cause);
  }
}
