package com.thehutgroup.accelerator;

public class GamerRunnerSingleton {

  private static final GameRunner INSTANCE = new GameRunner.Builder("A Game Runner").build();

  public static GameRunner getInstance() {
    return INSTANCE;
  }

  private GamerRunnerSingleton() {
  }

}
