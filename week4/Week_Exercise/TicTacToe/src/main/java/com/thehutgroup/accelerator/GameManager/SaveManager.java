package com.thehutgroup.accelerator.GameManager;

import com.thehutgroup.accelerator.GameBoard;
import com.thehutgroup.accelerator.GameRunner;
import com.thehutgroup.accelerator.SavingFileMissingException;
import com.thehutgroup.accelerator.Side;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * SaveManager manages the game saving and loading events
 */
public class SaveManager {
  private static final Logger log = LoggerFactory.getLogger(SaveManager.class);

  /**
   * saveGame saves the current game states in a file named "gameSave"
   * @param process The game process object. It provides all the game information for saving
   * @throws SavingFileMissingException if the file can not be written.
   */
  public static void saveGame(GameProcess process) throws SavingFileMissingException{
    String map = "";
    switch (process.getTurn()) {
      case Circle:
        map += 2;
        break;
      case Cross:
        map += 1;
        break;
      default:
        throw new RuntimeException("The game has invalid turn");
    }
    // convert the map
    GameBoard board = process.getBoard();
    for (int y = 0; y < board.getSize(); y++) {
      for (int x = 0; x < board.getSize(); x++) {
        map += getIntFromSide(board.peekPosition(x, y));
      }
    }
    String result = process.getCrossWin() + ";" + process.getCircleWin();
    String crossPlayer = process.getCrossPlayer();
    String circlePlayer = process.getCirclePlayer();
    String fileName = "gameSave";

    try {
      BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
      out.write(map);
      out.newLine();
      out.write(result);
      out.newLine();
      out.write(crossPlayer + ";" + circlePlayer);
      out.close();
    } catch (IOException excep) {
      log.warn("Failed of saving game to file. Game map is " + map
          + "\nGame result is " + result
          + "\nGame players are:" + crossPlayer + ":" + circlePlayer);
      throw new SavingFileMissingException("Cant save the game state to file", excep);
    }
  }

  /**
   * loadGame loads saved game from savegame file and return a GameProcess object
   * @return GameProcess contains all information of a game
   * @throws SavingFileMissingException is the file is missing
   */
  public static GameProcess loadGame() throws SavingFileMissingException{
    String fileName = "gameSave";
    String[] line;
    try {
      BufferedReader in = new BufferedReader(new FileReader(fileName));
      line = in.readLine().split("");
      Side[][] boardMap = {
          {
              getSideFromString(line[1]), getSideFromString(line[2]), getSideFromString(line[3])
          },
          {
              getSideFromString(line[4]), getSideFromString(line[5]), getSideFromString(line[6])
          },
          {
              getSideFromString(line[7]), getSideFromString(line[8]), getSideFromString(line[9])
          },
      };
      Side turn = getSideFromString(line[0]);
      GameBoard board = new GameBoard(boardMap);
      line = in.readLine().split(";");
      String[] players = in.readLine().split(";");
      int crossWin = Integer.parseInt(line[0]);
      int circleWin = Integer.parseInt(line[1]);
      in.close();
      GameProcess process = new GameProcess.Builder().board(board)
          .circleWin(circleWin).crossWin(crossWin).turn(turn)
          .crossPlayer(players[0]).circlePlayer(players[1])
          .build();

      return process;
    } catch (IOException excep) {
      excep.printStackTrace();
    }
    log.warn("Cant load the saved game");
    throw new SavingFileMissingException("Failed to load Game");
  }

  /**
   * getSideFromString convert a string to a side for restoring game
   * @param intSymbol the string of an integer representation of a side
   * @return a Side object
   */
  private static Side getSideFromString(String intSymbol) {
    switch (intSymbol) {
      case "1":
        return Side.Cross;
      case "2":
        return Side.Circle;
      case "0":
        return Side.Empty;
      default:
        throw new RuntimeException("Invalid int for board");
    }
  }

  /**
   * Get the integer representation of a Side object
   * @param sideSymbol the Side
   * @return an integer representation
   */
  private static int getIntFromSide(Side sideSymbol) {
    switch (sideSymbol) {
      case Circle:
        return 2;
      case Cross:
        return 1;
      case Empty:
        return 0;
      default:
        throw new RuntimeException("Invalid side symbol for board");
    }
  }

}
