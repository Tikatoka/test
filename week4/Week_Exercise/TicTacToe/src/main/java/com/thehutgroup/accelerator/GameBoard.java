package com.thehutgroup.accelerator;

/**
 * The GameBoard class
 */
public class GameBoard {
  private Side[][] boardMap;

  public GameBoard(Side[][] boardMap) {
    this.boardMap = boardMap;
  }

  public GameBoard() {
    Side empty = Side.Empty;
    this.boardMap = new Side[][] {
        {empty, empty, empty},
        {empty, empty, empty},
        {empty, empty, empty}
    };
  }

  /**
   * peekPosition return the piece on position x and y
   * @param x x coordinate
   * @param y y coordinate
   * @return a piece as (x,y)
   */
  public Side peekPosition(int x, int y) {
    if (x < 0 || x >= getSize() || y < 0 || y >= getSize()) {
      return null;
    }
    return this.boardMap[y][x];
  }
  /**
   * peekPosition return the piece on position x and y
   * @param position Position object
   * @return a piece as (x,y)
   */
  public Side peekPosition(Position position) {
    return peekPosition(position.getX(), position.getY());
  }

  /**
   * return game board size
   * @return 3
   */
  public int getSize() {
    return 3;
  }

  /**
   * Update the board with a move position and side
   * @param position the target position
   * @param side the move side
   */
  public void updateMap(Position position, Side side) {
    int px = position.getX();
    int py = position.getY();
    boardMap[py][px] = side;
  }

  /**
   * Deep clone the current board
   * @return a copy of current board
   */
  public GameBoard clone() {
    Side[][] newMap = new Side[3][3];
    for (int i = 0; i < getSize(); i++) {
      for (int j = 0; j < getSize(); j++) {
        newMap[i][j] = boardMap[i][j];
      }
    }
    return new GameBoard(newMap);
  }

}
