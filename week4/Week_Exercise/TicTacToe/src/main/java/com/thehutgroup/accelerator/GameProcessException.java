package com.thehutgroup.accelerator;

public class GameProcessException extends RuntimeException {
  public GameProcessException(String message) {
    super(message);
  }

  public GameProcessException(String message, Throwable cause) {
    super(message, cause);
  }
}
