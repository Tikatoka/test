package com.thehutgroup.accelerator;

import com.thehutgroup.accelerator.GameManager.GameProcess;
import com.thehutgroup.accelerator.GameManager.GameStatusChecker;
import com.thehutgroup.accelerator.GameManager.SaveManager;
import com.thehutgroup.accelerator.Players.Player;
import com.thehutgroup.accelerator.Players.PlayerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * GameRunner class provides method to run a game or a board
 */
public class GameRunner {
  // map. <enum>: value
  private int crossWin;
  private int circleWin;
  private Side turn;
  private static final Logger log = LoggerFactory.getLogger(GameRunner.class);

  public static class Builder {
    private String name;

    public Builder(String name) {
      this.name = name;
    }

    public GameRunner build() {
      return new GameRunner(this);
    }
  }

  private GameRunner(Builder b) {
    System.out.println("Initialized by builder with parameters -> name:" + b.name);
  }

  /**
   * runGame will start a game by specifed board, turn side and two player
   * @param board the board for game
   * @param turn which turn goes first
   * @param crossPlayer the Player plays cross
   * @param circlePlayer the Player plays circle
   */
  public void runGame(GameBoard board, Side turn, Player crossPlayer, Player circlePlayer) {
    this.turn = turn;
    boolean gameContinue = true;
    GameResult result;
//    log.warn("Test");
    while (gameContinue) {
      result = runBoard(board, turn, crossPlayer, circlePlayer);
      switch (result) {
        case CrossWin:
          crossWin++;
          board = new GameBoard();
          break;
        case CircleWin:
          circleWin++;
          board = new GameBoard();
          break;
        case Abort:
          gameContinue = false;
          break;
        case Draw:
          board = new GameBoard();
          break;
        default:
          log.warn("Game ended unexpectedly");
          throw new GameProcessException("Game stopped before finished");
      }
    }
  }

  /**
   * runBoard runs a given board and return the result when the board is completed
   * @param board the board to run
   * @param current current turn side
   * @param crossPlayer the Player plays cross
   * @param circlePlayer the Player plays circle
   * @return GameResult to represent the result of current board
   */
  public GameResult runBoard(GameBoard board, Side current, Player crossPlayer, Player circlePlayer) {
    GameStatusChecker statusChecker = new GameStatusChecker();
    Player turnPlayer = crossPlayer;
    Position selectPosition;
    Side currentTurn = current;
    GameResult gameResult;
    String gameFinishMessage = "";
    do {
      System.out.print("\nTurn:" + currentTurn + "    ");
      System.out.print("Score Cross-Circle: " + crossWin + "-" + circleWin + "\n");
      try {
        Runtime.getRuntime().exec("clear");
      } catch (IOException e) {
        log.warn("Failed to clear screen in game", e);
        throw new ClearScreenException("Can't clear the screen", e);
      }

      // find the play for this turn
      switch (currentTurn) {
        case Cross:
          turnPlayer = crossPlayer;
          break;
        case Circle:
          turnPlayer = circlePlayer;
          break;
      }
      // present the board
      //GameRender.printBoard(board);

      // player select a move, keep looping as long as the target is not empty
      do {
        // Player make a move according to the current board
        selectPosition = turnPlayer.makeOneMove(board.clone());

        // save the current game into file
        if (selectPosition.getY() == -1 && selectPosition.getX() == -1) {
          GameProcess process = new GameProcess.Builder()
              .board(board).turn(turn)
              .crossWin(crossWin).circleWin(circleWin)
              .circlePlayer(circlePlayer.saveToString())
              .crossPlayer(crossPlayer.saveToString())
              .build();
          try {
            SaveManager.saveGame(process);
          } catch (SavingFileMissingException e) {
            log.debug("Saving process failed, game continue");
          }
        }
      }
      while (board.peekPosition(selectPosition) != Side.Empty);

      //update board, take the move
      takeMove(board, selectPosition, currentTurn);

      // change turn
      currentTurn = currentTurn.getOther(currentTurn);
    } while (statusChecker.gameOverCheck(board) == GameResult.Continue);

    // print the game message
    gameResult = statusChecker.gameOverCheck(board);
    switch (gameResult) {
      case CrossWin:
        gameFinishMessage = "Cross win";
        break;
      case CircleWin:
        gameFinishMessage = "Circle win";
        break;
      case Draw:
        gameFinishMessage = "Drawn";
        break;
      default:
        throw new RuntimeException("Game should not finished");
    }
    System.out.println("\nGame finished: " + gameFinishMessage);
    return gameResult;
  }

  /**
   * make effect of a move to board
   * @param board the current board
   * @param move the move position
   * @param turn the side of whom makes the move
   */
  private void takeMove(GameBoard board, Position move, Side turn) {
    board.updateMap(move, turn);
  }

  /**
   * retore game from a GameProcess object. used of loading game from file
   * @param process GameProcess contains all information of a game
   */
  public void restoreGame(GameProcess process) {
    GameBoard board = process.getBoard();
    this.circleWin = process.getCircleWin();
    this.crossWin = process.getCrossWin();
    Side turn = process.getTurn();
    Player crossPlayer = PlayerFactory.getPlayerFromString(process.getCrossPlayer(), Side.Cross);
    Player circlePlayer = PlayerFactory.getPlayerFromString(process.getCirclePlayer(), Side.Circle);
    runGame(board, turn, crossPlayer, circlePlayer);
  }
}
