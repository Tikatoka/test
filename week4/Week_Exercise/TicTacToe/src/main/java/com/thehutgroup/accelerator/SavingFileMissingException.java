package com.thehutgroup.accelerator;

public class SavingFileMissingException extends Exception {
  public SavingFileMissingException(String message) {
    super(message);
  }

  public SavingFileMissingException(String message, Throwable cause) {
    super(message, cause);
  }
}
