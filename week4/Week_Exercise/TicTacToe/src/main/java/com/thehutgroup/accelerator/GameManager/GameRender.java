package com.thehutgroup.accelerator.GameManager;

import com.thehutgroup.accelerator.GameBoard;
import com.thehutgroup.accelerator.Side;

/**
 * The GameRender class provide method for rendering a game board
 */
public class GameRender {
  /**
   * printBoard will take a GameBoard object and then print the board on console
   * @param board The board to be printed
   */
  public static void printBoard(GameBoard board) {
    for (int i = 0; i < board.getSize(); i++) {
      System.out.print("  " + i + " ");
    }
    System.out.print("\n");
    for (int y = 0; y < board.getSize(); y++) {
      System.out.print(y);
      for (int x = 0; x < board.getSize(); x++) {
        Side slot = board.peekPosition(x, y);
        String slotSymbol;
        switch (slot) {
          case Empty:
            slotSymbol = "   ";
            break;
          case Cross:
            slotSymbol = " X ";
            break;
          case Circle:
            slotSymbol = " O ";
            break;
          default:
            throw new RuntimeException("Invalid board");
        }
        System.out.print(slotSymbol);
        if (x < 2) {
          System.out.print("|");
        }
      }
      if (y < 2) {
        System.out.print("\n" + "  --  --  --\n");
      }
    }
  }
}
