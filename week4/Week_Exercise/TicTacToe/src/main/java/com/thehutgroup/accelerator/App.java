package com.thehutgroup.accelerator;

import com.thehutgroup.accelerator.GameManager.GameProcess;
import com.thehutgroup.accelerator.GameManager.SaveManager;
import com.thehutgroup.accelerator.Players.AIPlayer;
import com.thehutgroup.accelerator.Players.HumanPlayer;
import com.thehutgroup.accelerator.Players.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) throws IOException {
    GameRunner runner = GamerRunnerSingleton.getInstance();

    System.out.println("Welcome to Tic Tac Toe");
    System.out.println("1. Start a new game - easy");
    System.out.println("2. Start a new game - medium");
    System.out.println("3. Start a new game - hard");
    System.out.println("4. Continue saved game");
    System.out.print("Please select your options: ");
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String s = br.readLine();
    try {
      if (s.equals("1")) {
        Player circlePlayer = new AIPlayer(Side.Circle, 0.3f);
        runner.runGame(new GameBoard(), Side.Cross, new HumanPlayer(Side.Cross), circlePlayer);
      }
      if (s.equals("2")) {
        Player circlePlayer = new AIPlayer(Side.Circle, 0.8f);
        runner.runGame(new GameBoard(), Side.Cross, new HumanPlayer(Side.Cross), circlePlayer);
      }
      if (s.equals("3")) {
        Player circlePlayer = new AIPlayer(Side.Circle, 1f);
        runner.runGame(new GameBoard(), Side.Cross, new HumanPlayer(Side.Cross), circlePlayer);
      }
      if (s.equals("4")) {
        GameProcess process = SaveManager.loadGame();
        runner.restoreGame(process);
      }
      System.out.println("Invalid Option, will start a new game anyway");
      Player circlePlayer = new AIPlayer(Side.Circle, 1f);
      runner.runGame(new GameBoard(), Side.Cross, new HumanPlayer(Side.Cross), circlePlayer);
    } catch (Exception e) {
      System.out.println("Something wrong, start a new game");
      Player circlePlayer = new AIPlayer(Side.Circle, 1f);
      runner.runGame(new GameBoard(), Side.Cross, new HumanPlayer(Side.Cross), circlePlayer);
    }


  }
}

// packages name should be all lowercase