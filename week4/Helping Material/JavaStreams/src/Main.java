import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {

  public static void main(String[] args) {



    Talk myTalk = Listen::listenIt;
    String s = myTalk.Speak();
    System.out.println(s);






  }
}



/*

// Performance of streams
       int maxSize = 20;
        List<String> myList = new ArrayList<String>(maxSize);
        for (int i = 0; i < maxSize; i++) {
            UUID myUUID = UUID.randomUUID();
            myList.add(myUUID.toString().concat("s"));

        }

        long initialTime = System.currentTimeMillis();
        List<String> collect = myList.stream().map(String::toUpperCase).collect(Collectors.toList());
        long finalTime = System.currentTimeMillis();
        long timeDifference = finalTime - initialTime;
        System.out.println(String.format("sequential stream took: %d ms", timeDifference));
        int seqTime= (int) timeDifference;


        long parallelinitialTime = System.currentTimeMillis();
        List<String> collect1 = myList.parallelStream().map(String::toUpperCase).collect(Collectors.toList());
        long parallelfinalTime = System.currentTimeMillis();
        long paralleltimeDifference = parallelfinalTime - parallelinitialTime;
        System.out.println(String.format("parallel stream took: %d ms", paralleltimeDifference));

        int parallelTime= (int) paralleltimeDifference;

        long percentae= (seqTime/parallelTime)*100;
        System.out.println(percentae+"%");


        \\Map function Example 1
         List<String> myList = new ArrayList<>();
        myList.add("manchester");
        myList.add("london");
        myList.add("mediacity");
        List<String> collect= myList.stream().map(String::toUpperCase).collect(Collectors.toList());
        System.out.println(collect);

        \\Streams map Example 2
        List<Integer> myList = new ArrayList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        List<Integer> collect= myList.stream().map(n ->n*2 ).collect(Collectors.toList());
        System.out.println(collect);

        \\filter example
        List<String> myList = new ArrayList<>();
        myList.add("manchester");
        myList.add("london");
        myList.add("mediacity");
        List<String> collect= myList.stream().filter(s -> s.startsWith("m")).collect(Collectors.toList());
        System.out.println(collect);

        //for each example
        List<Integer> myList = new ArrayList<>();
        myList.add(20000);
        myList.add(3000);
        myList.add(10000);
        myList.stream().map(n ->n*2 ).sorted().forEach(System.out::println);

        //reduce with integer
          List<Integer> myList = new ArrayList<>();
        myList.add(20000);
        myList.add(3000);
        myList.add(10000);
        Integer results=myList.stream().reduce(0, (x,y) -> x+y);
        System.out.println(results);

        //reduce with String
        List<String> myList = new ArrayList<>();
        myList.add("Man");
        myList.add("Lon");
        myList.add("Peak");
        String results = myList.stream().reduce("", (x, y) -> x + y);
        System.out.println(results);

        //sorted
        List<String> myList = new ArrayList<>();
    myList.add("Manchester");
    myList.add("Liverpool");
    myList.add("Chester");
    List<String> myList1= myList.stream().sorted(String::compareTo).collect(Collectors.toList());
    System.out.println(myList1);

    //sorted based on class objects
    List<City_List> myCityList = new ArrayList<>();
    City_List  obj=new City_List();
    obj.setCityName("Manchester");
    myCityList.add(obj);
    City_List obj1 = new City_List();
    obj1.setCityName("Chester");
    myCityList.add(obj1);

    List<City_List> mySortedList= myCityList.stream().sorted((o1, o2) -> o1.getCityName().compareTo(o2.getCityName())).
        filter((s1->s1.getCityName().contains("C"))).collect(Collectors.toList());

    System.out.println(mySortedList.get(0).getCityName());
    //System.out.println(mySortedList.get(1).getCityName());



        \\ Method reference
        //Listen listen=new Listen();
    Talk myTalk = Listen::listenIt;
    String s = myTalk.Speak();
    System.out.println(s);
 */