public class Calculator {


    CalculatorService myService;

    public Calculator(CalculatorService service) {
        this.myService = service;
    }

    public int perform(int i, int j) {
        return myService.performOperation(i, j);
        //return i+j;
    }
}

