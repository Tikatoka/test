package com.thg.accelerator.generics.examples;

import com.thg.accelerator.generics.examples.MiniConsoleApp;
import com.thg.accelerator.generics.examples.userinput.function.FibonacciFuntion;
import com.thg.accelerator.generics.examples.userinput.function.PalindromeFunction;
import com.thg.accelerator.generics.examples.userinput.parser.PositiveIntInputParser;
import com.thg.accelerator.generics.examples.userinput.parser.StringInputParser;

import java.util.function.Function;

public class PalindromeConsoleApp extends MiniConsoleApp<String, Boolean> {
  public PalindromeConsoleApp() {
    super("Palindrome Detector", new StringInputParser(), new PalindromeFunction());
  }
}
