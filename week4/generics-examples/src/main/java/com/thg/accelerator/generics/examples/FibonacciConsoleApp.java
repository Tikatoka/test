package com.thg.accelerator.generics.examples;

import com.thg.accelerator.generics.examples.userinput.function.FibonacciFuntion;
import com.thg.accelerator.generics.examples.userinput.parser.PositiveIntInputParser;

public class FibonacciConsoleApp extends MiniConsoleApp<Integer, Integer> {
  public FibonacciConsoleApp() {
    super("Fibonacci Calculator", new PositiveIntInputParser(), new FibonacciFuntion());
  }
}
