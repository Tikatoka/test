package com.thg.accelerator.generics.examples.userinput.parser;

import java.util.Optional;

public class PositiveIntInputParser extends InputParser<Integer> {
  public PositiveIntInputParser() {
    super("a positive integer");
  }

  @Override
  public Optional<Integer> parseResponse(String entry) {
    try {
      int value = Integer.parseInt(entry);
      return value > 0 ? Optional.of(value) : Optional.empty();
    } catch (NumberFormatException n) {
      return Optional.empty();
    }
  }
}
