package com.thg.accelerator.generics.examples;

public class App {

  public static void main(String[] args) {
    new FibonacciConsoleApp().run();
    new PalindromeConsoleApp().run();
  }
}
