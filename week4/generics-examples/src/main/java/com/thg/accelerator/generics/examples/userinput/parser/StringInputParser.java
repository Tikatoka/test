package com.thg.accelerator.generics.examples.userinput.parser;

import java.util.Optional;

public class StringInputParser extends InputParser<String> {
  public StringInputParser() {
    super(" whatever you like :)");
  }

  @Override
  public Optional<String> parseResponse(String entry) {
    return Optional.of(entry);
  }
}
