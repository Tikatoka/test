package com.thg.accelerator.generics.examples.userinput.parser;

import java.util.Optional;

public abstract class InputParser<T> {
  private String formatDescription;

  public InputParser(String formatDescription) {
    this.formatDescription = formatDescription;
  }

  public String getFormatDescription() {
    return formatDescription;
  }

  public abstract Optional<T> parseResponse(String entry);
}
