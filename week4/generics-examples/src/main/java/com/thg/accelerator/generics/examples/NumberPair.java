package com.thg.accelerator.generics.examples;

public class NumberPair<L extends Number, R extends Number> extends Pair<L, R> {
  public NumberPair(L left, R right) {
    super(left, right);
  }

  public Double sum() {
    return getLeft().doubleValue() + getRight().doubleValue();
  }
}
