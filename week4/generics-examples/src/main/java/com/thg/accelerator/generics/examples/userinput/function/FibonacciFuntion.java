package com.thg.accelerator.generics.examples.userinput.function;

import java.util.function.Function;

public class FibonacciFuntion implements Function<Integer, Integer> {
  @Override
  public Integer apply(Integer input) {
    int a = 1 ,b = 1;
    for(int i = 0; i < input; i++) {
      b = b + a;
      a = b - a;
    }
    return a;
  }
}
