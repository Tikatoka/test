package com.thg.accelerator.generics.examples;

import com.thg.accelerator.generics.examples.userinput.parser.InputParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class MiniConsoleApp<T, S> {
  private String name;
  private InputParser<T> inputParser;
  private Function<T, S> function;

  public MiniConsoleApp(String name, InputParser<T> inputParser, Function<T, S> function) {
    this.name = name;
    this.inputParser = inputParser;
    this.function = function;
  }

  public void run() {
    System.out.println("This is the " + name + " application.");
    Optional<T> input;
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    while(true) {
      System.out.println("Please enter " + inputParser.getFormatDescription());
      try {
        input = inputParser.parseResponse(bufferedReader.readLine());
        if (!input.isEmpty()) {
          break;
        }
      } catch (IOException e) {
      }
      System.out.println("Your input is invalid.");
    }
    S result = function.apply(input.get());
    System.out.println("Result is " + result.toString());


    List<Number> ints = new ArrayList<Integer>();
    ints.add(5);
    Integer i = ints.get(1);
  }

  public static <T extends Throwable> void throwMeConditional(boolean conditional, T exception) throws T {
    if(conditional) {
      throw exception;
    }
  }
}
