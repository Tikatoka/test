package com.thg.accelerator.generics.examples.userinput.function;

import java.util.function.Function;

public class PalindromeFunction implements Function<String, Boolean> {
  @Override
  public Boolean apply(String s) {
    for (int i = 0; i < s.length() / 2; i++) {
      if(s.charAt(i) != s.charAt(s.length() - i - 1)) {
        return false;
      }
    }
    return true;
  }
}
