public class MyThreadPool implements Runnable {

  public String taskName;

  public MyThreadPool(String name) {
    taskName = name;
  }


  @Override
  public void run() {

    for (int i = 0; i < 3; i++) {
      if (i == 0) {
        System.out.println(taskName + " initialised");


      } else {
        System.out.println(taskName + " is being executed");
      }
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    System.out.println(taskName + " is completed");

  }
}
