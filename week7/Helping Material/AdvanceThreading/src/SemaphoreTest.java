import java.util.concurrent.Semaphore;

public class SemaphoreTest {

  // max 4 people
  static Semaphore semaphore = new Semaphore(4);

  static class MyThread extends Thread {

    String name = "";

    MyThread(String name) {
      this.name = name;
    }

    public void run() {

      try {


        System.out.println(name + " : acquiring lock...");
        System.out.println(name + " : permits available: "
            + semaphore.availablePermits());

        semaphore.acquire();
        System.out.println(name + " : got the permit!");

        try {

          for (int i = 1; i <= 5; i++) {

            System.out.println(name + " : is performing operation " + i
                + ", Semaphore permits remaining : "
                + semaphore.availablePermits());

            // sleep 1 second
            Thread.sleep(1000);

          }

        } finally {

          // calling release() after a successful acquire()
          System.out.println(name + " : releasing lock to add the permit");
          semaphore.release();
          System.out.println(name + " : available Semaphore permits after lock release: "
              + semaphore.availablePermits());

        }

      } catch (InterruptedException e) {

        e.printStackTrace();

      }

    }

  }

  public static void main(String[] args) {

    System.out.println(" Semaphore permits : "
        + semaphore.availablePermits());

    MyThread t1 = new MyThread("t1");
    t1.start();

    MyThread t2 = new MyThread("t2");
    t2.start();

    MyThread t3 = new MyThread("t3");
    t3.start();

    MyThread t4 = new MyThread("t4");
    t4.start();

    MyThread t5 = new MyThread("t5");
    t5.start();

    MyThread t6 = new MyThread("t6");
    t6.start();

  }
}
