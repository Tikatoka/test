public class Dog implements Runnable {

    private String name;

    public Dog(String name)
    {
        this.name=name;
        System.out.println(name);
    }
    public Dog(){
        System.out.println("Unknown Dog ");
    }


    @Override
    public void run() {

        for (int i=0;i<20;i++)
        {
            System.out.println(name+" is barking");
            try {
                Thread.sleep(500L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
