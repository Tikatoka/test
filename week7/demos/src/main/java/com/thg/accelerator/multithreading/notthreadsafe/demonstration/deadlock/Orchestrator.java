package com.thg.accelerator.multithreading.notthreadsafe.demonstration.deadlock;

public class Orchestrator {

  public static void main(String[] args) {
    Triforce courage = new Triforce("courage");
    Triforce power = new Triforce("power");
    Triforce wisdom = new Triforce("wisdom");

    Thread t1 = new Thread(new TriforceAcquirer("1", courage, power, wisdom));
    Thread t2 = new Thread(new TriforceAcquirer("2", power, wisdom, courage));
    Thread t3 = new Thread(new TriforceAcquirer("3", wisdom, courage, power));

    t1.start();
    t2.start();
    t3.start();
  }
}
