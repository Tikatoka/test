package com.thg.accelerator.multithreading.notthreadsafe.demonstration.singleton;

import com.thg.accelerator.multithreading.notthreadsafe.LazySingleton;
import com.thg.accelerator.multithreading.notthreadsafe.LazySingleton2;

public class Runnable implements java.lang.Runnable {
  private Object object;

  @Override
  public void run() {
    object = LazySingleton2.getInstance();
  }

  public Object getObject() {
    return object;
  }
}
