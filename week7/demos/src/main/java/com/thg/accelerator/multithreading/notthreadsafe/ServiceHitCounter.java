package com.thg.accelerator.multithreading.notthreadsafe;

public class ServiceHitCounter {
  private int hitCount;

  public ServiceHitCounter() {
    this.hitCount = 0;
  }

  private void doWork(){
  }

  public void countWork() {
    doWork();
    hitCount++;
  }

  public int getHitCount() {
    return hitCount;
  }
}
