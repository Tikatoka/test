package com.thg.accelerator.multithreading.threadsafe;


public class LazySingleton {
  public static Object singleton;

  public static Object getInstance() {
    if(singleton == null) {
      synchronized (LazySingleton.class) {
        if(singleton == null) {
          singleton = new Object();
        }
      }
    }
    return singleton;
  }
}
