package com.thg.accelerator.multithreading.notthreadsafe;

public class LazySingleton {
  public static Object singleton;

  public static Object getInstance() {
    if(singleton == null) {
      singleton = new Object();
    }
    return singleton;
  }
}
