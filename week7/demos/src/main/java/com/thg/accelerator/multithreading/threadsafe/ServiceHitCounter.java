package com.thg.accelerator.multithreading.threadsafe;

import java.util.concurrent.atomic.AtomicInteger;

public class ServiceHitCounter {
  private AtomicInteger hitCount;

  public ServiceHitCounter() {
    this.hitCount = new AtomicInteger(0);
  }

  private void doWork(){
  }

  public void countWork() {
    doWork();
    hitCount.incrementAndGet();
  }

  public int getHitCount() {
    return hitCount.get();
  }
}
