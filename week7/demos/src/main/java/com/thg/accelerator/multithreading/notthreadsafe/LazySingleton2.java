package com.thg.accelerator.multithreading.notthreadsafe;

public class LazySingleton2 {
  public static Object singleton;

  public static Object getInstance() {
    if(singleton == null) {
      synchronized (LazySingleton2.class) {
        singleton = new Object();
      }
    }
    return singleton;
  }
}
