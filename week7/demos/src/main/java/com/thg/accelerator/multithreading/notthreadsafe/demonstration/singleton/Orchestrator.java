package com.thg.accelerator.multithreading.notthreadsafe.demonstration.singleton;

import com.thg.accelerator.multithreading.notthreadsafe.LazySingleton;

public class Orchestrator {
  public static void main(String[] args) throws InterruptedException {
    while(true) {
      Runnable runnable1 = new Runnable();
      Runnable runnable2 = new Runnable();

      Thread thread1 = new Thread(runnable1);
      Thread thread2 = new Thread(runnable2);


      thread1.start();
      thread2.start();


      thread1.join();
      thread2.join();

      if(runnable1.getObject() != runnable2.getObject()) {
        System.out.println("Not singletons: " + runnable1.getObject() + " and " + runnable2.getObject());
        return;
      }

      LazySingleton.singleton = null;
    }
  }
}
