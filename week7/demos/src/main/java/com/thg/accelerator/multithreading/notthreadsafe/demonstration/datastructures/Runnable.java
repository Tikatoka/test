package com.thg.accelerator.multithreading.notthreadsafe.demonstration.datastructures;

import java.util.List;
import java.util.Map;
import java.util.Queue;

public class Runnable implements java.lang.Runnable {
  private List<String> list;
  private Map<String, String> map;
  private Queue<Integer> queue;

  public Runnable(List<String> list, Map<String, String> map, Queue<Integer> queue) {
    this.list = list;
    this.map = map;
    this.queue = queue;
  }

  @Override
  public void run() {
    while(true) {
      double random = Math.random();
      list.add("lol" + random);
      map.put("oh", "dear");
      queue.add(9);

      map.remove("oh");
      queue.remove();
      list.remove("lol" + random);
    }
  }
}
