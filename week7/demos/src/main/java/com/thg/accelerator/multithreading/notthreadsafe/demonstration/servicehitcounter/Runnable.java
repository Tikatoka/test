package com.thg.accelerator.multithreading.notthreadsafe.demonstration.servicehitcounter;

import com.thg.accelerator.multithreading.notthreadsafe.ServiceHitCounter;

public class Runnable implements java.lang.Runnable {
  private int callCount;
  private ServiceHitCounter hitCounter;

  public Runnable(ServiceHitCounter hitCounter) {
    this.hitCounter = hitCounter;
    this.callCount = 0;
  }

  @Override
  public void run() {
    while (true) {
      hitCounter.countWork();
      callCount++;
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        break;
      }
    }
  }

  public int getCallCount() {
    return callCount;
  }
}
