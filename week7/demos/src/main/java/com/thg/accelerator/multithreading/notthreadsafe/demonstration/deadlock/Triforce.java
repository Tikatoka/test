package com.thg.accelerator.multithreading.notthreadsafe.demonstration.deadlock;

public class Triforce {
  private String power;

  public Triforce(String power) {
    this.power = power;
  }

  public synchronized void usePower(String name, Runnable runnable) throws InterruptedException {
    System.out.println(power + " acquired by " + name );
    runnable.run();
  }

  @Override
  public String toString() {
    return "Triforce{" +
        "power='" + power + '\'' +
        '}';
  }
}
