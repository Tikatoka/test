package com.thg.accelerator.multithreading.notthreadsafe.demonstration.deadlock;

public class TriforceAcquirer implements Runnable {
  private Triforce t1;
  private Triforce t2;
  private Triforce t3;
  private String name;

  public TriforceAcquirer(String name, Triforce t1, Triforce t2, Triforce t3) {
    this.name = name;
    this.t1 = t1;
    this.t2 = t2;
    this.t3 = t3;
  }

  @Override
  public void run() {
    try {
      t1.usePower(name, () -> {
        try {
          t2.usePower(name, () -> {
            try {
              t3.usePower(name, () -> {});
            } catch (InterruptedException e) {
              return;
            }
          });
        } catch (InterruptedException e) {
          return;
        }
      });
    } catch (InterruptedException e) {
      return;
    }
  }
}
