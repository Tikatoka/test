package com.thg.accelerator.multithreading.notthreadsafe.demonstration.datastructures;

import java.util.*;

public class Orchestrator {

  public static void main(String[] args) throws InterruptedException {

    List<String> list = new ArrayList<>();
    Map<String, String> map = new HashMap<>();
    Queue<Integer> queue = new LinkedList<>();


    new Thread(new Runnable(list, map, queue)).start();
    new Thread(new Runnable(list, map, queue)).start();

    while (true) {
      System.out.println("list size: " + list.size() + "; map size: " + map.size() + " queue size: " + queue.size() );
      Thread.sleep(500);
    }
  }

}
