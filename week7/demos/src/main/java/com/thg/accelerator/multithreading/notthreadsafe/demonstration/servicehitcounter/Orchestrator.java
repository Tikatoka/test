package com.thg.accelerator.multithreading.notthreadsafe.demonstration.servicehitcounter;

import com.thg.accelerator.multithreading.notthreadsafe.ServiceHitCounter;

public class Orchestrator {
  public static void main(String[] args) {
    ServiceHitCounter serviceHitCounter = new ServiceHitCounter();

    Runnable runnable1 = new Runnable(serviceHitCounter);
    Runnable runnable2 = new Runnable(serviceHitCounter);

    Thread thread1 = new Thread(runnable1);
    Thread thread2 = new Thread(runnable2);

    thread1.start();
    thread2.start();

    while(true) {
      int callCount1 = runnable1.getCallCount();
      int callCount2 = runnable2.getCallCount();

      System.out.println("Overall count is " + serviceHitCounter.getHitCount() + " individual sum is " + (callCount1 + callCount2));
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        break;
      }
    }
  }
}
