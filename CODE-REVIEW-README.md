# Java code review guide

## Project layout
Make sure the project uses a standard build tool, such as maven or gradle, and there are no intellij project files, .class files etc.

## Class structure and high-level design
For each class in the project:
* Is it a noun?
* Does it have only [one job](https://en.wikipedia.org/wiki/Single_responsibility_principle)?
* Is it [immutable](https://en.wikipedia.org/wiki/Immutable_object)?
* Are the [dependencies injected](https://en.wikipedia.org/wiki/Dependency_injection) into the class?
* If it's a [POJO](https://en.wikipedia.org/wiki/Plain_old_Java_object), does it have its own notion of [equality](https://dzone.com/articles/working-with-hashcode-and-equals-in-java) defined?
* Are you using packages to group classes in a similar domain together? 

## Method signatures
For each method:
* Does the name describe what it does?
* Does it do only one thing?
* Is the contract too loose? (e.g. returning int[] for coordinates) or too tight? (not enough info returned)

## Tests
* What is the code coverage like?
* Are your tests checking the contract of the class only (good), or are they testing implementation details (too tightly coupled)


## Code
* Can you see any code that looks visually repeated?
* Is it possible to combine these similar blocks into one, more general piece of code to [reduce duplication](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)?
* Have you chosen the right data structures to maximise performance? (think about which operations will be [called most often](https://en.wikipedia.org/wiki/Amdahl%27s_law))
* Does the [performance of your method](https://en.wikipedia.org/wiki/Big_O_notation) match the [complexity of the problem](https://en.wikipedia.org/wiki/Computational_complexity_theory)?
