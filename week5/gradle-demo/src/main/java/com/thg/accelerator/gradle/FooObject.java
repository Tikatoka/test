package com.thg.accelerator.gradle;

public class FooObject {
    private int foo;

    public FooObject(int foo) {
        this.foo = foo;
    }

    public int getFoo() {
        return foo;
    }
}
