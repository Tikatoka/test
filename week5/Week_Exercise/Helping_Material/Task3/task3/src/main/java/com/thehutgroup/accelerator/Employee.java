package com.thehutgroup.accelerator;

import java.util.Objects;

public class Employee implements Comparable<Employee> {
  private String firstname;
  private String lastname;
  private double salary;

  public Employee(String fn, String ln, double salary) {
    this.firstname = fn;
    this.lastname = ln;
    this.salary = salary;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public double getSalary() {
    return salary;
  }

  public void setSalary(double salary) {
    this.salary = salary;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Employee employee = (Employee) o;
    return Double.compare(employee.salary, salary) == 0 &&
        Objects.equals(firstname, employee.firstname) &&
        Objects.equals(lastname, employee.lastname);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstname, lastname, salary);
  }

  @Override
  public int compareTo(Employee o) {
    return this.firstname.compareTo(o.getFirstname());
  }

// no setter is required
  // equals hash code for objects
}
