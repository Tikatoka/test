package com.thehutgroup.accelerator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


public class SalaryBook {
  List<Employee> list = new ArrayList<>();

  public SalaryBook() {
    loadFromFile();
  }

  public void print() {
    System.out.println("*************Start of the content*************");
    for (Employee e : list) {
      System.out.printf(e.getFirstname() + " " + e.getLastname() + ": %.2f\n" ,e.getSalary());
    }
    System.out.println("*************End of the content*************");
  }

  public double promote() {
    return list.stream().mapToDouble(e->(e.getFirstname().toLowerCase().charAt(0)<'n'? 1.1:1.0) * e.getSalary()).sum();
  }

  private void loadFromFile() {
    String filePath = "./src/main/resources/data.txt";
    BufferedReader reader;
    try {
      reader = new BufferedReader(new FileReader(filePath));
      // first line does not contains any useful data
      reader.readLine();
      String line = reader.readLine();
      while (line != null) {
        String[] data = line.split("\\|");
        Employee oneEmployee = new Employee(data[0], data[1], Double.parseDouble(data[2]));
        list.add(oneEmployee);
        line = reader.readLine();
      }
    } catch (Exception e) {
      System.out.println("Can not load the file: " + filePath);
    }
  }

  public void sortByFirstName() {
    list.sort(Comparator.comparing(Employee::getFirstname));
  }

  public void sortByLastName() {
    list.sort(Comparator.comparing(Employee::getLastname));
  }
  public void sortBySalary() {
      list.sort(Comparator.comparing(Employee::getSalary));
  }

  public void adSortByFirstName() {
    Collections.sort(list);
  }
}
