package com.thehutgroup.accelerator;

public interface ServerRepository {

    public String getData(String urlString);
}
