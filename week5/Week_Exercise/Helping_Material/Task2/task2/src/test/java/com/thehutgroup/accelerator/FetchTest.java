package com.thehutgroup.accelerator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Unit test for simple App.
 */
public class FetchTest
{
    FetchData fd = new FetchData();

    @Before
    public void setUp(){
        fd.myRepo = Mockito.mock(ServerRepository.class);
        Mockito.when(fd.myRepo.getData("www.thg.com/datapost")).thenReturn("your data");
    }

    @Test
    public void testFetchData()
    {

        assertEquals("Fetch data from database: should get 'your data'", "your data", fd.hitServerRepository());
    }
}
