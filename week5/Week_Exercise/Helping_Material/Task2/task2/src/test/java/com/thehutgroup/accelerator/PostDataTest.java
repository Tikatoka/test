package com.thehutgroup.accelerator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PostDataTest {
  PostData pd = new PostData();

  @Before
  public void setUp(){
    pd.updateMyServer = Mockito.mock(UpdateServer.class);
    Mockito.when(pd.updateMyServer.updateVersion(1.1)).thenReturn(1.2);
  }
  @Test
  public void postDataTest(){
    assertTrue("Input version 1.1 then get 1.2 as return", 1.2 == pd.updateRepositoryVersion(1.1));
  }
}
