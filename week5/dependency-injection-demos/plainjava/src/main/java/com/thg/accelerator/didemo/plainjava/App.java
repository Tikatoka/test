package com.thg.accelerator.didemo.plainjava;

import com.thg.accelerator.didemo.plainjava.loader.FileBookLoader;
import com.thg.accelerator.didemo.plainjava.repository.RepositoryException;
import com.thg.accelerator.didemo.plainjava.repository.impl.InMemoryBookRepository;

import java.util.List;
import java.util.stream.Collectors;

public class App {
  public static void main(String[] args) throws RepositoryException {
    List<Book> loveBooks = new BookSearcher(new FileBookLoader("src/main/resources/shakespearePenguin.txt"),
        new InMemoryBookRepository()).findBooks("love");
    System.out.println(loveBooks.stream().map(Book::getTitle).collect(Collectors.toList()));
  }
}
