package com.thg.accelerator.didemo.plainjava;

import java.math.BigInteger;
import java.util.Objects;

public class Book {
  private long isbn;
  private String author;
  private String synopsis;
  private String title;

  private Book(long isbn, String title, String author, String synopsis) {
    this.title = title;
    this.isbn = isbn;
    this.author = author;
    this.synopsis = synopsis;
  }

  public String getTitle() {
    return title;
  }

  public long getIsbn() {
    return isbn;
  }

  public String getAuthor() {
    return author;
  }

  public String getSynopsis() {
    return synopsis;
  }

  @Override
  public String toString() {
    return "Book{" +
        "isbn=" + isbn +
        ", author='" + author + '\'' +
        ", synopsis='" + synopsis + '\'' +
        ", title='" + title + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Book book = (Book) o;
    return isbn == book.isbn &&
        Objects.equals(author, book.author) &&
        Objects.equals(synopsis, book.synopsis) &&
        Objects.equals(title, book.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isbn, author, synopsis, title);
  }

  public static class BookBuilder {
    private long isbn;
    private String author;
    private String synopsis;
    private String title;

    public BookBuilder isbn(long isbn) {
      this.isbn = isbn;
      return this;
    }

    public BookBuilder author(String author) {
      this.author = author;
      return this;
    }

    public BookBuilder synopsis(String synopsis) {
      this.synopsis = synopsis;
      return this;
    }

    public BookBuilder title(String title) {
      this.title = title;
      return this;
    }

    public Book createBook() {
      return new Book(isbn, title, author, synopsis);
    }
  }
}
