package com.thg.accelerator.didemo.springxml.repository.impl;

import com.thg.accelerator.didemo.springxml.Book;
import com.thg.accelerator.didemo.springxml.repository.BookRepository;
import com.thg.accelerator.didemo.springxml.repository.RepositoryException;

import java.util.List;
import java.util.Optional;

public class SqlLiteBookRepository implements BookRepository {
  @Override
  public void createBook(Book book) throws RepositoryException {

  }

  @Override
  public void updateBook(Book book) throws RepositoryException {

  }

  @Override
  public void deleteBook(int isbn) throws RepositoryException {

  }

  @Override
  public Optional<Book> getBook(int isbn) throws RepositoryException {
    return Optional.empty();
  }

  @Override
  public List<Book> findBooks(String search) throws RepositoryException {
    return null;
  }
}
