package com.thg.accelerator.didemo.springxml;

import com.thg.accelerator.didemo.springxml.repository.RepositoryException;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.stream.Collectors;

public class App {
  public static void main(String[] args) throws RepositoryException {
    ConfigurableApplicationContext context =
        new ClassPathXmlApplicationContext(new String[] {"beans.xml"});

    BookSearcher bookSearcher = context.getBean(BookSearcher.class);
    System.out.println(bookSearcher.findBooks("love").stream().map(Book::getTitle).collect(Collectors.toList()));
  }
}
