package com.thg.accelerator.didemo.springxml.loader;

import com.thg.accelerator.didemo.springxml.Book;

import java.util.List;

public interface BookLoader {
  List<Book> load();
}
