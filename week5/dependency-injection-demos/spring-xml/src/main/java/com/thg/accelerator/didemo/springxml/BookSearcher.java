package com.thg.accelerator.didemo.springxml;

import com.thg.accelerator.didemo.springxml.loader.BookLoader;
import com.thg.accelerator.didemo.springxml.repository.BookRepository;
import com.thg.accelerator.didemo.springxml.repository.RepositoryException;
import org.springframework.beans.factory.InitializingBean;

import java.util.List;

public class BookSearcher implements InitializingBean {
  private BookLoader bookLoader;
  private BookRepository bookRepository;

  public void setBookLoader(BookLoader bookLoader) {
    this.bookLoader = bookLoader;
  }

  public void setBookRepository(BookRepository bookRepository) {
    this.bookRepository = bookRepository;
  }

  public List<Book> findBooks(String query) throws RepositoryException {
    return bookRepository.findBooks(query);
  }

  @Override
  public void afterPropertiesSet() {
    bookLoader.load().stream().forEach(book -> {
      try {
        bookRepository.createBook(book);
      } catch (RepositoryException e) {
        throw new RuntimeException("Unable to save books :" , e);
      }
    });
  }
}
