package com.thg.accelerator.didemo.springboot.repository;

import com.thg.accelerator.didemo.springboot.Book;
import com.thg.accelerator.didemo.springboot.repository.impl.PostgresBookRepository;
import com.thg.accelerator.didemo.springboot.repository.impl.PostgresVanillaJDBCBookRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.*;

public class PostgresVanillaRepositoryTest {

  @Mock
  private JdbcTemplate template;

  @InjectMocks
  private PostgresVanillaJDBCBookRepository bookRepository;

  @Test
  public void testGetBookPresent() throws RepositoryException, SQLException {
    MockitoAnnotations.initMocks(this);
    Book book = new Book.BookBuilder().author("a")
        .isbn(123)
        .synopsis("syn")
        .title("title")
        .createBook();
    ResultSet resultSet = mock(ResultSet.class);
    when(resultSet.getString("title")).thenReturn(book.getTitle());
    when(resultSet.getString("author")).thenReturn(book.getAuthor());
    when(resultSet.getString("synopsis")).thenReturn(book.getSynopsis());
    when(resultSet.getLong("isbn")).thenReturn(book.getIsbn());
    when(template.queryForObject(eq("select * from books where isbn=?"), (RowMapper<Optional<Book>>) anyObject(), anyObject()))
          .then((Answer<Optional<Book>>) invocationOnMock -> {
            RowMapper<Optional<Book>> argument = invocationOnMock.getArgument(1);
            return argument.mapRow(resultSet, 0);
          });

    Optional<Book> bookResult = bookRepository.getBook((int) book.getIsbn());
    Assert.assertEquals("book should have equaled mock result", bookResult, Optional.of(book));
    verify(resultSet, times(1)).getString("title");
    verify(resultSet, times(1)).getString("author");
    verify(resultSet, times(1)).getString("synopsis");
    verifyNoMoreInteractions(resultSet);
  }
}
