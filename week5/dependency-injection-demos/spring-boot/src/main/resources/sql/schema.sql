create database library;
\connect library;

CREATE TABLE books (
 isbn BIGINT PRIMARY KEY,
 author VARCHAR (50) NOT NULL,
 synopsis VARCHAR (5000) NOT NULL,
 title VARCHAR (355) NOT NULL
);

Insert into books (isbn, author, synopsis, title)
          values (1010101, 'Tom', 'Dummy book', 'book should not be here');