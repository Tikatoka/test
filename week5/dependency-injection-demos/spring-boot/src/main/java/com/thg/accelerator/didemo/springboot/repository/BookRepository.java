package com.thg.accelerator.didemo.springboot.repository;

import com.thg.accelerator.didemo.springboot.Book;

import java.util.List;
import java.util.Optional;

public interface BookRepository {
  void createBook(Book book) throws RepositoryException;
  void updateBook(Book book) throws RepositoryException;
  void deleteBook(int isbn) throws RepositoryException;
  void deleteBooks() throws RepositoryException;
  Optional<Book> getBook(int isbn) throws RepositoryException;
  List<Book> findBooks(String search) throws RepositoryException;
}
