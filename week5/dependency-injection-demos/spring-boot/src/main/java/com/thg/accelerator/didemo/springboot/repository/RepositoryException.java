package com.thg.accelerator.didemo.springboot.repository;

public class RepositoryException extends Exception {
  public RepositoryException(String message) {
    super(message);
  }

  public RepositoryException(String message, Throwable cause) {
    super(message, cause);
  }
}
