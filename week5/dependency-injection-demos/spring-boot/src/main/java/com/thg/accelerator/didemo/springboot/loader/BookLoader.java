package com.thg.accelerator.didemo.springboot.loader;

import com.thg.accelerator.didemo.springboot.Book;

import java.util.List;

public interface BookLoader {
  List<Book> load();
}
