package com.thg.accelerator.didemo.springboot.repository.impl;

import com.thg.accelerator.didemo.springboot.Book;
import com.thg.accelerator.didemo.springboot.repository.BookRepository;
import com.thg.accelerator.didemo.springboot.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.List;
import java.util.Optional;

//@Component
public class PostgresBookRepository implements BookRepository {
  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;


  @Override
  public void createBook(Book book) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
                                      .addValue("isbn", book.getIsbn())
                                      .addValue("title", book.getTitle())
                                      .addValue("author", book.getAuthor())
                                      .addValue("synopsis", book.getSynopsis());
    try {
      jdbcTemplate.update("insert into books (isbn, title, author, synopsis) values (:isbn, :title, :author, :synopsis)", namedParameters);
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to insert book", e);
    }
  }

  @Override
  public void updateBook(Book book) throws RepositoryException {

  }

  @Override
  public void deleteBook(int isbn) throws RepositoryException {

  }

  @Override
  public void deleteBooks() throws RepositoryException {
    try {
      jdbcTemplate.update("delete from books", (SqlParameterSource) null);
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to insert book", e);
    }
  }

  @Override
  public Optional<Book> getBook(int isbn) throws RepositoryException {
    return Optional.empty();
  }

  @Override
  public List<Book> findBooks(String search) throws RepositoryException {
<<<<<<< HEAD:week5/dependency-injection-demos/spring-boot/src/main/java/com/thg/accelerator/didemo/springxml/repository/impl/PostgresBookRepository.java
//    jdbcTemplate.query("");
    return null;
=======
    SqlParameterSource namedParameters = new MapSqlParameterSource()
        .addValue("query", search);
    try {
      return jdbcTemplate.query("select isbn, title, author, synopsis from books where title like CONCAT('%', CONCAT(:query, '%')) or synopsis like CONCAT('%', CONCAT(:query, '%'))  ", namedParameters, (resultSet, i) -> {
        String title = resultSet.getString("title");
        String author = resultSet.getString("author");
        String synopsis = resultSet.getString("synopsis");
        long isbn = resultSet.getLong("isbn");
        return new Book.BookBuilder().author(author)
                                      .isbn(isbn)
                                      .synopsis(synopsis)
                                      .title(title)
                                      .createBook();
      });
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to query for books", e);
    }
>>>>>>> 1d8af926dcbe6caf568ec21272180006899516a7:week5/dependency-injection-demos/spring-boot/src/main/java/com/thg/accelerator/didemo/springboot/repository/impl/PostgresBookRepository.java
  }
}
