package com.thg.accelerator.didemo.springboot.repository.impl;

import com.thg.accelerator.didemo.springboot.Book;
import com.thg.accelerator.didemo.springboot.repository.BookRepository;
import com.thg.accelerator.didemo.springboot.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PostgresVanillaJDBCBookRepository implements BookRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;


  @Override
  public void createBook(Book book) throws RepositoryException {
    try {
      jdbcTemplate.update("insert into books (isbn, title, author, synopsis) values (?, ?, ?, ?)",
                              book.getIsbn(), book.getTitle(), book.getAuthor(), book.getSynopsis());
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to insert book", e);
    }
  }

  @Override
  public void updateBook(Book book) throws RepositoryException {

  }

  @Override
  public void deleteBook(int isbn) throws RepositoryException {

  }

  @Override
  public void deleteBooks() throws RepositoryException {
    try {
      jdbcTemplate.update("delete from books");
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to insert book", e);
    }
  }

  @Override
  public Optional<Book> getBook(int isbn) throws RepositoryException {
    try {
      return jdbcTemplate.queryForObject("select * from books where isbn=?",
          (resultSet, i) -> {
            String title = resultSet.getString("title");
            String author = resultSet.getString("author");
            String synopsis = resultSet.getString("synopsis");
            return Optional.of(new Book.BookBuilder().author(author)
                .isbn(isbn)
                .synopsis(synopsis)
                .title(title)
                .createBook());
          },isbn);
    } catch (EmptyResultDataAccessException e) {
      return Optional.empty();
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to query for books", e);
    }
  }

  @Override
  public List<Book> findBooks(String search) throws RepositoryException {
    try {
      return jdbcTemplate.query("select isbn, title, author, synopsis from books where title like CONCAT('%', CONCAT(?, '%')) or synopsis like CONCAT('%', CONCAT(?, '%'))  ",
          (resultSet, i) -> {
        String title = resultSet.getString("title");
        String author = resultSet.getString("author");
        String synopsis = resultSet.getString("synopsis");
        long isbn = resultSet.getLong("isbn");
        return new Book.BookBuilder().author(author)
                                      .isbn(isbn)
                                      .synopsis(synopsis)
                                      .title(title)
                                      .createBook();
      }, search);
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to query for books", e);
    }
  }
}
