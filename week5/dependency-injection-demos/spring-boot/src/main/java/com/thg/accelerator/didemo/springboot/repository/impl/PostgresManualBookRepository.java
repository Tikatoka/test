package com.thg.accelerator.didemo.springboot.repository.impl;

import com.thg.accelerator.didemo.springboot.Book;
import com.thg.accelerator.didemo.springboot.repository.BookRepository;
import com.thg.accelerator.didemo.springboot.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Do not write code like this -- it is an example to show you how
 * connecting to a DB works using a low level library (java.sql)
 */
public class PostgresManualBookRepository implements BookRepository {
  private String connectionUrl;
  private String username;
  private String password;

  @Autowired
  public PostgresManualBookRepository(@Value("${spring.datasource.url}") String connectionUrl,
                                      @Value("${spring.datasource.username}") String username,
                                      @Value("${spring.datasource.password}") String password) {
    this.connectionUrl = connectionUrl;
    this.username = username;
    this.password = password;
  }

  @Override
  public void createBook(Book book) throws RepositoryException {
    Connection con = null;
    try {
      con = DriverManager.getConnection(connectionUrl, username, password);
      Statement stmt = con.createStatement();
      String SQL = "insert into books (isbn, title, author, synopsis) values (" + book.getIsbn()
                      + ", '" + book.getTitle() + "', '"
                      + book.getAuthor() + "', '" + book.getSynopsis() + "')";
      stmt.execute(SQL);
    } catch (SQLException e) {
      throw new RepositoryException("failed to insert book", e);
    } finally {
      if(con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          throw new RepositoryException("Couldn't close", e);
        }
      }
    }
  }

  @Override
  public void updateBook(Book book) throws RepositoryException {

  }

  @Override
  public void deleteBook(int isbn) throws RepositoryException {

  }

  @Override
  public void deleteBooks() throws RepositoryException {
    Connection con = null;
    try {
      con = DriverManager.getConnection(connectionUrl, username, password);
      Statement stmt = con.createStatement();
      String SQL = "delete from books";
      stmt.execute(SQL);
    } catch (SQLException e) {
      throw new RepositoryException("failed to delete books", e);
    } finally {
      if(con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          throw new RepositoryException("Couldn't close", e);
        }
      }
    }
  }

  @Override
  public Optional<Book> getBook(int isbn) throws RepositoryException {
    return Optional.empty();
  }

  @Override
  public List<Book> findBooks(String search) throws RepositoryException {
    return new LinkedList<>();
  }
}
