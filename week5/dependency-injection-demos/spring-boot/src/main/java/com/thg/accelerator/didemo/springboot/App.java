package com.thg.accelerator.didemo.springboot;

import com.thg.accelerator.didemo.springboot.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class App {
  @Autowired
  private BookSearcher bookSearcher;

  @Autowired
  private BookRepository repo;

  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }

  @Bean
  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    return args -> {
      repo.createBook(new Book.BookBuilder()
                            .author("lol")
                            .isbn(1234)
                            .title("ur getting pwned")
                            .synopsis("a'); drop table books; select ('1")
                            .createBook());
    };
  }
}
