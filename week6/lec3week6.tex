\documentclass{beamer}
 
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{color}
\makeatletter
\setlength\beamer@paperwidth{16.00cm}%
\setlength\beamer@paperheight{9.00cm}%
\geometry{papersize={\beamer@paperwidth,\beamer@paperheight}}
\makeatother
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\usepackage{graphicx}
\graphicspath{ {./images/} }

\lstset{ 
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
	basicstyle=\footnotesize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{mygreen},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{blue},       % keyword style
	language=Octave,                 % the language of the code
	morekeywords={*,...},            % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{mymauve},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
 
%Information to be included in the title page:
\title{Java Course}
\subtitle{Week 6, Lecture 3\\Java: Database Interaction
}
\author{Rehman Arshad/Shaun Hall}
\institute{THG Accelerator}
\date{2019}
 
 
 
\begin{document}
 \lstset{language=Java}
\frame{\titlepage}
 

\begin{frame}
\frametitle{Reflecting on the course so far}
\centering
\begin{tabular}{|c|c|}  
	\hline
	\textbf{Week} & \textbf{Topics} \\ [0.5ex]
	\hline
	1 & Philosophy, Types, Basic Syntax, IntelliJ \\
	2 & Object-Orientation, Polymorphism, Testing \\
	3 & Data Structures, Software Design Principles \\
	4 & Streams, Generics, Testing \\
	5 & Dependencies (Build), Dependency Injection \\
	6 & Exceptions, Logging, Databases \\
	7 & Multi-Threading \\
	8 & REST Microservices \\
	9 & Practical Week -- Jenkins \\
	10 & Advanced Topics -- Reflection, Casting, Dependency Lattices \\
	11 & Memory Week -- Garbage Collection, Heap Analysis \\
	12 & Java + Docker, Metrics, Mockito \\
	
	\hline
\end{tabular} 
\end{frame}


\begin{frame}
\frametitle{Database Client Introduction }
Databases provide applications with persistent, shared state, allowing your application to scale and restart without data loss.

\vspace{1cm}
\centering{\includegraphics[width=200px]{appdiagram}}


\end{frame}

\begin{frame}
\frametitle{Code Goals }


\begin{itemize}
	\item Performance
	\begin{itemize}
		\item How many times should my query be executed to achieve my application-layer result
		\item How do I manage the connections to the database?
	\end{itemize}
	\item Security
	\item Flexibility
	\begin{itemize}
		\item Wrap implementation-specific return types and exceptions
		\item Use an interface to give flexibility to change database type
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Code Location }
All of these code examples are in implementations of \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/tree/master/week5/dependency-injection-demos/spring-boot/src/main/java/com/thg/accelerator/didemo/springboot/repository/impl}{BookRepository}
\vspace{1cm}

Test code is \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/blob/master/week5/dependency-injection-demos/spring-boot/src/test/java/com/thg/accelerator/didemo/springboot/repository/PostgresVanillaRepositoryTest.java}{here}
\end{frame}

\begin{frame}[fragile]
\frametitle{Manual approach (without DI) - Insertion}
\begin{lstlisting}
public void createBook(Book book) throws RepositoryException {
	Connection con = null;
	try {
		con = DriverManager.getConnection(connectionUrl, username, password);
		Statement stmt = con.createStatement();
		String SQL = "insert into books (isbn, title, author, synopsis) values (" + book.getIsbn() + ", '" + book.getTitle() + "', '"
		+ book.getAuthor() + "', '" + book.getSynopsis() + "')";
		stmt.execute(SQL);
	} catch (SQLException e) {
		throw new RepositoryException("failed to insert book", e);
	} finally {
		if(con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				throw new RepositoryException("Couldn't close", e);
			}
		}
	}
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Manual approach (without DI) - SQL Injection }
\begin{lstlisting}
@Bean
public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
	return args -> {
		repo.createBook(new Book.BookBuilder()
		.author("lol")
		.isbn(1234)
		.title("ur getting pwned")
		.synopsis("a'); drop table books; select ('1")
		.createBook());
	};
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Manual approach (without DI) - other problems }
\begin{itemize}
	\item Connection inefficiency 
	\begin{itemize}
		\item One connection \textbf{per query}
		\item Verbose try-catch inside finally when closing
	\end{itemize}
	\item Low level code -- preparing statement and manual sql construction
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{JDBC Templates -- insertion }
\begin{lstlisting}
@Override
public void createBook(Book book) throws RepositoryException {
	try {
		jdbcTemplate.update("insert into books (isbn, title, author, synopsis) values (?, ?, ?, ?)",
		book.getIsbn(), book.getTitle(), book.getAuthor(), book.getSynopsis());
	} catch (DataAccessException e) {
		throw new RepositoryException("failed to insert book", e);
	}
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{JDBC Templates -- selection 1 }
\begin{lstlisting}
@Override
public Optional<Book> getBook(int isbn) throws RepositoryException {
	try {
		return jdbcTemplate.queryForObject("select * from books where isbn=?  ",
		(resultSet, i) -> {
			String title = resultSet.getString("title");
			String author = resultSet.getString("author");
			String synopsis = resultSet.getString("synopsis");
			return Optional.of(new Book.BookBuilder().author(author)
			.isbn(isbn)
			.synopsis(synopsis)
			.title(title)
			.createBook());
		},isbn);
	} catch (EmptyResultDataAccessException e) {
		return Optional.empty();
	} catch (DataAccessException e) {
		throw new RepositoryException("failed to query for books", e);
	}
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{JDBC Templates -- can we improve? }
Security and connection management are much better.
Two problems still remain:
\vspace{1cm}
\begin{lstlisting}[language=sql]
select * from books 
\end{lstlisting}
Position of arguments makes the query brittle

\end{frame}

\begin{frame}[fragile]
\frametitle{JDBC Templates -- selection 2 }
\begin{lstlisting}
@Override
public Optional<Book> getBook(int isbn) throws RepositoryException {
	try {
		return jdbcTemplate.queryForObject("select isbn, title, author, synopsis from books where isbn=?  ",
		(resultSet, i) -> {
			String title = resultSet.getString("title");
			String author = resultSet.getString("author");
			String synopsis = resultSet.getString("synopsis");
			return Optional.of(new Book.BookBuilder().author(author)
			.isbn(isbn)
			.synopsis(synopsis)
			.title(title)
			.createBook());
		},isbn);
	} catch (EmptyResultDataAccessException e) {
		return Optional.empty();
	} catch (DataAccessException e) {
		throw new RepositoryException("failed to query for books", e);
	}
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{JDBC Templates -- selection 2 }
By selecting the fields we want in the query, we protect our code against new fields being added in the database.
\vspace{1cm}

We are making the code \textbf{forwards-compatible}.
\end{frame}

\begin{frame}[fragile]
\frametitle{Named JDBC Templates}
\begin{lstlisting}
@Override
public void createBook(Book book) throws RepositoryException {
	SqlParameterSource namedParameters = new MapSqlParameterSource()
	.addValue("isbn", book.getIsbn())
	.addValue("title", book.getTitle())
	.addValue("author", book.getAuthor())
	.addValue("synopsis", book.getSynopsis());
	try {
		jdbcTemplate.update("insert into books (isbn, title, author, synopsis) values (:isbn, :title, :author, :synopsis)", namedParameters);
	} catch (DataAccessException e) {
		throw new RepositoryException("failed to insert book", e);
	}
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Named JDBC Templates}
Naming the query parameters makes the query less error prone -- the order of our parameters doesn't matter any more.
\end{frame}

\begin{frame}[fragile]
\frametitle{Writing tests for JDBC template clients}
\begin{lstlisting}[basicstyle=\tiny]
@Test
public void testGetBookPresent() throws RepositoryException, SQLException {
	MockitoAnnotations.initMocks(this);
	Book book = new Book.BookBuilder().author("a")
	.isbn(123)
	.synopsis("syn")
	.title("title")
	.createBook();
	ResultSet resultSet = mock(ResultSet.class);
	when(resultSet.getString("title")).thenReturn(book.getTitle());
	when(resultSet.getString("author")).thenReturn(book.getAuthor());
	when(resultSet.getString("synopsis")).thenReturn(book.getSynopsis());
	when(resultSet.getLong("isbn")).thenReturn(book.getIsbn());
	when(template.queryForObject(eq("select * from books where isbn=?"), (RowMapper<Optional<Book>>) anyObject(), anyObject()))
	.then((Answer<Optional<Book>>) invocationOnMock -> {
		RowMapper<Optional<Book>> argument = invocationOnMock.getArgument(1);
		return argument.mapRow(resultSet, 0);
	});
	
	Optional<Book> bookResult = bookRepository.getBook((int) book.getIsbn());
	Assert.assertEquals("book should have equaled mock result", bookResult, Optional.of(book));
	verify(resultSet, times(1)).getString("title");
	verify(resultSet, times(1)).getString("author");
	verify(resultSet, times(1)).getString("synopsis");
	verifyNoMoreInteractions(resultSet);
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Notes about ORM frameworks }
Object Relational Mapping frameworks aim to abstract away the sql behind their code. Examples include Hibernate and JPA.
\vspace{1cm}

Developers need to be aware of the sql they are executing, especially in non-toy contexts.
\vspace{1cm}

Do not use ORM frameworks.

\end{frame}

\begin{frame}
\frametitle{Think about efficiency -- databases are good at batching }
When considering an overall application activity,

\begin{itemize}
	\item Are you executing queries sparingly -- could you reduce the number of queries by doing a join?
	\item Are you using database indicies effectively?
	\item Do you have any transactional requirements? If so, are you releasing locks in all cases? (c.f. finally clause)
	\item Are you taking out read locks? Consider tolerating dirty reads in return for less contention with writers.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Versioning your schemata}
Your schemata will evolve over time, and it should be treated as code, alongside the rest of the project.
\vspace{1cm}

At the very least, your schemata should be in source control as .sql files. There are some frameworks to help implement incremental changes e.g. Liquibase.
\end{frame}

\begin{frame}
\frametitle{Database Summary}
\begin{itemize}
	\item Use a high-level framework, such as Spring's NamedJDBCTemplate to manage connection pooling and secure SQL coding
	\item Make sure you're aware of the sql your code will execute to achieve its high-level operations efficiently with minimal contention
	\item Ensure your schemata is source-controlled and it's easy for developers to start a local instance (with a single command, such as docker-compose up)

\end{itemize}
\end{frame}

\end{document}