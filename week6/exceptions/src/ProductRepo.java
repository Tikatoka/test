public interface ProductRepo {
  public Product getProduct(int productId);
}
