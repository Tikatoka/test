package com.thg.mainlog;
import com.thg.secondarylog.SecondClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

    private static final Logger log = LoggerFactory.getLogger(App.class);


    public static void main(String[] args) {

        log.trace("This is a trace level message", App.class.getSimpleName());
        log.debug("This is a debug level message");
        log.info("This is an info level message");
        log.warn("This is a warn level message!");
        log.error("This is an error level message");
        SecondClass obj= new SecondClass();
        obj.getLogs();


    /*int value = 42;
    int divider = 0;
    try {
      int newValue = 42 / divider;

    } catch (ArithmeticException e) {
      log.error("Cannot divide {} b {}", value, divider);

    }*/

    }


}
