package com.thg.secondarylog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecondClass {

  private static final Logger log = LoggerFactory.getLogger(SecondClass.class);

  public void getLogs() {
    log.trace("This is a trace level message from Second Class");
    log.debug("This is a debug level message from Second Class");
    log.info("This is an info level message from Second Class");
    log.warn("This is a warn level message from Second Class");
    log.error("This is an error level message from Second Class");

  }
}
