package com.thehutgroup.accelerator.dependenceInjection.repositorymanager;

import com.thehutgroup.accelerator.dependenceInjection.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class PostgresRepositoryManager implements RepositoryManager {
  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  @Override
  public void registerProduct(Product product) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
        .addValue("id", product.getId())
        .addValue("description", product.getDescription())
        .addValue("catalogIndex", product.getCatalogIndex())
        .addValue("imageIndex", product.getImageIndex())
        .addValue("priceInPence", product.getPriceInPence())
        .addValue("name", product.getName());

    try {
      jdbcTemplate.update("Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description) " +
          "values (:id, :priceInPence, :imageIndex, :catalogIndex, :name, :description)", namedParameters);
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to insert book", e);
    }
  }

  @Override
  public void updateProduct(int id, Product product) throws RepositoryException {
    try {
      if (product.getId() != id) {
        throw new RepositoryException("The id of the new product is not the same as target id");
      }
      SqlParameterSource namedParameters = new MapSqlParameterSource()
          .addValue("id", product.getId())
          .addValue("description", product.getDescription())
          .addValue("catalogIndex", product.getCatalogIndex())
          .addValue("imageIndex", product.getImageIndex())
          .addValue("priceInPence", product.getPriceInPence())
          .addValue("name", product.getName());
      jdbcTemplate.update("update productinfo set priceInPence=:priceInPence, imageIndex=:imageIndex, " +
          "catalogIndex=:catalogIndex, name=:name, description=:description where id=:id ", namedParameters);
    } catch (DataAccessException e) {
      throw new RepositoryException("failed to update a product with ID:" + id, e);
    }
  }

  @Override
  public Product searchProduct(int id) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
        .addValue("id", id);
    try {
      List<Product> products = jdbcTemplate.query("select id, priceinpence, imageindex, catalogindex, name, description " +
          "from productinfo where id=:id;", namedParameters, new ProductDataExtractor());
      if (products.size() > 1) {
        throw new RepositoryException("Search of product with id " + id + " returns more than 1 items");
      } else {
        return products.isEmpty() ? null : products.get(0);
      }
    } catch (Exception e) {
      throw new RepositoryException("Can't complete the search for product id " + id, e);
    }
  }

  public List<Product> searchProductWithPriceGreaterThan(int priceBottomLimit) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
        .addValue("priceBottomLimit", priceBottomLimit);
    try {
      List<Product> products = jdbcTemplate.query("select id, priceinpence, imageindex, catalogindex, name, description " +
          "from productinfo where priceinpence>=:priceBottomLimit;", namedParameters, new ProductDataExtractor());
      return products;
    } catch (Exception e) {
      throw new RepositoryException("Can't complete the search for product with minimum price: " + priceBottomLimit, e);
    }
  }

  public List<Product> searchProductWithPriceLessThan(int priceupperLimit) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
        .addValue("priceBottomLimit", priceupperLimit);
    try {
      List<Product> products = jdbcTemplate.query("select id, priceinpence, imageindex, catalogindex, name, description " +
          "from productinfo where priceinpence<=:priceBottomLimit;", namedParameters, new ProductDataExtractor());
      return products;
    } catch (Exception e) {
      throw new RepositoryException("Can't complete the search for product with max price: " + priceupperLimit, e);
    }
  }


  @Override
  public void deleteProduct(int id) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
        .addValue("id", id);
    try {
      jdbcTemplate.update("delete from productinfo where id=:id;", namedParameters);
    } catch (Exception e) {
      throw new RepositoryException("Can't delete product with id: " + id);
    }
  }

  @Override
  public List<Product> searchProductByName(String name) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
        .addValue("name", name);
    try {
      List<Product> products = jdbcTemplate.query("select id, priceinpence, imageindex, catalogindex, name, description " +
          "from productinfo where name=:name;", namedParameters, new ProductDataExtractor());
      return products;
    } catch (Exception e) {
      throw new RepositoryException("Can't complete the search for product name " + name, e);
    }
  }

  @Override
  public List<Product> fuzzySearchProductByName(String name) throws RepositoryException {
    SqlParameterSource namedParameters = new MapSqlParameterSource()
        .addValue("name", "%" + name + "%");
    try {
      List<Product> products = jdbcTemplate.query("select id, priceinpence, imageindex, catalogindex, name, description " +
          "from productinfo where name like :name ;", namedParameters, new ProductDataExtractor());
      return products;
    } catch (Exception e) {
      throw new RepositoryException("Can't complete the fuzzy search for product name " + name, e);
    }
  }

  private class ProductDataExtractor implements ResultSetExtractor<List<Product>> {
    @Override
    public List<Product> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
      List<Product> products = new ArrayList<>();
      while (resultSet.next()) {
        int id = resultSet.getInt("id");
        int priceInPence = resultSet.getInt("priceInPence");
        int imageIndex = resultSet.getInt("imageIndex");
        int catalogIndex = resultSet.getInt("catalogIndex");
        String name = resultSet.getString("name").trim();
        String description = resultSet.getString("description").trim();
        Product product = new Product(id, priceInPence, imageIndex, catalogIndex, name, description);
        products.add(product);
      }
      return products;
    }
  }

}
