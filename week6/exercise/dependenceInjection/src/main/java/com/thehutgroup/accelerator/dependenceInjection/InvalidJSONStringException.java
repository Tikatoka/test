package com.thehutgroup.accelerator.dependenceInjection;

public class InvalidJSONStringException extends RuntimeException {
  public InvalidJSONStringException(String message) {
    super(message);
  }

  public InvalidJSONStringException(String message, Throwable cause) {
    super(message, cause);
  }
}
