package com.thehutgroup.accelerator.dependenceInjection.repositorymanager;

import com.thehutgroup.accelerator.dependenceInjection.Product;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.util.List;

public interface RepositoryManager {
  void registerProduct(Product product) throws RepositoryException;

  void updateProduct(int id, Product product) throws RepositoryException;

  Product searchProduct(int id) throws RepositoryException;

  void deleteProduct(int id) throws RepositoryException;

  List<Product> searchProductByName(String name) throws RepositoryException;

  List<Product> fuzzySearchProductByName(String name) throws RepositoryException;

  List<Product> searchProductWithPriceGreaterThan(int priceBottomLimit) throws RepositoryException;

  List<Product> searchProductWithPriceLessThan(int priceBottomLimit) throws RepositoryException;

  abstract class dataExtractor implements ResultSetExtractor {
  }
}
