package com.thehutgroup.accelerator.dependenceInjection;

import com.thehutgroup.accelerator.dependenceInjection.repositorymanager.PostgresRepositoryManager;
import com.thehutgroup.accelerator.dependenceInjection.repositorymanager.RepositoryException;
import com.thehutgroup.accelerator.dependenceInjection.repositorymanager.RepositoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ProductManager {
  @Autowired
  RepositoryManager repoManager;

  /**
   * Register a product to database
   *
   * @param product the Product object need to be written into db
   * @throws RuntimeException if there is connection issue or the id is already existed
   */
  public void registerAProduct(Product product) {
    try {
      repoManager.registerProduct(product);
    } catch (RepositoryException e) {
      throw new RuntimeException("Failed to register a new product with id: " + product.getId());
    }
  }

  /**
   * Delete the product in db by its id
   *
   * @param id the product id for which to be deleted
   * @throws RuntimeException is there is connection issue or the id is not existed
   */

  public void deleteProductById(int id) {
    try {
      repoManager.deleteProduct(id);
    } catch (RepositoryException e) {
      throw new RuntimeException("Failed to delete a product with id: " + id);
    }
  }


  /**
   * Search a product according to an id number
   *
   * @param id An integer of a product id
   * @return The product with the specified id or null if no match
   * @throws RuntimeException when there are more than two products or connection issue happens
   */
  public Product searchProductById(int id) {
    try {
      Product product = this.repoManager.searchProduct(id);
      return product;
    } catch (RepositoryException e) {
      throw new RuntimeException("Failed to search in the repository with product id" + id, e);
    }
  }


  /**
   * Search a product according to an id number
   *
   * @param name An string of name pattern
   * @return The products with the specified name
   * @throws RuntimeException when there are errors of connection
   */
  public List<Product> searchProductByName(String name) {
    try {
      List<Product> products = this.repoManager.searchProductByName(name);
      return products;
    } catch (RepositoryException e) {
      throw new RuntimeException("Failed to search in the repository with product name" + name, e);
    }
  }

  /**
   * Search a product according to an id number
   *
   * @param name An string of name pattern
   * @return The products which contains the name pattern
   * @throws RuntimeException when there are errors of connection
   */
  public List<Product> fuzzySearchProductByName(String name) {
    try {
      List<Product> products = this.repoManager.fuzzySearchProductByName(name);
      return products;
    } catch (RepositoryException e) {
      throw new RuntimeException("Failed to fuzzy search in the repository with product name" + name, e);
    }
  }


  /**
   * Updates the price of a product (specified by its id) with new value
   *
   * @param id           the id of which product to be updated
   * @param priceInPence the new price of the product
   */
  public void updataProductPriceById(int id, int priceInPence) {
    try {
      Product currentProduct = searchProductById(id);
      if (currentProduct == null) {
        System.out.println("The product with id:" + id + " does not existed in the system");
        return;
      }
      currentProduct.setPriceInPence(priceInPence);
      repoManager.updateProduct(id, currentProduct);
    } catch (RepositoryException e) {
      throw new RuntimeException("Failed to update new price: " + priceInPence
          + " to product with id: " + id, e);
    }
  }

  /**
   * search the products with price greater than the given limit
   *
   * @param priceBottomLimit the minimun price of a product
   * @return a products list with the price greater than the given limit
   */
  public List<Product> searchProductWithPriceGreaterThan(int priceBottomLimit) {
    try {
      List<Product> productSet = repoManager.searchProductWithPriceGreaterThan(priceBottomLimit);
      return productSet;
    } catch (RepositoryException e) {
      throw new RuntimeException("Failed to search products with ");
    }
  }

  /**
   * search the products with price less than the given limit
   *
   * @param priceUpLimit the minimun price of a product
   * @return a products list with the price less than the given limit
   */
  public List<Product> searchProductWithPriceLessThan(int priceUpLimit) {
    try {
      List<Product> productSet = repoManager.searchProductWithPriceLessThan(priceUpLimit);
      return productSet;
    } catch (RepositoryException e) {
      throw new RuntimeException("Failed to search products with ");
    }
  }

  /**
   * Search the products with price within the given range
   *
   * @param priceBottomLimit the minimum price
   * @param priceUpLimit     the max price
   * @return a list of products with price within range [priceBottomLimit, priceUpLimit]
   */
  public List<Product> searchProductWithinPriceRange(int priceBottomLimit, int priceUpLimit) {
    try {
      List<Product> productSetUplimit = searchProductWithPriceLessThan(priceUpLimit);
      List<Product> productSetBottom = searchProductWithPriceGreaterThan(priceBottomLimit);
      productSetUplimit.retainAll(productSetBottom);
      return productSetUplimit;
    } catch (RuntimeException e) {
      throw new RuntimeException("Failed to search products with price range [" + priceBottomLimit +
          ", " + priceUpLimit + "]");
    }
  }

}
