package com.thehutgroup.accelerator.dependenceInjection;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Objects;

public class Product {

  static final int HASH_KEY = 110;

  @JsonProperty("id")
  private int id;
  @JsonProperty("priceInPence")
  private int priceInPence;
  @JsonProperty("imageIndex")
  private int imageIndex;
  @JsonProperty("catalogIndex")
  private int catalogIndex;
  @JsonProperty("name")
  private String name;
  @JsonProperty("description")
  private String description;
  @JsonProperty("hash")
  private int hash;

  public Product(int id, int price_in_pence, int imageIndex,
                 int catalogIndex,
                 String name, String description) {
    this.id = id;
    this.priceInPence = price_in_pence;
    this.imageIndex = imageIndex;
    this.catalogIndex = catalogIndex;
    this.name = name;
    this.description = description;
  }

  @JsonCreator
  public Product(@JsonProperty("id") int id, @JsonProperty("priceInPence") int price_in_pence, @JsonProperty("imageIndex") int imageIndex,
                 @JsonProperty("catalogIndex") int catalogIndex,
                 @JsonProperty("name") String name, @JsonProperty("description") String description, @JsonProperty("hash") int hash) {
    this.id = id;
    this.priceInPence = price_in_pence;
    this.imageIndex = imageIndex;
    this.catalogIndex = catalogIndex;
    this.name = name;
    this.description = description;
    this.hash = hash;
  }

  public static Product fromJSON(String jsonString){
    ObjectMapper mapper = new ObjectMapper();
    try {
      Product product = mapper.readValue(jsonString, Product.class);
      int hash = product.hashCode();
      if (hash != product.hash) {
        throw new InvalidJSONStringException("The json data is not valid!");
      } else {
        return product;
      }
    } catch (IOException e) {
      System.out.println("failed to initialize a product with json string " + jsonString);
    }
    return null;
  }

  public int getId() {
    return id;
  }

  public int getPriceInPence() {
    return priceInPence;
  }

  public int getImageIndex() {
    return imageIndex;
  }

  public int getCatalogIndex() {
    return catalogIndex;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setPriceInPence(int priceInPence) {
    this.priceInPence = priceInPence;
  }

  public void setImageIndex(int imageIndex) {
    this.imageIndex = imageIndex;
  }

  public void setCatalogIndex(int catalogIndex) {
    this.catalogIndex = catalogIndex;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String toJSONString() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    try {
      this.hash = this.hashCode();
      String jsonString = mapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(this);
      return jsonString;
    } catch (JsonProcessingException e) {
      throw new Exception("Can't get the JSON string of this instance");
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Product product = (Product) o;
    return id == product.id &&
        priceInPence == product.priceInPence &&
        imageIndex == product.imageIndex &&
        catalogIndex == product.catalogIndex &&
        Objects.equals(name, product.name) &&
        Objects.equals(description, product.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(HASH_KEY, id, priceInPence, imageIndex, catalogIndex, name, description);
  }
}
