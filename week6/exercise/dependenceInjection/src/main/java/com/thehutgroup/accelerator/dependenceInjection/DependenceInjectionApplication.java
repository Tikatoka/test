package com.thehutgroup.accelerator.dependenceInjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DependenceInjectionApplication {




  @Autowired
  private ProductManager productManager;

  public static void main(String[] args) {
    SpringApplication.run(DependenceInjectionApplication.class, args);
  }

//  @Bean
//  public void registerAProduct() throws Exception {
////    Product tempProduct = new Product(3, 99, 1, 1, "mock", "not real");
//////    productManager.registerAProduct(tempProduct);
////    String str = tempProduct.toJSONString();
////    System.out.println(str);
////    Product secodProduct = Product.fromJSON(str);
////    System.out.println(secodProduct.getName());
//
//
//
////    Product p = productManager.searchBookById(1);
////    System.out.println(p.getName());
//
//  }

}

