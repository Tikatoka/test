create database products;
\connect products;

CREATE TABLE productInfo
(
  id             BIGINT PRIMARY KEY,
  priceInPence BIGINT     NOT NULL,
  imageIndex     BIGINT     NOT NULL,
  catalogIndex   INT        NOT NULL,
  name           CHAR(50)   NOT NULL,
  description    CHAR(5000) NOT NULL
);
