create database products;
\connect products;

CREATE TABLE productInfo
(
  id             BIGINT PRIMARY KEY,
  priceInPence BIGINT     NOT NULL,
  imageIndex     BIGINT     NOT NULL,
  catalogIndex   INT        NOT NULL,
  name           CHAR(50)   NOT NULL,
  description    CHAR(5000) NOT NULL
);

Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description)
values (1, 99, 1, 1, 'Dummy product', 'The product is here just for testing');

Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description)
values (2, 99, 1, 1, 'good lipstick', 'The product is here just for testing');

Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description)
values (3, 99, 1, 1, 'average lipstick', 'The product is here just for testing');

Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description)
values (4, 99, 1, 1, 'bad lipstick', 'The product is here just for testing');

Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description)
values (5, 199, 1, 1, 'Dummy product', 'The product is here just for testing');

Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description)
values (6, 80, 1, 1, 'cheap product', 'The product is here just for testing');

Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description)
values (7, 80, 1, 1, 'not existed product', 'The product is here just for testing');

Insert into productInfo (id, priceInPence, imageIndex, catalogIndex, name, description)
values (8, 80, 1, 1, 'not to be', 'The product is here just for testing');