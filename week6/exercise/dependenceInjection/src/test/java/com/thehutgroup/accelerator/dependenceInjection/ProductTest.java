package com.thehutgroup.accelerator.dependenceInjection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductTest {
  @Test
  public void ProductToJSONStringTest() throws Exception {
    Product tempProduct = new Product(1, 99, 1, 1, "Dummy product",
        "The product is here just for testing");
    String jsonString = tempProduct.toJSONString();
    String expectedString = "{\n" +
        "  \"id\" : 1,\n" +
        "  \"priceInPence\" : 99,\n" +
        "  \"imageIndex\" : 1,\n" +
        "  \"catalogIndex\" : 1,\n" +
        "  \"name\" : \"Dummy product\",\n" +
        "  \"description\" : \"The product is here just for testing\",\n" +
        "  \"hash\" : 1757030455\n" +
        "}";
    assertEquals(expectedString, jsonString);
  }

  @Test
  public void JSONStringToProductTest() throws Exception {
    String jsonString = "{\n" +
        "  \"id\" : 1,\n" +
        "  \"priceInPence\" : 99,\n" +
        "  \"imageIndex\" : 1,\n" +
        "  \"catalogIndex\" : 1,\n" +
        "  \"name\" : \"Dummy product\",\n" +
        "  \"description\" : \"The product is here just for testing\",\n" +
        "  \"hash\" : 1757030455\n" +
        "}";
    Product recoveredProduct = Product.fromJSON(jsonString);
    Product expectedProduct = new Product(1, 99, 1, 1, "Dummy product",
        "The product is here just for testing");
    assertEquals(expectedProduct, recoveredProduct);
  }

  @Test(expected = InvalidJSONStringException.class)
  public void InvalidJSONStringToProductTest(){
    // the price should be 99 with the given hash, now provided as 199 instead
    String jsonString = "{\n" +
        "  \"id\" : 1,\n" +
        "  \"priceInPence\" : 199,\n" +
        "  \"imageIndex\" : 1,\n" +
        "  \"catalogIndex\" : 1,\n" +
        "  \"name\" : \"Dummy product\",\n" +
        "  \"description\" : \"The product is here just for testing\",\n" +
        "  \"hash\" : 1757030455\n" +
        "}";
    Product recoveredProduct = Product.fromJSON(jsonString);
  }

}
