package com.thehutgroup.accelerator.dependenceInjection;

import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.connection.waiting.HealthChecks;
import com.thehutgroup.accelerator.dependenceInjection.repositorymanager.PostgresRepositoryManager;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
//@Component
public class DependenceInjectionApplicationTests {

//  @Mock
//  PostgresRepositoryManager postgresRepositoryManager;
//
//  @InjectMocks
//  ProductManager productManager;

  @Autowired
  ProductManager realManager;

  @ClassRule
  public static DockerComposeRule docker = DockerComposeRule.builder()
      .file("src/test/resources/docker-compose-test.yaml")
      .waitingForService("db", HealthChecks.toHaveAllPortsOpen())
      .build();

//  @Test
//  public void ProductManagerMockSearchTest() throws Exception {
//    Product tempProduct = new Product(1, 99, 1, 1, "mock", "not real");
//    when(postgresRepositoryManager.searchProduct(1)).thenReturn(tempProduct);
//    Product product = productManager.searchProductById(1);
//    assertTrue(product.equals(tempProduct));
//  }

  @AfterClass
  synchronized public static void tearDown() throws Exception {
//    System.out.println("tear down");
//    Runtime.getRuntime().exec("docker rm `docker ps | grep postgres:11.1-alpine | cut -d \" \" -f 1 ` -f");
  }

//  @BeforeClass
//  public static void setUp() throws IOException {
//    Runtime.getRuntime().exec("docker-compose -f src/test/resources/docker-compose-test.yaml down");
//  }

  @Test
  public void ProductManagerSearchTest1() throws Exception {
    Product tempProduct = new Product(1, 99, 1, 1, "Dummy product",
        "The product is here just for testing");
    Product product = realManager.searchProductById(1);
    assertTrue(tempProduct.equals(product));
  }

  @Test
  public void registerTest() throws Exception {
    Product tempProduct = new Product(11, 10000, 1, 1, "!!!",
        "The product is here just for testing");
    realManager.registerAProduct(tempProduct);
    Product product = realManager.searchProductById(11);
    assertEquals(tempProduct, product);
  }

//  @Test
//  public void ProductManagerMockSearchTest2() throws Exception {
//    when(postgresRepositoryManager.searchProduct(99)).thenReturn(null);
//    Product product = productManager.searchProductById(99);
//    assertNull(product);
//  }

  @Test
  public void ProductManagerSearchTest2() {
    Product product = realManager.searchProductById(99);
    assertNull(product);
  }

  @Test
  public void ProductManagerSearchNameTest1() {
    List<Product> products = realManager.searchProductByName("good lipstick");
    List<Product> expectedReturn = Arrays.asList(
        new Product(2, 99, 1, 1, "good lipstick", "The product is here just for testing")
    );
    assertArrayEquals("Search with one return product", expectedReturn.toArray(), products.toArray());
  }

  @Test
  public void ProductManagerSearchNameTest2() {
    List<Product> products = realManager.searchProductByName("Dummy product");
    List<Product> expectedReturn = Arrays.asList(
        new Product(1, 99, 1, 1, "Dummy product", "The product is here just for testing"),
        new Product(5, 199, 1, 1, "Dummy product", "The product is here just for testing")
    );
    assertArrayEquals("Search with two return products with same name", expectedReturn.toArray(), products.toArray());
  }


  @Test
  public void ProductManagerFuzzySearchNameTest() throws Exception {
    List<Product> products = realManager.fuzzySearchProductByName("lipstick");
    List<Product> expectedReturn = Arrays.asList(
        new Product(2, 99, 1, 1, "good lipstick", "The product is here just for testing"),
        new Product(3, 99, 1, 1, "average lipstick", "The product is here just for testing"),
        new Product(4, 99, 1, 1, "bad lipstick", "The product is here just for testing")
    );
    assertArrayEquals("Search with three return products with same name pattern", expectedReturn.toArray(), products.toArray());
  }

  @Test
  public void ProductManagerUpdatePriceById() {
    realManager.updataProductPriceById(7, 999);
    Product product = realManager.searchProductById(7);
    Product expected = new Product(7, 999, 1, 1, "not existed product", "The product is here just for testing");
    assertTrue("The price of product with id 7 should be 999", expected.equals(product));
  }

  @Test
  public void ProductManagerPriceRangeSearchTest1() {
    List<Product> products = realManager.searchProductWithinPriceRange(100, 200);
    List<Product> expectedReturn = Arrays.asList(
        new Product(5, 199, 1, 1, "Dummy product", "The product is here just for testing")
    );
    assertArrayEquals("Search price range within a range, return 1 product", expectedReturn.toArray(), products.toArray());
  }

  @Test
  public void ProductManagerDeleteTest() {
    realManager.deleteProductById(8);
    Product product = realManager.searchProductById(8);
    assertNull(product);
  }
}

