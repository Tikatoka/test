# Week 6 Exercise
## Designing an application to store product information
Design an application that could be used in THG's e-commerce platform for allowing product information to be created, retrieved, updated and deleted (CRUD)

Think about how you would like to model a product -- what kind of product data do you think you need to store to satisfy the needs of a product page?

Your solution should use a dependency injection technique presented in week5 and maven or gradle as your build tool.

Write code snippets that demonstrate the CRUD operations on products.

Write tests including mocks.

You are free to store the products in memory, on the file system, or in a database (if you provide a `docker-compose.yaml` to spin up your database :) )

Feel free to use the code [here](https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/tree/master/week5/dependency-injection-demos/) as a starting point.

## Optional 1
Think about multi-lingual modelling -- how can we store/retrieve translated product information?

## Optional 2
Install docker-compose and follow the example in [spring-boot](https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/tree/master/week5/dependency-injection-demos/spring-boot) to use a postgres database as your data store.