package com.thehutgroup.accelerator;

import java.util.ArrayList;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testFibonacci()
    {
        Fibonacci fib = new Fibonacci();
        ArrayList<Integer> one_element = new ArrayList<Integer>();
        one_element.add(1);
        assertTrue("Output with only 1 element", one_element.equals(fib.getResult(1)));
        ArrayList<Integer> two_element = new ArrayList<Integer>();
        two_element.add(1);
        two_element.add(2);
        assertTrue("Output with only 1 element", two_element.equals(fib.getResult(2)));
        ArrayList<Integer> three_element = new ArrayList<Integer>();
        three_element.add(1);
        three_element.add(2);
        three_element.add(3);
        assertTrue("Output with only 1 element", three_element.equals(fib.getResult(3)));
    }


    @Test
    public void testHourglass(){
        Hourglass hour = new Hourglass();
        String string_level_1 = "*\n";
        String string_level_3 = "*****\n" +
                " *** \n" +
                "  *  \n" +
                " *** \n" +
                "*****\n";
        String string_level_7 = "*************\n" +
                " *********** \n" +
                "  *********  \n" +
                "   *******   \n" +
                "    *****    \n" +
                "     ***     \n" +
                "      *      \n" +
                "     ***     \n" +
                "    *****    \n" +
                "   *******   \n" +
                "  *********  \n" +
                " *********** \n" +
                "*************\n";
        assertEquals("level 7", string_level_7, hour.getResult(7));
        assertEquals("level 3", string_level_3, hour.getResult(3));
        assertEquals("level 1", string_level_1, hour.getResult(1));
    }

    @Test
    public void testPalindrome(){
        Palindrome pal = new Palindrome();
        assertTrue("single character char", pal.getResult("a"));
        assertTrue("single character int", pal.getResult(7));
        assertTrue("two characters aa", pal.getResult("aa"));
        assertTrue("two characters ab", !pal.getResult("ab"));
        assertTrue("two characters with space", pal.getResult("a a"));
        assertTrue("two characters with mix of upper and lower", pal.getResult("aA"));
        assertTrue("two characters with mix of upper and lower and space", pal.getResult("a A"));
    }
}
