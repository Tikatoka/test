package com.thehutgroup.accelerator;
import java.util.ArrayList;

public class Fibonacci {
    public ArrayList<Integer> getResult(int count){
        ArrayList<Integer> result = new ArrayList<Integer>();
        if (count >= 1){
            result.add(1);
        }
        if (count >= 2){
            result.add(1,2);
        }
        if (count >= 3){
            // initialise the count
            // initialise the first two elements of a fib
            int i = 2;
            int a = 1;
            int b = 2;
            while (i < count){
                // calculate the next element
                int temp = b;
                b = a + b;
                a = temp;
                // push the new element into result array
                result.add(b);
                // update the loop control variable
                i++;
            }
        }
        return result;

    }
}
