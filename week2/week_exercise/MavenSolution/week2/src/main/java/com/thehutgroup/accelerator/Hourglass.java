package com.thehutgroup.accelerator;
public class Hourglass {
    public String getResult(int level){
        // declear the result string
        String result = "";

        // for a given level number, the stats is 2 * level - 1
        // the max number of starts occurs on the very top layer
        int maxStars = 2 * level - 1;
        // build the string from top layer
        // the loop index is layer - 1 for easy handling of both sides
        for (int i= level - 1; i > -level; i--){
            int numStars = 2 * Math.abs(i) + 1;
            int numSpaces = maxStars - numStars;
            // prepare the space string
            String space = new String(new char[numSpaces/2]).replace("\0", " ");
            // prepare the star string
            String star = new String(new char[numStars]).replace("\0", "*");
            result += space + star + space+'\n';
        }
        return result;
    }
}
