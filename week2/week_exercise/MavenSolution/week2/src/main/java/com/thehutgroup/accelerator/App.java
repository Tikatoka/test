package com.thehutgroup.accelerator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main ( String[] args )throws IOException
    {
        System.out.println("Please select the program to run:\n" +
                "1. Fibonacci\n2. Palindrome\n3. Hourglass\n");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();

        switch(s) {
            case "1":
                System.out.println("How many fib number do you want: ");
                s = br.readLine();
                Fibonacci fibSolution = new Fibonacci();
                ArrayList<Integer> result = fibSolution.getResult(Integer.parseInt(s));
                System.out.println("The answer of fib with n=" + s + " is:");
                System.out.println(result.toString());
                break;

            case "2":
                System.out.println("Input a string for palindrome test:");
                s = br.readLine();
                Palindrome pal = new Palindrome();
                boolean case_1 = pal.getResult(s);
                System.out.println("Palindrome test is: " + case_1);
                break;

            case "3":
                System.out.println("How many levels do you want:");
                s = br.readLine();
                Hourglass hou = new Hourglass();
                String hg = hou.getResult(Integer.parseInt(s));
                System.out.println(hg);
                break;

            default:
                System.out.println("Your choice is not supported");
                break;
        }
    }
}
