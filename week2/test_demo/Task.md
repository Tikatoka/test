# Testing Task
## Compulsory
This project is based on the game of [Draughts](https://en.wikipedia.org/wiki/Draughts)

There is currently a failing test in the project -- PawnMovementDoubleCaptureTest.

Implement the logic in the Pawn class so that any number of captures are possible and the test passes

## Optional 1
Implement the King piece and add tests for them

## Optional 2
Write a class, DraughtsGame, that allows users to move pieces, resign, and recognise game over

## Optional 3
Write a basic draughts AI.