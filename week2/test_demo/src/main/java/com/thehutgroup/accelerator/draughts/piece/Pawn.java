package com.thehutgroup.accelerator.draughts.piece;

import com.thehutgroup.accelerator.draughts.Colour;
import com.thehutgroup.accelerator.draughts.DraughtsBoard;
import com.thehutgroup.accelerator.draughts.Move;
import com.thehutgroup.accelerator.draughts.Position;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.List;

public class Pawn extends DraughtsPiece {
  public Pawn(Colour colour) {
    super(colour);
  }

  @Override
  public Set<Move> possibleMoves(Position position, DraughtsBoard board) {
    Move move = new Move(position.getX(), position.getY());
    return this.searchMoves(move, board, 99, true);
  }

  public boolean isKing() {
    return false;
  }

  // search the possible moves
  protected Set<Move> searchMoves(Move position, DraughtsBoard board, int depth, boolean startSearch) {
    if (depth < 1) {
      // return empty hashSet if the depth is less than 1
      return new HashSet<>();
    }
    Set<Move> potentialMoves = this.getPotentialMoves(position);
    Set<Move> moves = new HashSet<>();
    for (Move move : potentialMoves) {
      switch (this.validMoveCheck(position, move, board)) {
        case 1:
          if (startSearch) {
            moves.add(move);
          }
          break;
        case 2:
          int newTargetPositionX = (move.getX() - position.getX()) + move.getX();
          int newTargetPositionY = (move.getY() - position.getY()) + move.getY();
          Move nextSearch = new Move(newTargetPositionX, newTargetPositionY);
          nextSearch.copyRoute(position);
          nextSearch.captureOne(move);
          Set<Move> furtherSearch = this.searchMoves(nextSearch,
              board, depth - 1, false);
          moves.add(nextSearch);
          moves.addAll(furtherSearch);
          break;
      }
    }
    return moves;
  }

  protected Set<Move> getPotentialMoves(Move position) {
    Set<Move> moves = new HashSet<>();
    Function<Integer, Function<Integer, Integer>> yMover;

    switch (this.getColour()) {
      case BLACK:
        yMover = y -> d -> y - d;
        break;
      case WHITE:
        yMover = y -> d -> y + d;
        break;
      default:
        throw new RuntimeException("invalid colour");
    }
    for (int x = -1; x <= 1; x = x + 2) {
      int potentialX = position.getX() + x;
      int potentialY = yMover.apply(position.getY()).apply(1);
      Move potentialMove = new Move(potentialX, potentialY);
      moves.add(potentialMove);
    }
    return moves;
  }

  protected int validMoveCheck(Move current, Move target, DraughtsBoard board) {
    // check weather a move is valid or not
    // Return 0: not valid, 1 valid as normal move or 2 valid as capture move

    // target position is empty, can perform a normal move
    if (board.positionVacant(target)) {
      return 1;
    }
    // target position has other color, but there is empty position behind it, can capture
    if (board.positionOccupied(Colour.getOther(this.getColour()), target, current.getCaptures())) {
      int deltaX = target.getX() - current.getX();
      int deltaY = target.getY() - current.getY();
      Position po_behind = new Position(target.getX() + deltaX, target.getY() + deltaY);
      if (board.positionVacant(po_behind)) {
        return 2;
      }
    }
    // Rest cases, return 0
    return 0;
  }


}
