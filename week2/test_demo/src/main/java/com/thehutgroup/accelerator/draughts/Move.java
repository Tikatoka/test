package com.thehutgroup.accelerator.draughts;

import java.util.HashSet;
import java.util.Set;

public class Move extends Position {
    private Set<Position> captures = new HashSet<>();

    public Move(int x, int y) {
        super(x, y);
    }


    public void copyRoute(Move move) {
        this.captures.clear();
        for (Position position : move.captures) {
            int positionX = position.getX();
            int positionY = position.getY();
            Position newPo = new Position(positionX, positionY);
            this.captures.add(newPo);
        }
    }

    public Set<Position> getCaptures() {
        return captures;
    }

    public void captureOne(Position position) {
        Position newPo = new Position(position.getX(), position.getY());
        this.captures.add(newPo);
    }


}
