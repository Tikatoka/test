package com.thehutgroup.accelerator.draughts.piece;

import com.thehutgroup.accelerator.draughts.DraughtsBoard;
import com.thehutgroup.accelerator.draughts.Colour;
import com.thehutgroup.accelerator.draughts.Move;
import com.thehutgroup.accelerator.draughts.Position;

import java.util.Set;

public abstract class DraughtsPiece {
  private Colour colour;

  public DraughtsPiece(Colour colour) {
    this.colour = colour;
  }

  public Colour getColour() {
    return this.colour;
  }

  public abstract boolean isKing();

  public abstract Set<Move> possibleMoves(Position position, DraughtsBoard board);
}

