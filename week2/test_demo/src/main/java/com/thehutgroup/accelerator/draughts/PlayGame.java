package com.thehutgroup.accelerator.draughts;

import com.thehutgroup.accelerator.draughts.GameRunner;

import java.io.IOException;

public class PlayGame {
  public static void main(String[] args) throws IOException {
    boolean whiteAI = false;
    boolean blackAI = true;
    if (args.length > 0 && args[0].equals("1")) {
      whiteAI = true;
    }
    if (args.length > 1 && args[1].equals("1")) {
      blackAI = true;
    }
    GameRunner runner = new GameRunner();
    runner.runGame(whiteAI, blackAI);
  }
}
