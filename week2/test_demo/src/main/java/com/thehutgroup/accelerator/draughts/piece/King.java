package com.thehutgroup.accelerator.draughts.piece;

import com.thehutgroup.accelerator.draughts.Colour;
import com.thehutgroup.accelerator.draughts.Move;

import java.util.HashSet;
import java.util.Set;

public class King extends Pawn {
    public King(Colour colour) {
        super(colour);
    }

    @Override
    public boolean isKing() {
        return true;
    }

    @Override
    protected Set<Move> getPotentialMoves(Move position) {
        Set<Move> moves = new HashSet<>();
        for (int x = -1; x <= 1; x = x + 2) {
            for (int y = -1; y <= 1; y = y + 2) {
                int potentialX = position.getX() + x;
                int potentialY = position.getY() + y;
                Move potentialMove = new Move(potentialX, potentialY);
                moves.add(potentialMove);
            }
        }
        return moves;
    }
}
