package com.thehutgroup.accelerator.draughts;


import com.thehutgroup.accelerator.draughts.piece.DraughtsPiece;
import com.thehutgroup.accelerator.draughts.piece.King;
import com.thehutgroup.accelerator.draughts.piece.Pawn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.Set;

public class GameRunner {
  public void runGame(boolean wAI, boolean bAI) throws IOException {
    Colour currentTurn = Colour.WHITE;
    Pawn BP = new Pawn(Colour.BLACK);
    Pawn WP = new Pawn(Colour.WHITE);
    DraughtsPiece[][] pieces = new DraughtsPiece[][] {
        //y=8
        {null, BP, null, BP, null, BP, null, BP},
        {BP, null, BP, null, BP, null, BP, null},
        {null, BP, null, BP, null, BP, null, BP},
        {null, null, null, null, null, null, null, null},
        {null, null, null, null, null, null, null, null},
        {WP, null, WP, null, WP, null, WP, null},
        {null, WP, null, WP, null, WP, null, WP},
        {WP, null, WP, null, WP, null, WP, null}
        //y=0,x=0                                 //x=8
    };
    pieces = this.reverse(pieces);
    DraughtsBoard board = new DraughtsBoard(pieces);

    do {
      this.evalueBoard(board);
      this.printBoard(board);
      System.out.println("Current turn is " + currentTurn);
      if ((wAI && currentTurn == Colour.WHITE) || (bAI && currentTurn == Colour.BLACK)) {
        this.AImove(board, currentTurn);
        currentTurn = Colour.getOther(currentTurn);
      } else {
        Position piece = this.selectOnePiece(board, currentTurn);
        System.out.println("Your Input is " + piece);
        Move move = this.choiceOneMove(board, piece);
        System.out.println("Your Move is " + move);
        if (move.getY() == 99 && move.getX() == 99) {
          System.out.println("Reselect");
        } else {
          this.takeMove(board, move, piece, currentTurn);
          currentTurn = Colour.getOther(currentTurn);
        }
      }
    } while (!this.gameOverCheck(board));
  }

  public boolean gameOverCheck(DraughtsBoard board) {
    //game over condition:
    // 1. one color has no piece left
    // 2. one color got no possible move
    DraughtsPiece[][] currentBoard = board.getBoard();
    boolean blackHasPiece = false;
    boolean whiteHasPiece = false;
    boolean blackHasMove = false;
    boolean whiteHasMove = false;
    for (int i = 0; i < currentBoard.length; i++) {
      for (int j = 0; j < currentBoard[i].length; j++) {
        DraughtsPiece piece = currentBoard[i][j];
        if (piece == null) {
          continue;
        }
        switch (piece.getColour()) {
          case BLACK:
            blackHasPiece = true;
            if (!blackHasMove) {
              Set<Move> move = piece.possibleMoves(new Position(j, i), board);
              if (!move.isEmpty()) {
                blackHasMove = true;
              }
            }
            break;
          case WHITE:
            whiteHasPiece = true;
            if (!whiteHasMove) {
              Set<Move> move = piece.possibleMoves(new Position(j, i), board);
              if (!move.isEmpty()) {
                whiteHasMove = true;
              }
            }
            break;
        }
        if (blackHasPiece && whiteHasPiece && blackHasMove && whiteHasMove) {
          return false;
        }
      }
    }
    return true;
  }

  private void printBoard(DraughtsBoard board) {
    DraughtsPiece[][] currentBoard = this.reverse(board.getBoard());
    for (int i = 0; i < currentBoard.length; i++) {
      for (int j = 0; j < currentBoard[i].length; j++) {
        DraughtsPiece piece = currentBoard[i][j];
        if (piece == null) {
          if ((i % 2 - j % 2) == 0) {
            System.out.print("    ");
          } else {
            System.out.print(" __ ");
          }
        } else {
          String name = " ";
          switch (piece.getColour()) {
            case WHITE:
              name += "W";
              break;
            case BLACK:
              name += "B";
              break;
          }
          if (piece.isKing()) {
            name += "K ";
          } else {
            name += "P ";
          }
          System.out.print(name);
        }
      }
      System.out.print("\n");
    }
    this.reverse(board.getBoard());
  }

  private DraughtsPiece[][] reverse(DraughtsPiece[][] pieces) {
    for (int i = 0; i < pieces.length / 2; i++) {
      DraughtsPiece[] swap = pieces[i];
      pieces[i] = pieces[pieces.length - i - 1];
      pieces[pieces.length - i - 1] = swap;
    }
    return pieces;
  }

  private Position selectOnePiece(DraughtsBoard board, Colour colour) throws IOException {
    Position choice = new Position(-1, -1);
    do {
      System.out.print("Please chose one " + colour + " piece (e.g. \"0 0\" for the bottom left connor):");
      choice = this.getPositionInput();
    } while (!board.positionOccupied(colour, choice));
    return choice;
  }

  private Position getPositionInput() throws IOException {
    Position choice = new Position(-1, -1);
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String s = br.readLine();
    String[] arr = s.split("\\s+");
    try {
      if (arr[0].equals("q")) {
        return new Position(99, 99);
      }
      int positionX = Integer.parseInt(arr[0]);
      int positionY = Integer.parseInt(arr[1]);
      choice = new Position(positionX, positionY);
    } catch (Exception e) {
      System.out.println("Invalid input. Please check your input!");
    }
    return choice;
  }

  private Move choiceOneMove(DraughtsBoard board, Position position) throws IOException {
    // choose one move
    // if the chosen one is not valid, ask the cuser
    int X = position.getX();
    int Y = position.getY();
    Position choice = new Position(-1, -1);
    Set<Move> validMoves = board.getBoard()[Y][X].possibleMoves(position, board);
    do {
      System.out.print("Please chose one position to move (e.g. \"0 0\" for the bottom left connor) or q to reselect:");
      choice = this.getPositionInput();
      if ((choice.getX() == 99) && (choice.getY() == 99)) {
        return new Move(99, 99);
      }
    } while (!validMoves.contains(choice));
    for (Move move : validMoves) {
      if (move.equals(choice)) {
        return move;
      }
    }
    return null;
  }

  private void takeMove(DraughtsBoard board, Move move, Position startPosition, Colour colour) {
    // update the board according to the given move
    Set<Position> captures = move.getCaptures();
    for (Position cap : captures) {
      board.getBoard()[cap.getY()][cap.getX()] = null;
    }
    board.getBoard()[startPosition.getY()][startPosition.getX()] = null;
    board.getBoard()[move.getY()][move.getX()] = new Pawn(colour);
  }

  public void evalueBoard(DraughtsBoard board) {
    // check and convert pawn to king
    DraughtsPiece[] whiteKingLine = board.getBoard()[7];
    DraughtsPiece[] blackKingline = board.getBoard()[0];
    for (int i = 0; i < whiteKingLine.length; i++) {
      DraughtsPiece piece = whiteKingLine[i];
      if (piece != null && piece.getColour() == Colour.WHITE) {
        whiteKingLine[i] = new King(piece.getColour());
      }
    }

    for (int i = 0; i < blackKingline.length; i++) {
      DraughtsPiece piece = blackKingline[i];
      if (piece != null && piece.getColour() == Colour.BLACK) {
        blackKingline[i] = new King(piece.getColour());
      }
    }
  }

  private int AImove(DraughtsBoard board, Colour colour) {
    // AI will take the first available move
    DraughtsPiece[][] currentBoard = board.getBoard();
    for (int i = 0; i < currentBoard.length; i++) {
      for (int j = 0; j < currentBoard[i].length; j++) {
        DraughtsPiece piece = currentBoard[i][j];
        if (piece != null && piece.getColour() == colour) {
          Set<Move> moves = piece.possibleMoves(new Position(j, i), board);
          for (Move move : moves) {
            Position startP = new Position(j, i);
            this.takeMove(board, move, startP, colour);
            return 0;
          }
        }
      }
    }
    return 0;
  }
}
