package com.thehutgroup.accelerator.draughts.piece;

import com.thehutgroup.accelerator.draughts.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PawnTest {
    private static Pawn BP = new Pawn(Colour.BLACK);
    private static Pawn WP = new Pawn(Colour.WHITE);
    private static King BK = new King(Colour.BLACK);
    private static King WK = new King(Colour.WHITE);

    @Test
    public void PawnMovementStartTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=8
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, BP  , null, BP  , null, BP  , null},
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {WP  , null, WP  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=8
        }));

        Set<Move> moves = WP.possibleMoves(new Position(1, 1), start);
        assertEquals("should have no options",
                new HashSet<>(),
                moves);

        moves = WP.possibleMoves(new Position(0, 2), start);
        assertEquals("should have one option",
                new HashSet<>(Arrays.asList(new Move(1, 3))),
                moves);


        moves = WP.possibleMoves(new Position(2, 2), start);
        assertEquals("should have two options",
                new HashSet<>(Arrays.asList(new Move(3, 3), new Move(1, 3))),
                moves);



    }

    @Test
    public void PawnMovementSingleCaptureTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=8
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, BP  , null, BP  , null, BP  , null},
                {null, BP  , null, BP  , null, null, null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, BP  , null, null, null, null},
                {WP  , null, WP  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=8
        }));


        Set<Move> moves = WP.possibleMoves(new Position(2, 2), start);
        assertEquals("should have one option to capture and one to move",
                new HashSet<>(Arrays.asList(new Move(4, 4), new Move(1, 3))),
                moves);


    }

    @Test
    public void PawnMovementDoubleCaptureTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=7
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, null, null, BP  , null, null, null},
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, BP  , null, null, null, null},
                {WP  , null, WP  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=7
        }));


        Set<Move> moves = WP.possibleMoves(new Position(2, 2), start);
        assertEquals("should have three options to capture and one to move",
                new HashSet<>(Arrays.asList(new Move(4, 4),
                        new Move(1, 3),
                        new Move(6, 6),
                        new Move(2, 6)
                )),
                moves);


    }


    @Test
    public void KingMovementStartTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=8
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, BP  , null, BP  , null, BP  , null},
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, WK, null, null, null},
                {WK , null, WK  , null, null  , null, WP  , null},
                {null, WK  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=8
        }));

        Set<Move> moves = WK.possibleMoves(new Position(1, 1), start);
        assertEquals("king should have no options",
                new HashSet<>(),
                moves);

        moves = WK.possibleMoves(new Position(0, 2), start);
        assertEquals("king should have one option",
                new HashSet<>(Arrays.asList(new Move(1, 3))),
                moves);


        moves = WK.possibleMoves(new Position(2, 2), start);
        assertEquals("king should have two options",
                new HashSet<>(Arrays.asList(new Move(3, 3), new Move(1, 3))),
                moves);

        moves = WK.possibleMoves(new Position(4, 3), start);
        assertEquals("king should have four options",
                new HashSet<>(Arrays.asList(new Move(3, 2), new Move(3, 4), new Move(5, 2), new Move(5, 4))),
                moves);
    }


    @Test
    public void KingMovementSingleCaptureTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=8
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, BP  , null, BP  , null, WK  , null},
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, BP  , null, null, null, null},
                {WP  , null, WK  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=8
        }));


        Set<Move> moves1 = WK.possibleMoves(new Position(6, 6), start);
        assertEquals("should have one option to capture",
                new HashSet<>(Arrays.asList(new Move(4, 4))),
                moves1);

        Set<Move> moves2 = WK.possibleMoves(new Position(2, 2), start);
        assertEquals("should have one option to capture and one to move",
                new HashSet<>(Arrays.asList(new Move(4, 4), new Move(1, 3))),
                moves2);

    }

    @Test
    public void KingMovementDoubleCaptureTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=7
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, null, null, BP  , null, null, null},e
                {null, BP  , null, WP  , null, WP  , null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, WP  , null, null, null, null},
                {WP  , null, BK  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=7
        }));


        Set<Move> moves = BK.possibleMoves(new Position(2, 2), start);
        assertEquals("should have three options to capture and one to move",
                new HashSet<>(Arrays.asList(new Move(4, 4),
                        new Move(1, 3),
                        new Move(6, 6),
                        new Move(2, 6)
                )),
                moves);


    }


    @Test
    public void EvalueBoard() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=8
                {null, WP  , null, BP  , null, WP  , null, BP  },
                {BP  , null, BP  , null, BP  , null, BP  , null},
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {WP  , null, WP  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=8
        }));

        GameRunner runner = new GameRunner();
        runner.evalueBoard(start);
        DraughtsPiece piece_1 = start.getBoard()[7][1];
        DraughtsPiece piece_2 = start.getBoard()[7][5];
        DraughtsPiece piece_3 = start.getBoard()[7][3];

        assertTrue("One king should be on 1, 7", piece_1.isKing());
        assertTrue("One king should be on 5, 7", piece_2.isKing());
        assertTrue("Black piece on 3, 7 should not be a king", !piece_3.isKing());
    }


    @Test
    public void GameOverTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=8
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {null, null, null, null, WP  , null, WP  , null},
                {null, null, null, WP  , null, WP  , null, WP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null  , null, WP  , null, WP  , null},
                {null, null, null, WP  , null, WP  , null, WP  },
                {null, null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=8
        }));

        DraughtsBoard start2 = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=8
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {null, null, null, null, WP  , null, WP  , null},
                {null, null, null, WP  , null, WP  , null, WP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, WP  , null, WP  , null},
                {null, null, null, WP  , null, WP  , WP  , WP  },
                {null, null, WP  , null, WP  , null, WP  , BP  }
                //y=0,x=0                                 //x=8
        }));
        GameRunner runner = new GameRunner();
        boolean over = runner.gameOverCheck(start2);
        assertTrue("Game over: black can not move", over);
    }

    private DraughtsPiece[][] reverse(DraughtsPiece[][] pieces) {
        for (int i = 0; i < pieces.length / 2; i++) {
            DraughtsPiece[] swap = pieces[i];
            pieces[i] = pieces[pieces.length - i - 1];
            pieces[pieces.length - i - 1] = swap;
        }
        return pieces;
    }
}
