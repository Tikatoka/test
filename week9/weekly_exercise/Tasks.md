# REST Exercise
## Product Microservice
Build a REST level 2 spring boot product microservice that supports the following operations:
* GET /product/{id}
* PUT /product/{id}
* DELETE /product/{id}
* POST /product (create product; id should not be specified by the client -- server should return the URI of the product created in the Location header)

You can use the product service backend you built in week 6 or start from scratch.
Feel free to use your choice of data store -- e.g. postgres or in memory (check Rehman's code example for in-memory data).

Make sure that you use the correct response codes and handle errors with an appropriate response message. Use Exception handling in case of a request failure.

Make sure your code is thread-safe and has sufficient test coverage.

Your code should have the layers mentioned in week 8, lecture 2, and you should keep your internal model and JSON POJO classes separate.

Achieve full REST glory by reaching level 3 on the Richardson Maturity Model with a self-describing API. You can look into the syntax from TaskController.java from project "MyRestApplication"


# Optional 1

Write a REST test that involves starting up your application in the test. See week8/demos for an example.





