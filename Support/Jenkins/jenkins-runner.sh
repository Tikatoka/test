# workaround for ruby runtime install issue
# per http://stackoverflow.com/questions/33883551/error-installing-ruby-runtime-in-jenkins
javaArgs="$javaArgs -Dhudson.ClassicPluginStrategy.noBytecodeTransformer=true"