# Basic Calculator

* Create two Java Classes with names "Calculator" and "MainClass" respectively. In the first class, create four methods for addition, subtraction, multiplication and division of two integers. 
* Add the main() method in the second class that takes two integers as an input and add/subtract/multiply/subtract them, based on the mathematical operation that the user wants to execute. 
* Both integers and the required mathematical operation should be inputted at run time.
* Print the result to the console (stdout)
* Use your prefered text editor, but not an IDE -- run from the command line only
* Handle division as integer or floating point, whichever you prefer

# Example Operation
```
$ java MainClass 3 + 2
$ 5
```

# Hints
* Explore how to take user input in Java
* Explore how to call methods of one class from another
* You need to have JDK and JRE installed in your machine for compiling a Java program (version 10)

