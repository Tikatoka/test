import java.io.BufferedReader;
import java.io.IOException;
import java.io.*;

public class Main {


    // calculate the result according to the given number and operator
    public static void main(String[] args)throws IOException {
        // 3 arguments are required for two number and one operator
        if (args.length != 3){
            System.out.println("3 input arguments must be provided");
        } else {
            try{
                // try to convert 1, 3 args to number
                // exception means the argument is not a number
                double value_1 = Double.valueOf(args[0]);
                double value_2 = Double.valueOf(args[2]);
                double result = 0.0;
                String validOperations = "+-*/";
                // make sure the operator are valid
                if (validOperations.indexOf(args[1]) == -1 || args[1].length() != 1 ){
                    System.out.println("Invailid operator!");
                } else {
                    MySecondClass cal = new MySecondClass();
                    cal.calculate(value_1, value_2, args[1].toString());
                }
            }
            catch(NumberFormatException nfe){
                System.out.println("The first and third args must be a number");
            }
        }
    }
}