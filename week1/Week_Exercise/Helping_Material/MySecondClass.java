public class MySecondClass {

    // given two value and operator, return the result]
    public void calculate (double value_1, double value_2, String operator)
    {
        double result = 0;
        switch(operator)
        {
            case "+":
                result = value_1 + value_2;
                break;
            case "*":
                result = value_1 * value_2;
                break;
            case "-":
                result = value_1 - value_2;
                break;
            case "/":
                result = value_1 / value_2;
                break;
        }
        System.out.println("Result is " + Double.toString(result))
    }           
}
