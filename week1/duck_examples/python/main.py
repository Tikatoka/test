from duck import Duck
from dabbling_duck import DabblingDuck
from diving_duck import DivingDuck
from imposter_duck import ImposterDuck


def quack(duck: Duck):
    duck.quack()


quack(Duck())
quack(DivingDuck())
quack(DabblingDuck())
quack(ImposterDuck())

