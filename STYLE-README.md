## Java Style
The file styleFile.xml is a [checkstyle](http://checkstyle.sourceforge.net/) file that defines how THG Java code should be formatted.

It largely follows the [Google Java style](https://google.github.io/styleguide/javaguide.html)

### Importing the check into IntelliJ
* File -> Settings -> Plugins -> Browse Repository -> CheckStyle-IDEA -> Install
* Restart intellij
* File -> Settings -> Checkstyle -> + 
  * Call it thg, point to the styleFile.xml
  * Tick the box to activate it
  
 At the bottom, click "CheckStyle", then the run icon to check the style
 
### Getting IntelliJ to fix your style problems
* File -> Settings -> Editor -> Code Style -> Java 
* Settings Cog -> Import Scheme -> From CheckStyle Config -> select styleFile.xml
* Now, when you reformat code (Cmd, Alt, L on MacOS), it will comply to the checkstyle config

# Now your code will look beautiful  