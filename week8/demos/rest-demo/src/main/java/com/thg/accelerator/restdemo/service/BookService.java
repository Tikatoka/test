package com.thg.accelerator.restdemo.service;

import com.thg.accelerator.restdemo.model.Book;
import com.thg.accelerator.restdemo.repository.BookRepository;
import com.thg.accelerator.restdemo.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Component
public class BookService {
  private static final Logger logger = LoggerFactory.getLogger(BookService.class);

  @Autowired
  private BookRepository bookRepository;

  @Autowired
  private Function<Book, String> fieldSearchExtractor;

  public void updateBook(Book book) throws ServiceException {
    try {
      bookRepository.updateBook(book);
    } catch (RepositoryException e) {
      logger.error("Got a repository error when updating book with id {}", book.getIsbn(), e);
      throw new ServiceException("Error while updating book with id " + book.getIsbn(), e);
    }
  }

  public boolean deleteBook(long id) throws ServiceException {
    try {
      return bookRepository.deleteBook(id);
    } catch (RepositoryException e) {
      logger.error("Got a repository error when getting book with id {}", id, e);
      throw new ServiceException("Error while getting book with id " + id, e);
    }
  }

  public Optional<Book> getBook(long id) throws ServiceException {
    try {
      System.out.println(1);
      return bookRepository.getBook(id);
    } catch (RepositoryException e) {
      logger.error("Got a repository error when getting book with id {}", id, e);
      throw new ServiceException("Error while getting book with id " + id, e);
    }
  }

  public List<Book> findBooks(String search) throws ServiceException {
    try {
      return bookRepository.findBooks(search.toLowerCase(), fieldSearchExtractor);
    } catch (RepositoryException e) {
      logger.error("Got a repository error when searching for {}", search, e);
      throw new ServiceException("Error while searching for " + search, e);
    }
  }

}
