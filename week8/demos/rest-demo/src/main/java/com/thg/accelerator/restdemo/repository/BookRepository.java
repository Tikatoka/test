package com.thg.accelerator.restdemo.repository;

import com.thg.accelerator.restdemo.model.Book;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public interface BookRepository {
  void updateBook(Book book) throws RepositoryException;
  boolean deleteBook(long isbn) throws RepositoryException;
  void deleteBooks() throws RepositoryException;
  Optional<Book> getBook(long isbn) throws RepositoryException;
  List<Book> findBooks(String search, Function<Book, String> bookFieldSearchExtractor) throws RepositoryException;
}
