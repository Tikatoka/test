package com.thg.accelerator.restdemo.service;

import com.thg.accelerator.restdemo.model.Book;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AllFieldsBookFieldSearchExtractor implements Function<Book, String> {
  @Override
  public String apply(Book book) {
    return book.getAuthor() + book.getSynopsis() + book.getTitle();
  }
}
