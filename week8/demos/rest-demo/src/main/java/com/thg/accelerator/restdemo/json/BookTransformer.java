package com.thg.accelerator.restdemo.json;

import com.thg.accelerator.restdemo.model.Book;
import org.springframework.stereotype.Component;

@Component
public class BookTransformer {
  public BookJson toJson(Book book) {
    return new BookJson(book.getIsbn(), book.getAuthor(), book.getSynopsis(), book.getTitle());
  }

  public Book fromJson(BookJson book) {
    return new Book.BookBuilder()
                .isbn(book.getId())
                .author(book.getAuthor())
                .synopsis(book.getSynopsis())
                .title(book.getTitle())
                .createBook();
  }
}
