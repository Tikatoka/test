package com.thg.accelerator.restdemo.repository;

import com.thg.accelerator.restdemo.model.Book;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class InMemoryBookRepository implements BookRepository {
  private Map<Long, Book> library;

  public InMemoryBookRepository() {
    this.library = new HashMap<>();
  }

  @Override
  public void updateBook(Book book) throws RepositoryException {
    library.put(book.getIsbn(), book);
  }

  @Override
  public boolean deleteBook(long isbn) throws RepositoryException {
    if (!library.containsKey(isbn)) {
      return false;
    } else {
      library.remove(isbn);
      return true;
    }
  }

  @Override
  public void deleteBooks() throws RepositoryException {
    library.clear();
  }

  @Override
  public Optional<Book> getBook(long isbn) throws RepositoryException {
    if (library.containsKey(isbn)) {
      return Optional.of(library.get(isbn));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public List<Book> findBooks(String search, Function<Book, String> bookFieldSearchExtractor) {
    return library.entrySet().stream().map(Map.Entry::getValue)
        .filter(book -> bookFieldSearchExtractor.apply(book).contains(search))
                  .collect(Collectors.toList());
  }
}
