package com.thg.accelerator.restdemo.json;

import java.util.Objects;

public class BookJson {
  private long id;
  private String author;
  private String synopsis;
  private String title;

  public BookJson() {
  }

  public BookJson(long id, String author, String synopsis, String title) {
    this.id = id;
    this.author = author;
    this.synopsis = synopsis;
    this.title = title;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public void setSynopsis(String synopsis) {
    this.synopsis = synopsis;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public long getId() {
    return id;
  }

  public String getAuthor() {
    return author;
  }

  public String getSynopsis() {
    return synopsis;
  }

  public String getTitle() {
    return title;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BookJson bookJson = (BookJson) o;
    return id == bookJson.id &&
        Objects.equals(author, bookJson.author) &&
        Objects.equals(synopsis, bookJson.synopsis) &&
        Objects.equals(title, bookJson.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, author, synopsis, title);
  }
}
