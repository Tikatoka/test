package com.thg.accelerator.restdemo.facade;

import com.thg.accelerator.restdemo.json.BookJson;
import com.thg.accelerator.restdemo.json.BookTransformer;
import com.thg.accelerator.restdemo.json.ErrorJson;
import com.thg.accelerator.restdemo.model.Book;
import com.thg.accelerator.restdemo.service.BookService;
import com.thg.accelerator.restdemo.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/book")
public class BookFacade {
  @Autowired
  private BookTransformer transformer;

  @Autowired
  private BookService service;

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public ResponseEntity<BookJson> getBook(@PathVariable(value="id") Long id) throws ServiceException {
    Optional<Book> book = service.getBook(id);
    if(book.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      BookJson bookJson = transformer.toJson(book.get());
      return new ResponseEntity<>( null, HttpStatus.OK);
    }
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteBook(@PathVariable(value="id") Long id) throws ServiceException {
    boolean success = service.deleteBook(id);
    if(!success) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public ResponseEntity<ErrorJson> updateBook(@PathVariable(value="id") Long id, @RequestBody BookJson bookJson) throws ServiceException {
    if(id != bookJson.getId()) {
      return new ResponseEntity<>(new ErrorJson("id in path didn't match id in book"), HttpStatus.BAD_REQUEST);
    }
    Book book = transformer.fromJson(bookJson);
    service.updateBook(book);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @RequestMapping(value = "/search/{query}", method = RequestMethod.POST)
  public ResponseEntity<List<BookJson>> search(@PathVariable(value="query") String query) throws ServiceException {
    List<Book> books = service.findBooks(query);
    List<BookJson> booksJson = books.stream().map(transformer::toJson).collect(Collectors.toList());
    return new ResponseEntity<>(booksJson, HttpStatus.OK);
  }
}
