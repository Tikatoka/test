package com.thg.accelerator.restdemo.repository;

public class RepositoryException extends Exception {
  public RepositoryException(String message) {
    super(message);
  }

  public RepositoryException(String message, Throwable cause) {
    super(message, cause);
  }
}
