#!/bin/bash
while read line;
do
	title=`echo $line | cut -d ';' -f1`
	author=`echo $line | cut -d ';' -f2`
	isbn=`echo $line | cut -d ';' -f3`
	synopsis=`echo $line | cut -d ';' -f4`
	bookJson="{\"id\": $isbn, \"author\": \"$author\", \"synopsis\":\"$synopsis\",\"title\":\"$title\"}"
	curl -XPUT -H "Content-Type: application/json"  -w "%{http_code}" localhost:8080/library/book/$isbn -d "$bookJson"
done < $1
