\documentclass{beamer}
 
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{color}
\makeatletter
\setlength\beamer@paperwidth{16.00cm}%
\setlength\beamer@paperheight{9.00cm}%
\geometry{papersize={\beamer@paperwidth,\beamer@paperheight}}
\makeatother
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\usepackage{graphicx}
\graphicspath{ {./images/} }

\lstset{ 
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
	basicstyle=\footnotesize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{mygreen},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{blue},       % keyword style
	language=Octave,                 % the language of the code
	morekeywords={*,...},            % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{mymauve},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
 
%Information to be included in the title page:
\title{Java Course}
\subtitle{Week 8, Lecture 1: REST Introduction -- The Client's View}
\author{Shaun Hall/Rehman Arshad}
\institute{THG Accelerator}
\date{2019}
 
 
 
\begin{document}
 \lstset{language=Java}
\frame{\titlepage}

\begin{frame}
\frametitle{Modularity in Software Design}
In the SDLC lectures, one of the principles conveyed was using modularity to reduce coupling.

\vspace{1cm}

This was in the context of how to structure an application, but the same principle applies when considering system architecture design -- we want to delegate responsibility of \textbf{each domain} to a \textbf{separate application}.

\end{frame}

 \begin{frame}
 \frametitle{Introducing Microservices}
The microservice architecture involves structuring an application as a collection of services that are:

\begin{itemize}
	\item Highly maintainable and testable
	\item Loosely coupled
	\item Independently deployable
	\item Organized around business capabilities.
\end{itemize}

The microservice architecture enables the continuous delivery/deployment of large, complex applications. It also enables an organization to evolve its technology stack.
\end{frame}



\begin{frame}
\frametitle{THG Microservice Architecture (excl Voyager)}
	\centering
\includegraphics[width=0.7\linewidth]{images/architecture_2018Q2_4}
\end{frame}

 \begin{frame}
\frametitle{Communicating Between Microservices}
Microservices need to communicate with other microservices. 

\vspace{1cm}

We need to use a protocol that is language agnostic -- microservices are written in the language most suited for their domain (we have a mixture of Java/Python/Golang).

\vspace{1cm}

Consider HTTP. It's a stateless request-response protocol with good support in all modern languages.

\end{frame}

\begin{frame}[fragile]
\frametitle{Using HTTP for inter-service communication - example}
\begin{lstlisting}
POST productservice.thg/products HTTP/1.1
{
	"action": "retrieve",
	"data": {
		"productId": 10530943
	}
}
\end{lstlisting}The server might respond with the following message
\begin{lstlisting}
HTTP/1.1 200 OK
{
	"success": true,
	"product" : {
		"id": 10530943,
		"title": "Impact Whey Protein",
		...	
	}
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Using HTTP for inter-service communication - example 2}
\begin{lstlisting}
POST productservice.thg/products HTTP/1.1
{
	"action": "create",
	"data": {
		"productId": 10530941,
		"title": "Whey More Impact Protein",
		...
	}
}
\end{lstlisting}The server might respond with the following message
\begin{lstlisting}
HTTP/1.1 200 OK
{
	"success": true,
	"text": "Product has been created, you're welcome."
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{The Motivation for REST}
In this example, we are using the HTTP protocol, but the HTTP verbs, status codes and message body requests/responses are arbitrarily defined by the product service.

\vspace{1cm}

With this approach, clients need to read lengthy documentation to understand how to use each service correctly.

\vspace{1cm}

Can we have a set of agreed conventions about how to use HTTP in our interfaces?
\end{frame}




\begin{frame}
\frametitle{REST}
\textbf{Re}presentational \textbf{S}tate \textbf{T}ransfer is an architectural style for designing interfaces using HTTP. Services that conform to this style are said to be \textbf{RESTful}.

\vspace{1cm}

The Richardson Maturity Model defines how RESTful your interface is; from totally un-RESTful (level 0) to reaching full REST glory (level 3). Most THG services are level 2 on this scale.
\end{frame}

\begin{frame}
\frametitle{Richardson Maturity Model}
	\centering
\includegraphics[width=0.7\linewidth]{images/richardson}

\end{frame}

\begin{frame}
\frametitle{Level 0 -- POX (Plain Old XML) Swamp}
The interface described earlier is a level 0 REST API because it uses HTTP as a tunneling mechanism for your own remote interaction mechanism. Verbs, URIs and body formats are arbitrarily used.

\end{frame}

\begin{frame}[fragile]
\frametitle{Level 1 -- Resources}
Rather than using a single URL, level 1 REST APIs use the URL to convey which entity is being operated on.

e.g. 

\begin{lstlisting}
POST productservice.thg/products/10530941 HTTP/1.1
{
	"action": "create",
		"data": {
			"title": "Whey More Impact Protein",
			...
	}
}
\end{lstlisting}

\begin{lstlisting}
POST productservice.thg/products/10530941 HTTP/1.1
{
	"action": "delete"
}
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
\frametitle{Level 2 -- HTTP Verbs}
Level 0 and 1 APIs use verbs in a non-conventional way.

Level 2 APIs use HTTP verbs as closely as possible with how they're used in HTTP itself. For example, HTTP GET has idempotent and side-effect-free semanics.

\begin{lstlisting}
PUT productservice.thg/product/10530941 HTTP/1.1
{
	"data": {
		"title": "Whey More Impact Protein",
		...
	}
}
\end{lstlisting}

\begin{lstlisting}
DELETE productservice.thg/product/10530941 HTTP/1.1
\end{lstlisting}

\end{frame}

\begin{frame}[fragile]
\frametitle{Level 2 -- Verb Semantics}
The following HTTP verbs are used in REST apis.
\begin{itemize}
\item \textbf{GET} /resource/{id} -- retrieve the resource with given id
\item \textbf{DELETE} /resource/{id} -- delete the resource with given id
\item \textbf{PUT} /resource/{id} -- create a resource with the given id, or overwrite it if it exists
\item \textbf{POST} /resource/ -- create a resource with the specified body, with the server generating an id and communicating this with the \textbf{Location} header in the response

\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Level 2 -- Status Codes}
Success
\begin{itemize}
	\item 200 OK -- success with a response body
	\item 201 CREATED -- create succeeded -- Location gives the URL of the new resource
	\item 204 NO CONTENT -- success but no response body (e.g. update operation)
\end{itemize}

Client Errors
\begin{itemize}
	\item 400 BAD REQUEST -- invalid syntax/body too large etc.
	\item 401 UNAUTHORIZED -- authentication failure
	\item 403 FORBIDDEN -- authorization failure
	
\end{itemize}

Server Errors
\begin{itemize}
	\item 500 INTERNAL SERVER ERROR
	\item 503 SERVICE UNAVAILABLE	
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Level 3 -- Hypermedia Controls}
Level 3 APIs are self documenting and allow browsing.

\begin{lstlisting}
GET productservice.thg/product/ HTTP/1.1
\end{lstlisting}

\begin{lstlisting}
{
	"products": [
		{"productId": 1, "uri": "productservice.thg/product/1",
			"verbs": ["GET", "PUT", "DELETE"]},
		{"productId": 2, "uri": "productservice.thg/product/2",
			"verbs": ["GET", "PUT", "DELETE"]},
		...
	]
}
\end{lstlisting}

\end{frame}

\begin{frame}

\frametitle{Interchange Formats}
Clients specify the format of their request with the header \textbf{Content-Type}.

\vspace{1cm}

Clients specify the format they would like to receive with \textbf{Accept}

\vspace{1cm}

A REST service can use any interchange format, but the most common are JSON and XML. Some REST services support multiple interchange formats.

\end{frame}




\end{document}