\documentclass{beamer}
 
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{color}
\makeatletter
\setlength\beamer@paperwidth{16.00cm}%
\setlength\beamer@paperheight{9.00cm}%
\geometry{papersize={\beamer@paperwidth,\beamer@paperheight}}
\makeatother
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\usepackage{graphicx}
\graphicspath{ {./images/} }

\lstset{ 
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
	basicstyle=\footnotesize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{mygreen},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{blue},       % keyword style
	language=Octave,                 % the language of the code
	morekeywords={*,...},            % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{mymauve},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
 
%Information to be included in the title page:
\title{Java Course}
\subtitle{Week 8, Lecture 2: REST Implementation with Spring Boot}
\author{Shaun Hall/Rehman Arshad}
\institute{THG Accelerator}
\date{2019}
 
 
 
\begin{document}
 \lstset{language=Java}
\frame{\titlepage}

\begin{frame}
\frametitle{Code Location}
Code examples for this lecture are \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/tree/master/week8/demos/rest-demo}{here}
\end{frame}

\begin{frame}
\frametitle{High-level Code Structure}
	\centering
\includegraphics[width=1\linewidth]{images/rest-app-structure}

\end{frame}

\begin{frame}
\frametitle{Servlet Containers}
A \emph{Java Servlet} is a component for servicing requests, normally using the HTTP protocol.

\vspace{1cm}

A \emph{Servlet Container} is responsible for the lifecycle of servlets -- despatching them when requests arrive, managing the thread pool and handling exceptions that occur during execution. These naturally introduce concurrency into the service -- \textbf{all your code needs to be thread-safe}.

\vspace{1cm}

Examples of servlet containers:
\begin{itemize}
	\item Apache Tomcat
	\item Jetty
\end{itemize}

\end{frame}

 \begin{frame}
\frametitle{Repository (Provider) Layer}
This has been covered in \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/blob/master/week6/lec3week6.pdf}{Week 6 Lecture 3}
\end{frame}

\begin{frame}
\frametitle{Service Layer}
Given a reference to the repository layer, it implements a contract that is useful for the domain.

\vspace{1cm}

Examples:
\begin{itemize}
	\item Sending an event on a middleware layer to signal to subscribers that something has changed after writing to the repository
	\item Performing multiple repository operations to achieve a high-level goal (e.g. parent-child product retrieval)
	\item Caching, where appropriate and safe
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Facade Class -- Dealing with Interchange Formats}

To make dealing with the conversion between the textual representations and Java classes easy, we use a \textbf{marshalling} library to convert between \textbf{POJOs} and their textual representations. Examples of these libraries are \emph{Jaxb} (xml) and \emph{Jackson} (JSON).

\vspace{0.5cm}

In general, the structure returned/accepted in the REST API is different from the structure we want to use internally. We also may want to version the external API separately from version of the service. For these reasons, the POJOs representing the external model are usually in a separate project. This also makes it easy for dependent services to use your service (if they run on the JVM).

\vspace{0.5cm}

A transformer is used to convert entities from the internal model to the external model and vice versa.
\end{frame}

\begin{frame}
\frametitle{Designing POJO Interface Classes}
The POJO interface classes need to be designed so that the marshalling library is able to instatiate them from their textual representations. For example, it needs to map between textual fields and class fields. 

\vspace{0.5cm}

There are two ways of helping the marshaller with this mapping:

\begin{itemize}
	\item Make sure your class field names match the textual field names and have both a default constructor and setters for each field
	\item Have a constructor with parameters labelled with the @JsonProperty/@XmlElement annotation.
\end{itemize}

Designing your POJOs to be tolerant to unknown fields (forwards-tolerant) allows the service to add new fields without breaking clients on an older version.

\end{frame}

 \begin{frame}[fragile]
\frametitle{Facade Class -- Paths}
Define the root path prefix of your service with the \textbf{server.servlet.context-path} property in application.properties.

\vspace{1cm}

Annotate your Facade classes with 

\begin{lstlisting}
@RestController
@RequestMapping("/book")
public class BookFacade
\end{lstlisting}
Annotate your methods with the relative path (including variables) and HTTP method
\begin{lstlisting}
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
\end{lstlisting}
\end{frame}
 
 \begin{frame}[fragile]
 \frametitle{Facade Class -- Methods}
 Use @PathVariable and @RequestBody to capture parameters. The unmarshalling into BookJson is performed by Jackson in this example.
 \begin{lstlisting}
@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
public ResponseEntity<ErrorJson> updateBook(@PathVariable(value="id") Long id, @RequestBody BookJson bookJson) throws ServiceException {
	if(id != bookJson.getId()) {
		return new ResponseEntity<>(new ErrorJson("id in path didn't match id in book"), HttpStatus.BAD_REQUEST);
	}
	Book book = transformer.fromJson(bookJson);
	service.updateBook(book);
	return new ResponseEntity<>(HttpStatus.NO_CONTENT);
}
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]
\frametitle{Facade Class -- Exception Handling}
Exceptions can be handled in the Facade methods individually, or to save repeated code, with an exception mapper:
\begin{lstlisting}
@ControllerAdvice
public class FacadeErrorHandler {
	
	@ExceptionHandler(ServiceException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorJson processError(ServiceException ex) {
		return new ErrorJson("There has been an error servicing your request. Please ask the development team to check the logs.");
	}
}
\end{lstlisting}
\end{frame}



 \begin{frame}[fragile]
\frametitle{REST Testing}
\begin{lstlisting}
@Test
public void testCreateBook() {
	webClient.get().uri("/library/book/1").exchange().expectStatus()
							.isNotFound().expectBody().isEmpty();
	
	BookJson bookJson = new BookJson(1, "author", "synopsis", "title");
	
	webClient.put().uri("/library/book/1")
						.contentType(MediaType.APPLICATION_JSON)
						.body(BodyInserters.fromObject(bookJson)).exchange()
						.expectStatus().isNoContent().expectBody().isEmpty();
	
	webClient.get().uri("/library/book/1").exchange().expectStatus()
						.isOk().expectBody(BookJson.class).isEqualTo(bookJson);
}
\end{lstlisting}
\end{frame}


 \begin{frame}
\frametitle{Next Lecture}
Implementing a Level 3 API and generating documentation with swagger.
\end{frame}




\end{document}