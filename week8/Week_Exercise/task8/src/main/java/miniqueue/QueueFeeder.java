package miniqueue;

public class QueueFeeder implements Runnable {
  private MiniQueue<String> queue;

  public QueueFeeder(MiniQueue<String> queue) {
    this.queue = queue;
  }

  @Override
  public void run() {
    String threadName = Thread.currentThread().getName();
    for (int i = 0; i < 20; i++) {
//      synchronized (queue){
      queue.enqueue("Created by {" + threadName + "}");
      try {
        Thread.sleep(200);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      String element = queue.dequeue();
      System.out.println("Element " + element + " dequeued from {" + threadName + "}");

    }
  }
}
