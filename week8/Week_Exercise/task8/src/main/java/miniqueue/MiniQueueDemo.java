package miniqueue;

public class MiniQueueDemo {
  public static void main(String[] args) throws InterruptedException {
    MiniQueueImplement<String> queue = new MiniQueueImplement<>();
    Thread th1 = new Thread(new QueueFeeder(queue));
    Thread th2 = new Thread(new QueueFeeder(queue));
    Thread th3 = new Thread(new QueueFeeder(queue));

    th1.start();
    th2.start();
    th3.start();

    th3.join();
    th2.join();
    th1.join();
    System.out.println("The list should has a size of 0, actual size is " + queue.size());
  }
}
