package miniqueue;

import java.util.ArrayList;

public class MiniQueueImplement<T> implements MiniQueue<T> {
  private ArrayList<T> content = new ArrayList<>();

  @Override
  public void enqueue(T o) {
    synchronized (content) {
      content.add(o);
    }
  }

  @Override
  public T dequeue() {
    synchronized (content) {
      if (content.size() == 0) {
        return null;
      }
      return content.remove(0);
    }
  }

  @Override
  public int size() {
    synchronized (content) {
      return content.size();
    }
  }

  @Override
  public boolean isEmpty() {
    synchronized (content) {
      return content.size() == 0;
    }
  }
}
