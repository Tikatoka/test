package threadpool;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class GenTwo implements Runnable {
  private String name;
  private CountDownLatch latch;
  private Semaphore semaphore;

  public GenTwo(String name, CountDownLatch latch, Semaphore semaphore) {
    this.name = name;
    this.latch = latch;
    this.semaphore = semaphore;
  }

  @Override
  public void run() {
    try {
      String thName = Thread.currentThread().getName();
      System.out.println(name + " is waiting for all GenOne tasks finished");
      latch.await();
      System.out.println(name + " is waiting for the semaphore");
      semaphore.acquire();
      System.out.println(name + " gets semaphore");

      System.out.printf("The task [%s] is being doing on thread-{%s} %n", this.name, thName);
      Thread.sleep(5000);
      semaphore.release();
      System.out.println(name + " releases semaphore");
    } catch (Exception e) {
    }
  }
}
