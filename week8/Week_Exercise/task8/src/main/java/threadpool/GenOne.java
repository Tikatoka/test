package threadpool;

import java.util.concurrent.CountDownLatch;

public class GenOne implements Runnable {
  private String name;
  private CountDownLatch latch;

  public GenOne(String name, CountDownLatch latch) {
    this.name = name;
    this.latch = latch;
  }

  @Override
  public void run() {
    try {
      String name = Thread.currentThread().getName();
      System.out.printf("The task [%s] is being doing on thread-{%s} %n", this.name, name);
      Thread.sleep(10000);
      System.out.printf("The task [%s] has finished by thread-{%s} %n", this.name, name);
      latch.countDown();
    } catch (Exception e) {
    }
  }
}
