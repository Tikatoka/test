package threadpool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ThreadPoolDemo {
  public static void main(String[] args) throws InterruptedException {
    CountDownLatch latch = new CountDownLatch(6);
    Semaphore sem = new Semaphore(2);
    List<Runnable> taskList = new ArrayList<>();
    ExecutorService pool = Executors.newFixedThreadPool(4);
    for (int i=0; i<6; i++){
      pool.submit(new GenOne("GenOne-" + (i+1), latch));
    }
    latch.await();
    for (int i=0; i<6; i++){
      pool.submit(new GenTwo("GenTwo-" + (i+1), latch, sem));
    }
    pool.shutdown();
  }
}
