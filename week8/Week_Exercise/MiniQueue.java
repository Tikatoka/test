public interface MiniQueue<T> {
  void enqueue(T t);
  T dequeue();
  int size();
  boolean isEmpty();
}
