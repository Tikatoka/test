package com.thg.rest;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

  private static final String template= "Hello World,%s";

  @GetMapping("/welcome_message")
  public String welcome(){
    return "Hello Rest";
  }

  @GetMapping("/welcome_object")
  public WelcomeMessage welcomeObject(){
    return new WelcomeMessage("Hi from Message Object");
  }

  @GetMapping("/welcome_parameters/name/{name}")
  public WelcomeMessage welcomeObjectWithParameters(@PathVariable String name){
    return new WelcomeMessage(String.format(template,name));
  }

}
