package com.thg.rest;

import java.util.Date;

public class Tasks {

  private int taskId;
  private String userName;
  private String taskDescription;
  private Date targetDate;
  private boolean taskStatus;

  public Tasks(int id, String name, String desc, Date date, boolean status) {

    taskId = id;
    userName = name;
    taskDescription = desc;
    targetDate = date;
    taskStatus = status;

  }

  public Tasks() {
  }

  public int getTaskId() {
    return taskId;
  }

  public String getUserName() {
    return userName;
  }

  public String getTaskDescription() {
    return taskDescription;
  }

  public Date getTargetDate() {
    return targetDate;
  }

  public boolean isTaskStatus() {
    return taskStatus;
  }
}
