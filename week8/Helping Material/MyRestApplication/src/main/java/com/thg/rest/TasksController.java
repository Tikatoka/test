package com.thg.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class TasksController {

  @Autowired
  private TasksService service;
//
  @RequestMapping(value = "/users/{name}/tasks", method = RequestMethod.GET)
  public List<Tasks> getTasks(@PathVariable String name){
    return service.retrieveTasks(name);

  }
//
//  @RequestMapping(value = "/users/{name}/tasks/{id}", method = RequestMethod.GET)
//  public Tasks getTaskById(@PathVariable String name, @PathVariable int id){
//    return service.retreiveOnIdAndName(id,name);
//
//  }
//
//  @RequestMapping(value = "/pi/sigma/{name}.{id}", method = RequestMethod.GET)
//  public Tasks a(@PathVariable String name, @PathVariable int id){
//    return service.retreiveOnIdAndName(id,name);
//
//  }

  @RequestMapping(value = "/users/{name}/tasks/{id}", method = RequestMethod.GET)
  public org.springframework.hateoas.Resource<Tasks> getTaskById(@PathVariable String name, @PathVariable int id){
    Tasks tasks = service.retreiveOnIdAndName(id,name);
    if (tasks==null) {
      System.out.println("throw exception here after defining it");
    }
    org.springframework.hateoas.Resource<Tasks> myTasks = new org.springframework.hateoas.Resource<Tasks>(tasks);
    ControllerLinkBuilder myLink= ControllerLinkBuilder.linkTo(methodOn(this.getClass()).getTasks(name));
    ControllerLinkBuilder myLink1= ControllerLinkBuilder.linkTo(methodOn(this.getClass()).getTaskById(name,id));
    myTasks.add(myLink.withRel("parent"));
    myTasks.add(myLink1.withSelfRel());
    myTasks.add(myLink1.withRel("I string"));
    return myTasks;

  }

//@RequestMapping(value = "/users/{name}/tasks/", method = RequestMethod.POST)
//  ResponseEntity<?> addTask (@PathVariable String name, @RequestBody Tasks tasks){
//    Tasks createTask = service.addTasks(name, tasks.getTaskDescription(), tasks.getTargetDate(),tasks.isTaskStatus());
//    if (createTask == null){
//      return ResponseEntity.noContent().build();
//    }
//  URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createTask.getTaskId()).toUri();
//    return ResponseEntity.created(location).build();
//}

}
