package com.thg.rest;

public class WelcomeMessage {

  private String message;

  public WelcomeMessage(String myMessage){
    message=myMessage;
  }

  public String getMessage() {
    return message;
  }
}
