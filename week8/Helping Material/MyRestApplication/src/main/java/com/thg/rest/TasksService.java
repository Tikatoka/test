package com.thg.rest;

import com.thg.rest.Tasks;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TasksService {

  private static List<Tasks> tasksList = new ArrayList<Tasks>();
  private static int count = 3;

  //static initialisers to save some Tasks in the memory
  static {
    tasksList.add(new Tasks(1, "Shaun", "Java Lecture", new Date(), false));
    tasksList.add(new Tasks(2, "James", " Spring Boot", new Date(), false));
    tasksList.add(new Tasks(3, "Rehman", "Irritate Shaun", new Date(), false));

  }

  public List<Tasks> retrieveTasks(String user) {
    List<Tasks> myList = new ArrayList<Tasks>();
    for (Tasks myTasks : tasksList) {
      if (myTasks.getUserName().equals(user)) {
        myList.add(myTasks);
      }
    }
    return myList;
  }

  public Tasks retreiveOnIdAndName(int id, String name) {
    for (Tasks myTask : tasksList) {
      if (myTask.getTaskId() == id && myTask.getUserName().equals(name) ) {
        return myTask;
      }
    }
    return null;
  }

  public Tasks addTasks (String name, String desc, Date targetDate, boolean status){
    Tasks task= new Tasks(++count,name, desc, targetDate, status);
    tasksList.add(task);
    return task;
  }
}
