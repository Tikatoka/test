package com.thehutgroup.accelerator.draughts;

public class PlayGame {
  public static void main(String[] args) {
    DraughtsBoard board = new DraughtsBoard();
    GameController controller = new GameController();
    controller.runBoard(board, false, false);
  }


}
