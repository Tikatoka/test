package com.thehutgroup.accelerator.draughts.piece;

import com.thehutgroup.accelerator.draughts.DraughtsBoard;
import com.thehutgroup.accelerator.draughts.Colour;
import com.thehutgroup.accelerator.draughts.Move;
import com.thehutgroup.accelerator.draughts.Position;

import java.util.Set;

public abstract class DraughtsPiece {
  private Colour colour;

  public DraughtsPiece(Colour colour) {
    this.colour = colour;
  }

  public Colour getColour() {
    return this.colour;
  }

  protected int getDeltaY(){
    int deltaY = 0;
    switch(this.getColour()){
      case WHITE:
        deltaY = 1;
        break;
      case BLACK:
        deltaY = -1;
        break;
    }
    return deltaY;
  }

  public abstract Set<Move> getPotentialMoves(Position position);
}

