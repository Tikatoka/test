package com.thehutgroup.accelerator.draughts;

import com.thehutgroup.accelerator.draughts.piece.DraughtsPiece;
import com.thehutgroup.accelerator.draughts.piece.Pawn;

public class DraughtsBoard {
  private DraughtsPiece[][] board;

  public DraughtsBoard(DraughtsPiece[][] board) {
    this.board = board;
  }

  public DraughtsBoard() {
    Pawn bp = new Pawn(Colour.BLACK);
    Pawn wp = new Pawn(Colour.WHITE);
    this.board = new DraughtsPiece[][] {
        //y=8
        {wp, null, wp, null, wp, null, wp, null},
        {null, wp, null, wp, null, wp, null, wp},
        {wp, null, wp, null, wp, null, wp, null},
        {null, null, null, null, null, null, null, null},
        {null, null, null, null, null, null, null, null},
        {null, bp, null, bp, null, bp, null, bp},
        {bp, null, bp, null, bp, null, bp, null},
        {null, bp, null, bp, null, bp, null, bp}
        //y=0,x=0                                 //x=8
    };
  }

  public DraughtsPiece[][] getBoard() {
    return this.board;
  }

  public boolean positionVacent(Position position) {
    return positionWithinBoard(position)
        && board[position.getY()][position.getX()] == null;
  }

  private boolean positionWithinBoard(Position position) {
    return position.getY() < this.getSize()
        && position.getY() >= 0
        && position.getX() >= 0
        && position.getX() < this.getSize();
  }

  public boolean positionOccupied(Colour colour, Position position) {
    return positionWithinBoard(position)
        && board[position.getY()][position.getX()] != null
        && board[position.getY()][position.getX()].getColour() == colour;
  }

  public DraughtsPiece getPiece(Position position) {
    int px = position.getX();
    int py = position.getY();
    return board[py][px];
  }

  public int getSize() {
    return this.board.length;
  }
}
