package com.thehutgroup.accelerator.draughts;

public class AIPlayer implements GamePlayer{

  private Colour colour;

  public AIPlayer (Colour colour){this.colour = colour;}


  @Override
  public Move selectPiece(DraughtsBoard board) {
    return null;
  }

  @Override
  public Position selectTargetPosition(DraughtsBoard board, Position position) {
    return null;
  }

  public Move selectTargetPosition(DraughtsBoard board, Move move) {
    return null;
  }
}
