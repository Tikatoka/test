package com.thehutgroup.accelerator.draughts;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Move extends Position {
  private Set<Position> capturedPositions = new HashSet<>();

  public Set<Position> getCapturedPositions() {
    return capturedPositions;
  }

  private Position startPosition;

  public Move(int x, int y) {
    super(x, y);
  }

  public Move(int x, int y, Position p) {
    super(x, y);
    this.startPosition = p;
  }

  public Position getStartPosition() {
    return startPosition;
  }

  public void setStartPosition(Position startPosition) {
    this.startPosition = startPosition;
  }

  public void captureOne(Position p) {
    this.capturedPositions.add(p);
  }

  public void addCaptureSet(Set<Position> captureset) {
    this.capturedPositions.addAll(captureset);
  }

  // return a new move represent from the current one to the target position
  public Move nextMove(Position targetPosition, Position capturePosition) {
    Move next = this.nextMove(targetPosition);
    // capture the position by this move
    next.captureOne(capturePosition);
    return next;
  }

  public Move nextMove(Position targetPosition) {
    // new move in to the new position
    Move next = new Move(targetPosition.getX(), targetPosition.getY());
    // remember the previous capture set
    next.addCaptureSet(this.getCapturedPositions());
    // assign the start position
    next.setStartPosition(new Position(startPosition.getX(), startPosition.getY()));
    return next;
  }

  public int getDeltaX() {
    return this.getX() - startPosition.getX();
  }

  public int getDeltaY() {
    return this.getY() - startPosition.getY();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    Move move = (Move) o;
    return this.getX() == position.getX()
        && this.getY() == position.getY()
        && this.startPosition.getX() == position.
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getX(), this.getY(),
        capturedPositions,
        this.startPosition.getX(), this.startPosition.getY());
  }
}
