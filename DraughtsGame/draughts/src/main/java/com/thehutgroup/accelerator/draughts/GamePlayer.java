package com.thehutgroup.accelerator.draughts;

import java.io.IOException;

public interface GamePlayer {
  abstract Position selectPiece (DraughtsBoard board);
  abstract Position selectTargetPosition(DraughtsBoard board, Position position) ;
}
