package com.thehutgroup.accelerator.draughts;

import com.thehutgroup.accelerator.draughts.piece.DraughtsPiece;
import com.thehutgroup.accelerator.draughts.piece.King;
import com.thehutgroup.accelerator.draughts.piece.Pawn;

import java.util.HashSet;
import java.util.Set;

public class GameController {
  private Colour turnColour;
  private int turnCount;

  GameController() {
  }

  // return 1 for white win and 2 for black win
  public int runBoard(DraughtsBoard board, boolean whiteAI, boolean blackAI) {
    DraughtsBoard gameBoard = new DraughtsBoard();
    GamePlayer whitePlayer = this.getPlayer(Colour.WHITE, whiteAI);
    GamePlayer blackPlayer = this.getPlayer(Colour.BLACK, blackAI);
    GamePlayer current = whitePlayer;
    turnColour = Colour.WHITE;
    turnCount = 0;

    while (true) {
      // print the board
      printBoard(board);
      // update the turn count
      turnCount++;

      // set the current player
      switch (turnColour) {
        case BLACK:
          current = blackPlayer;
          break;
        case WHITE:
          current = whitePlayer;
          break;
      }

      // evalue the whole borad
      evalueBoard(gameBoard);

      // search for all possible moves
      Set<Move> validMoves = new HashSet<>();
      for (int i = 0; i < gameBoard.getSize(); i++) {
        for (int j = 0; j < gameBoard.getSize(); j++) {
          if (gameBoard.getBoard()[j][i] != null) {
            Move ini = new Move(i, j);
            ini.setStartPosition(new Position(i, j));
            validMoves.addAll(searchMovesForPosition(board, ini, 99, true));
          }
        }
      }
      // check game over
      if (!gameContinueCheck(validMoves, board)) {
        break;
      }

      // print colour of this turn
      System.out.println("Turn of " + turnColour);
      // select one origin position
      Position position = current.selectPiece(board);
      // select a target position, test the selection till valid
      Position p_target;
      do {
        p_target = current.selectTargetPosition(board, position);
      } while (validTarget(p_target, position, validMoves));

      Move move = findMove(position, p_target, validMoves);
      takeMove(move, board);

      turnColour = Colour.getOther(turnColour);
    }

    // game over, the winner has the opposite color to turn colour
    switch (turnColour) {
      case BLACK:
        return 1;
      case WHITE:
        return 2;
    }
    return 0;
  }

  private GamePlayer getPlayer(Colour colour, boolean AIenable) {
    if (AIenable) {
      return new AIPlayer(colour);
    }
    return new HumanPlayer(colour);
  }


  // evalue the board and convert pawn to king
  private void evalueBoard(DraughtsBoard board) {
    DraughtsPiece[][] boardMap = board.getBoard();
    int endLine = board.getSize() - 1;
    DraughtsPiece[] whithKingLine = boardMap[endLine];
    DraughtsPiece[] blackKingLine = boardMap[0];
    for (int i = 0; i <= endLine; i++) {
      if (whithKingLine[i] != null && whithKingLine[i].getColour() == Colour.WHITE) {
        whithKingLine[i] = new King(Colour.WHITE);
      }
      if (blackKingLine[i] != null && blackKingLine[i].getColour() == Colour.BLACK) {
        blackKingLine[i] = new King(Colour.BLACK);
      }
    }
  }

  // evalue the board and return whether the game is over
  // true: game continue
  // false: game over
  private boolean gameContinueCheck(Set<Move> validMoves, DraughtsBoard board) {
    DraughtsPiece[][] boardMap = board.getBoard();
    boolean blackHasMove = false;
    boolean whiteHasMove = false;

    // check for move existance
    for (Move move : validMoves) {
      Position p = move.getStartPosition();
      DraughtsPiece piece = board.getPiece(p);
      if (piece.getColour() == Colour.WHITE) {
        whiteHasMove = true;
      }
      if (piece.getColour() == Colour.BLACK) {
        blackHasMove = true;
      }
      if (whiteHasMove && blackHasMove) {
        break;
      }
    }
    return whiteHasMove && blackHasMove;
  }


  // test whether a move is valid or not
  // return 0: not valid
  //        1: normal move
  //        2: capture
  private int validMove(Move move, DraughtsBoard board) {
    DraughtsPiece[][] boardMap = board.getBoard();
    DraughtsPiece piece = boardMap[move.getStartPosition().getY()][move.getStartPosition().getX()];
    Colour colour = piece.getColour();
    // target position is empty, normal move
    if (board.positionVacent(move)) {
      return 1;
    }
    // target position has opposite colour piece
    if (board.positionOccupied(Colour.getOther(colour), move)) {
      int deltaX = move.getDeltaX();
      int deltaY = move.getDeltaY();
      Position p = new Position(move.getX() + deltaX, move.getY() + deltaY);
      // there is empty slot behind target piece, capture the piece and valid to move behind
      if (board.positionVacent(p)) {
        return 2;
      }
    }
    return 0;
  }

  public Set<Move> searchMovesForPosition(DraughtsBoard board, Move p, int depth, boolean initialSearch) {
    Set<Move> moves = new HashSet<>();
    if (depth < 1) {
      return moves;
    }
    DraughtsPiece piece = board.getPiece(p);
    if (piece == null) return moves;
    Set<Move> potentialMoves = piece.getPotentialMoves(p);
    for (Move move : potentialMoves) {
      // normal move
      if (validMove(move, board) == 1 && initialSearch) {
        moves.add(move);
      }
      // recursive search capture move
      if (validMove(move, board) == 2) {
        Set<Position> captures = p.getCapturedPositions();
        if (!captures.contains(p)) {
          p.captureOne(move);
          int deltaX = move.getX() - p.getX();
          int deltaY = move.getY() - p.getY();
          Move next = p.nextMove(new Position(move.getX() + deltaX, move.getY() + deltaY));
          Set<Move> deepSearch = searchMovesForPosition(board, next, depth - 1, false);
          moves.add(next);
          moves.addAll(deepSearch);
        }
      }
    }
    return moves;
  }

  public Set<Move> searchMovesForPosition(DraughtsBoard board, Position p, int depth, boolean initialSearch) {
    Move move = new Move(p.getX(), p.getY(), p);
    return searchMovesForPosition(board, move, depth, initialSearch);
  }


  private boolean validTarget(Position target, Position origin, Set<Move> validMoves) {
    if (origin == target) {
      return false;
    }
    for (Move move : validMoves) {
      if (move.equals(target) && move.getStartPosition() == origin) {
        return true;
      }
    }
    return false;
  }

  private Move findMove(Position origin, Position target, Set<Move> validMoves) {
    for (Move move : validMoves) {
      if (move.equals(target) && move.getStartPosition() == origin) {
        return move;
      }
    }
    return null;
  }

  private void takeMove(Move move, DraughtsBoard board) {
    DraughtsPiece[][] boardMap = board.getBoard();
    for (Position p : move.getCapturedPositions()) {
      boardMap[p.getY()][p.getX()] = null;
    }
    DraughtsPiece temp = boardMap[move.getStartPosition().getY()][move.getStartPosition().getX()];
    boardMap[move.getStartPosition().getY()][move.getStartPosition().getX()] = null;
    boardMap[move.getY()][move.getX()] = temp;
  }

  private void printBoard(DraughtsBoard board) {
    try {
      Runtime.getRuntime().exec("clear");
    } catch (Exception e) {
      System.out.println("Why?");
    }
    int length = board.getSize();
    System.out.print("   ");
    // print the colum index
    for (int t = 0; t < length; t++) {
      System.out.print(" " + t + "  ");
    }
    System.out.print("\n  ");
    System.out.println(new String(new char[32]).replace("\0", "-"));
    for (int i = length - 1; i >= 0; i--) {
      System.out.print(" " + i + "|");
      for (int j = 0; j < length; j++) {

        DraughtsPiece piece = board.getBoard()[i][j];
        // print nul
        if (piece == null) {
          if ((i % 2 - j % 2) == 0) {
            System.out.print(" -- ");
          } else {
            System.out.print("    ");
          }
        } else {
          String pieceString = "";
          // print piece
          if (piece.getColour() == Colour.WHITE) {
            pieceString += " W";
          } else {
            pieceString += " B";
          }

          if (piece instanceof Pawn) {
            pieceString += "P ";
          } else {
            pieceString += "K ";
          }
          System.out.print(pieceString);
        }

      }
      System.out.print("\n");
      System.out.print("  |\n");
    }
  }
}
