package com.thehutgroup.accelerator.draughts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HumanPlayer implements GamePlayer {
  private Colour colour;

  public HumanPlayer (Colour colour){this.colour = colour;}

  @Override
  public Position selectPiece(DraughtsBoard board) {
    Position p;
    System.out.print("Please select one piece for move, coordinates separate by space (e.g. 1 3)");
    while (true) {
      try {
        p = this.userInput();
        if (board.getPiece(p).getColour()==colour){
          break;
        }
        System.out.print("\nYou can't move this piece, please re-select:");
      } catch (Exception e) {
        System.out.println("\nInvalid input coordinates. Try again please");
      }
    }
    return p;
  }

  @Override
  public Position selectTargetPosition(DraughtsBoard board, Position position) {
    System.out.print("Please select the target position for move:");
    Position p;
    while (true) {
      try {
        p = this.userInput();
        break;
      } catch (Exception e) {
        System.out.println("Invalid input coordinates. Try again please");
      }
    }
    return p;
  }

  private Position userInput() throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String s = br.readLine();
    String[] args = s.split(" ");
    int x = Integer.parseInt(args[0]);
    int y = Integer.parseInt(args[1]);
    Position p = new Position(x, y);
    return p;
  }
}
