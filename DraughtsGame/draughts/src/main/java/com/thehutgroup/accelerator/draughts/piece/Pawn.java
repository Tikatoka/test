package com.thehutgroup.accelerator.draughts.piece;

import com.thehutgroup.accelerator.draughts.Colour;
import com.thehutgroup.accelerator.draughts.Move;
import com.thehutgroup.accelerator.draughts.Position;

import java.util.HashSet;
import java.util.Set;

public class Pawn extends DraughtsPiece {
  public Pawn(Colour colour) {
    super(colour);
  }

  @Override
  public Set<Move> getPotentialMoves(Position position) {
    int deltaY = this.getDeltaY();
    int x = position.getX();
    int y = position.getY();
    Set<Move> potentialMoves = new HashSet<>();
    // potential moves of a pawn have 2 directions
    potentialMoves.add(moveTo(x + 1, y + deltaY, position));
    potentialMoves.add(moveTo(x - 1, y + deltaY, position));
    return potentialMoves;
  }

  protected Move moveTo(int x, int y, Position p){
    Move move = new Move(x, y);
    move.setStartPosition(p);
    return move;
  }
}
