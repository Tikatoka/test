package com.thehutgroup.accelerator.draughts;
import com.thehutgroup.accelerator.draughts.piece.DraughtsPiece;
import com.thehutgroup.accelerator.draughts.piece.King;
import com.thehutgroup.accelerator.draughts.piece.Pawn;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


public class GameControllerTest {
  private static Pawn BP = new Pawn(Colour.BLACK);
  private static Pawn WP = new Pawn(Colour.WHITE);
  private static King BK = new King(Colour.BLACK);
  private static King WK = new King(Colour.WHITE);

  @Test
  public void PawnNormalMoveOneDirection(){
    DraughtsBoard board = new DraughtsBoard();
    GameController controller = new GameController();
    Set<Move> moves = controller.searchMovesForPosition(board, new Position(0,2), 99, true);
    Set<Move> tar_moves = new HashSet<>();
    tar_moves.add(new Move(1,3, new Position(0,2)));
    assertTrue("Only one normal move", tar_moves.equals(moves));
  }

  @Test
  public void PawnNormalMoveTwoDirections(){
    DraughtsBoard board = new DraughtsBoard();
    GameController controller = new GameController();
    Set<Move> moves = controller.searchMovesForPosition(board, new Position(2,2), 99, true);
    Set<Move> tar_moves = new HashSet<>();
    tar_moves.add(new Move(1,3, new Position(2,2)));
    tar_moves.add(new Move(3,3, new Position(2,2)));
    assertTrue("Move to both directions", tar_moves.equals(moves));
  }

  @Test
  public void PawnNoMove(){
    DraughtsBoard board = new DraughtsBoard();
    GameController controller = new GameController();
    Set<Move> moves = controller.searchMovesForPosition(board, new Position(0,0), 99, true);
    Set<Move> tar_moves = new HashSet<>();
    assertTrue("White piece get stucked, no move is avaiable", tar_moves.equals(moves));
  }


  @Test
  public void PawnSingleCapture(){
    DraughtsBoard board = new DraughtsBoard(reverse(new DraughtsPiece[][] {
        //y=8
        {null, BP  , null, BP  , null, BP  , null, BP  },
        {BP  , null, BP  , null, BP  , null, BP  , null},
        {null, null, null, null, null, null, null, BP  },
        {null, null, null, null, null, null, null, null},
        {null, BP  , null, null, null, null, null, null},
        {WP  , null, WP  , null, WP  , null, WP  , null},
        {null, WP  , null, WP  , null, WP  , null, WP  },
        {WP  , null, WP  , null, WP  , null, WP  , null}
        //y=0,x=0                                 //x=8
    }));
    GameController controller = new GameController();
    Set<Move> moves = controller.searchMovesForPosition(board, new Position(0,2), 99, true);
    Set<Move> tar_moves = new HashSet<>();
    Move tar_move = new Move(2,4, new Position(0,2));
    tar_move.captureOne(new Position(1, 3));
    tar_moves.add(tar_move);
    assertTrue("Only one capture move", tar_moves.equals(moves));
  }

  private DraughtsPiece[][] reverse(DraughtsPiece[][] pieces) {
    for (int i = 0; i < pieces.length / 2; i++) {
      DraughtsPiece[] swap = pieces[i];
      pieces[i] = pieces[pieces.length - i - 1];
      pieces[pieces.length - i - 1] = swap;
    }
    return pieces;
  }
}
