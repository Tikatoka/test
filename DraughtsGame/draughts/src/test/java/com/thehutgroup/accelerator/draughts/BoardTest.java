package com.thehutgroup.accelerator.draughts;

import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class BoardTest {
  @Test
  public void vacantPosition(){
    Position p_1 = new Position(-1, 1);
    DraughtsBoard board = new DraughtsBoard();
    assertTrue("Position out of range too small", !board.positionVacent(p_1));
    Position p_2 = new Position(0, 0);
    assertTrue("Position should not vacant", !board.positionVacent(p_2));
    Position p_3 = new Position(0, 4);
    assertTrue("Position vacant", board.positionVacent(p_3));
    Position p_4 = new Position(0, 8);
    assertTrue("Position out of range too big", !board.positionVacent(p_4));
    Position p_5 = new Position(7, 7);
    assertTrue("Position occupied", !board.positionVacent(p_5));
  }

  @Test
  public void OccupiedPosition(){
    DraughtsBoard board = new DraughtsBoard();
    Position p_1 = new Position(0, 0);
    assertTrue("Position occupied by white", board.positionOccupied(Colour.WHITE, p_1));
    Position p_2 = new Position(7, 7);
    assertTrue("Position occupied by black not white", !board.positionOccupied(Colour.WHITE, p_2));
    Position p_3 = new Position(1, 3);
    assertTrue("Position is null, not occupied by white", !board.positionOccupied(Colour.WHITE, p_3));
    Position p_4 = new Position(8, 3);
    assertTrue("Position out of range, not occupied by white", !board.positionOccupied(Colour.WHITE, p_4));
  }
}
