package com.thehutgroup.accelerator.draughts;
import com.thehutgroup.accelerator.draughts.piece.King;
import com.thehutgroup.accelerator.draughts.piece.Pawn;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


public class PieceTest {
  @Test
  public void PawnTest(){
    Pawn p = new Pawn(Colour.BLACK);
    Set<Move> po_moves = p.getPotentialMoves(new Position(0, 0));
    Set<Move> tar_moves = new HashSet<>();
    tar_moves.add(new Move(-1, -1, new Position(0,0)));
    tar_moves.add(new Move(1, -1, new Position(0,0)));
    assertTrue("2 potential moves for a black pawn", tar_moves.equals(po_moves));
  }

  @Test
  public void PawnTestWrongStartPosition() {
    Pawn p = new Pawn(Colour.BLACK);
    Set<Move> po_moves = p.getPotentialMoves(new Position(0, 0));
    Set<Move> tar_moves = new HashSet<>();
    tar_moves.add(new Move(-1, -1, new Position(0, 0)));
    tar_moves.add(new Move(1, -1, new Position(0, 3)));
    assertTrue("2 potential moves for a black pawn with wrong start position", !tar_moves.equals(po_moves));
  }

  @Test
  public void KingTest(){
    Pawn p = new King(Colour.WHITE);
    Set<Move> po_moves = p.getPotentialMoves(new Position(4, 4));
    Set<Move> tar_moves = new HashSet<>();
    tar_moves.add(new Move(3, 3, new Position(4,4)));
    tar_moves.add(new Move(3, 5, new Position(4,4)));
    tar_moves.add(new Move(5, 5, new Position(4,4)));
    tar_moves.add(new Move(3, 5, new Position(4,4)));
    assertTrue("4 potential moves for a black King", tar_moves.equals(po_moves));
  }
}
