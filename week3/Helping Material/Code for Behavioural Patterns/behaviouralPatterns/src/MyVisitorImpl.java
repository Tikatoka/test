public class MyVisitorImpl implements MyVisitor {


    @Override
    public int visit(Book b) {

        if (b.getBookCost() < 10) {
            System.out.println("Delivery is Free Total Cost is " + b.getBookCost());
            return b.getBookCost();
        } else {
            System.out.println("3 £ for delivery, total cost is  " + b.getBookCost() + 3);
            return b.getBookCost() + 3;
        }


    }

    @Override
    public int visit(CD c) {
        if (c.getCdCost() < 10) {
            System.out.println("Delivery is free, total cost is  " + c.getCdCost());
            return c.getCdCost();
        } else {
            int cost = c.getCdCost() + 3;
            System.out.println("3 £ for delivery, total cost is  " + cost);
            return c.getCdCost() + 2;
        }

    }
}
