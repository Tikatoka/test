public interface MyVisitor {

    public int visit(Book b);
    public int visit(CD c);

}
