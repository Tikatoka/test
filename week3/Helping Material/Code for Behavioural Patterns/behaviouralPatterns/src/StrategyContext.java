public class StrategyContext {

    private Strategy myStrategy;

    public void setMyStratgey(Strategy myStrategy) {
        this.myStrategy = myStrategy;
    }

    //use the strategy
    public void useMyStrategy() {
        myStrategy.sortingAlgorithm();
    }
}
