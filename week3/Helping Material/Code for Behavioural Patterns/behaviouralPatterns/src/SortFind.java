public abstract class SortFind {

    //Template methods
    public final void sortFind() {
        sort();
        search();
    }

    protected abstract void sort();

    protected abstract void search();
}
