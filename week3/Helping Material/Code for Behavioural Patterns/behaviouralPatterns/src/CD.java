public class CD implements Element {

    private int cdCost;

    public CD(int cost)
    {
        this.cdCost=cost;
    }

    public int getCdCost() {
        return cdCost;
    }

    public void setCdCost(int cdCost) {
        this.cdCost = cdCost;
    }

    @Override
    public int accept(MyVisitorImpl visitor) {
        return visitor.visit(this);
    }
}
