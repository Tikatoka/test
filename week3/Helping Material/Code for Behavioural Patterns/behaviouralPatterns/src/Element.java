public interface Element {

    public int accept(MyVisitorImpl visitor);
}
