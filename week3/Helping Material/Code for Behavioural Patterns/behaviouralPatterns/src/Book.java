public class Book implements Element{

    private int bookCost;

    public Book(int cost){
        this.bookCost=cost;
    }

    public int getBookCost() {
        return bookCost;
    }

    public void setBookCost(int bookCost) {
        this.bookCost = bookCost;
    }

    @Override
    public int accept(MyVisitorImpl visitor) {
        return visitor.visit(this);
    }
}
