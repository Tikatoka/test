public class Main {

    public static void main(String[] args) {

        StrategyContext myContext = new StrategyContext();
        myContext.setMyStratgey(new BubbleSort());
        myContext.useMyStrategy();
        

    }
}

/*
        // Strategy
        StrategyContext myContext = new StrategyContext();
        myContext.setMyStratgey(new BubbleSort());
        myContext.useMyStrategy();
*/

 /*
        //Template
        SortFind obj= new BubbleBinary();
        obj.sortFind();
        SortFind obj1= new QuickLinear();
        obj1.sortFind();

          //visitor
        Element b = new Book(2);
        Element c = new CD(10);
        MyVisitor visitor = new MyVisitorImpl();
        visitor.visit((Book)b);
        visitor.visit((CD) c);

        */

  /*//Template
        SortFind obj= new BubbleBinary();
        obj.sortFind();
        SortFind obj1= new QuickLinear();
        obj1.sortFind();*/