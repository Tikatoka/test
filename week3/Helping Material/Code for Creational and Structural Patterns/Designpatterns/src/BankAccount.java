public class BankAccount {

    private double accountNo;
    private String ownerName;
    private double profitRatio;
    private double balance;
    private String branchName;

    private BankAccount()
    {

    }

    public double getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(double accountNo) {
        this.accountNo = accountNo;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public double getProfitRatio() {
        return profitRatio;
    }

    public void setProfitRatio(double profitRatio) {
        this.profitRatio = profitRatio;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public static class Builder{

        private double accountNo;
        private String ownerName;
        private double profitRatio;
        private double balance;
        private String branchName;

        public Builder(double accountNumber){
            this.accountNo=accountNumber;

        }
        public Builder withOwner(String owner)
        {
            this.ownerName=owner;
            return this;
        }
        public Builder withProfitRatio(double ratio)
        {
            this.profitRatio=ratio;
            return this;
        }
        public Builder withCurrentBalance(double balance)
        {
            this.balance=balance;
            return this;
        }
        public Builder atLocation(String branch)
        {
            this.branchName=branch;
            return this;
        }

    public BankAccount Build()

    {
        BankAccount account = new BankAccount();
        account.accountNo = this.accountNo;
        account.ownerName = this.ownerName;
        account.branchName = this.branchName;
        account.balance = this.balance;
        account.profitRatio = this.profitRatio;
        return account;
    }


    }
}
