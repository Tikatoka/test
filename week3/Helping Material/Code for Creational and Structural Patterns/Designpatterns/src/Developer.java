import java.util.List;

public class Developer extends EmployeeComponent {

    private String Name;
    private int ID;

    public Developer(String name, int id) {
        this.ID = id;
        this.Name = name;
    }


    @Override
    public void printDepartmentName() {

        System.out.println("Development");

    }
}
