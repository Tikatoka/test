public interface ComplexShapes {

    double area();

    double perimeter();

    void drawShape();
}
