public class MyAdapter implements SimpleShapes {

    private ComplexShapes adaptee;

    public MyAdapter(ComplexShapes adaptee) {
        //super();
        this.adaptee = adaptee;
    }

    @Override
    public void draw() {

        adaptee.drawShape();
    }

    @Override
    public void resize() {
        System.out.println("Add details to resize complex shapes here");
    }

    @Override
    public String description() {
        if (adaptee instanceof Triangle)
            return "triangle";
        else
            return "unknown";

    }

    @Override
    public boolean isHide() {
        return false;
    }
}
