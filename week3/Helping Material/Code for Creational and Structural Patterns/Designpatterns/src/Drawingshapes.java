import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Drawingshapes {

    List<SimpleShapes> shapes = new ArrayList<SimpleShapes>();

    public Drawingshapes() {

        //super();
    }

    public void addShape(SimpleShapes shape) {
        shapes.add(shape);
    }

    public List<SimpleShapes> getShapes() {
        return new ArrayList<SimpleShapes>(shapes);
    }

    public void draw() {
        if (shapes.isEmpty()) {
            System.out.println("Nothing to draw!");
        } else {
            shapes.stream().forEach(shape -> shape.draw());
        }
    }

    public void resize() {
        if (shapes.isEmpty()) {
            System.out.println("Nothing to resize!");
        } else {
            shapes.stream().forEach(shape -> shape.resize());
        }
    }
}
