package com.thehutgroup.accelerator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameRunnerTest {
  private Side cross = Side.Cross;
  private Side circle = Side.Circle;
  private Side empty = Side.Empty;


  @Test
  public void gameOverTest() {
    GameBoard board = new GameBoard(new Side[][] {
        {empty, empty, cross},
        {empty, empty, cross},
        {empty, empty, cross},
    });
    GameRunner runner = new GameRunner();
    GameStatusChecker checker = new GameStatusChecker();
    assertEquals("Cross Win, column line", GameResult.CrossWin, checker.gameOverCheck(board));

    board = new GameBoard(new Side[][] {
        {cross, cross, cross},
        {empty, empty, empty},
        {empty, empty, empty},
    });
    assertEquals("Cross Win, row line", GameResult.CrossWin, checker.gameOverCheck(board));

    board = new GameBoard(new Side[][] {
        {circle, circle, circle},
        {empty , empty , empty },
        {empty , empty , empty },
    });
    assertEquals("Circle Win, row line", GameResult.CircleWin, checker.gameOverCheck(board));

    board = new GameBoard(new Side[][] {
        {empty, empty, cross},
        {empty, cross, empty},
        {cross, empty, empty},
    });
    assertEquals("Cross Win, diagonal line", GameResult.CrossWin, checker.gameOverCheck(board));
    board = new GameBoard(new Side[][] {
        {empty, cross, cross},
        {empty, empty, empty},
        {cross, empty, empty},
    });
    assertEquals("No one wins, continue", GameResult.Continue, checker.gameOverCheck(board));

    board = new GameBoard(new Side[][] {
        {cross, circle, cross},
        {circle, circle, cross},
        {cross, cross, circle},
    });
    assertEquals("No one wins, drawn", GameResult.Draw, checker.gameOverCheck(board));
  }

}
