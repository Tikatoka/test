package com.thehutgroup.accelerator;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AITest {
  private Side cross = Side.Cross;
  private Side circle = Side.Circle;
  private Side empty = Side.Empty;

  @Test
  public void gameOverTest() {
    GameBoard board = new GameBoard(new Side[][] {
        {empty , empty , cross},
        {circle, circle, empty},
        {empty , empty , cross},
    });
    Player minMaxAI = new AIPlayer(Side.Cross);
    Position move = minMaxAI.makeOneMove(board);
    Position tar = new Position(2, 1);
    assertEquals(move, tar);

    board = new GameBoard(new Side[][] {
        {empty, empty, circle},
        {empty, cross, empty},
        {empty, empty, circle},
    });
    move = minMaxAI.makeOneMove(board);
    tar = new Position(2, 1);
    assertEquals(move, tar);


    board = new GameBoard(new Side[][] {
        {empty , empty , circle},
        {empty , cross , empty },
        {circle, cross , circle},
    });
    move = minMaxAI.makeOneMove(board);
    tar = new Position(1, 0);
    assertEquals(move, tar);

    board = new GameBoard(new Side[][] {
        {empty , empty , circle},
        {empty , cross , empty },
        {circle, cross , circle},
    });
    move = minMaxAI.makeOneMove(board);
    tar = new Position(1, 0);
    assertEquals(move, tar);
}

}
