package com.thehutgroup.accelerator;


public class GameBoard {
  private Side[][] boardMap;

  GameBoard(Side[][] boardMap) {
    this.boardMap = boardMap;
  }

  GameBoard() {
    Side empty = Side.Empty;
    this.boardMap = new Side[][] {
        {empty, empty, empty},
        {empty, empty, empty},
        {empty, empty, empty}
    };
  }

  public Side peekPosition(int x, int y) {
    if (x < 0 || x >= getSize() || y < 0 || y >= getSize()) {
      return null;
    }
    return this.boardMap[y][x];
  }

  public Side peekPosition(Position position) {
    return peekPosition(position.getX(), position.getY());
  }

  public int getSize() {
    return 3;
  }

  public void updateMap(Position position, Side side) {
    int px = position.getX();
    int py = position.getY();
    boardMap[py][px] = side;
  }

  public GameBoard clone() {
    Side[][] newMap = new Side[3][3];
    for (int i = 0; i < getSize(); i++) {
      for (int j = 0; j < getSize(); j++) {
        newMap[i][j] = boardMap[i][j];
      }
    }
    return new GameBoard(newMap);
  }

}
