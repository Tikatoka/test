package com.thehutgroup.accelerator;

public enum Side {
  Cross, Circle, Empty;

  public static Side getOther(Side side) {
    switch (side) {
      case Circle:
        return Cross;
      case Cross:
        return Circle;
      default:
        throw new RuntimeException("Invalid Side!");
    }
  }

  int getInt(Side side) {
    switch (side) {
      case Circle:
        return 2;
      case Cross:
        return 1;
      default:
        throw new RuntimeException("Invalid Side!");
    }
  }



}
