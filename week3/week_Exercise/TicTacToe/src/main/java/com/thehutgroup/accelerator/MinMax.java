package com.thehutgroup.accelerator;

public enum MinMax {
  Min, Max;

  public static MinMax getOther(MinMax symbol) {
    switch (symbol) {
      case Max:
        return Min;
      case Min:
        return Max;
        default:
          throw new RuntimeException("Un support min max symbol");
    }
  }
}
