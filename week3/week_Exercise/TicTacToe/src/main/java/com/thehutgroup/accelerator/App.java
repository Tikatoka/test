package com.thehutgroup.accelerator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) throws IOException {
    GameRunner runner = new GameRunner();
    System.out.println("Welcome to Tic Tac Toe");
    System.out.println("1. Start a new game");
    System.out.println("2. Continue saved game");
    System.out.print("Please select your options: ");
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String s = br.readLine();
    try {
      if (s.equals("1")) {
        runner.runGame();
      }
      if (s.equals("2")) {
        runner.restorGame();
      }
      System.out.println("Invalid Option, will start a new game anyway");
      runner.runGame();
    } catch (Exception e) {
      System.out.println("Something wrong, start a new game");
      runner.runGame();
    }


  }
}
