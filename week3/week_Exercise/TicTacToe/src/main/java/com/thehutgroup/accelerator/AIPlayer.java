package com.thehutgroup.accelerator;


public class AIPlayer implements Player {
  private Side id;

  public AIPlayer(Side side) {
    this.id = side;
  }

  @Override
  public Position makeOneMove(GameBoard board) {
    MinMaxNode node = new MinMaxNode(board.clone(), id, MinMax.Max, id);
    node.search();
    GameBoard desireBoard = node.getBest();
    for (int i = 0; i < board.getSize(); i++) {
      for (int j = 0; j < board.getSize(); j++) {
        if (desireBoard.peekPosition(i, j) != board.peekPosition(i, j)) {
          return new Position(i, j);
        }
      }
    }
    throw new RuntimeException("failed to make a move");
  }

//  private int evalueBoard(GameBoard board){
//    // evalue the board
//    // if this play wins, score 10
//    // if the other play wins, score -10
//    // if the game drawn, score 0
//    // if the game continue, and other player has two link with empty score -5
//    // if the game continue, and this player has two link with empty score 5
//
//    GameStatusChecker checker = new GameStatusChecker();
//    GameResult result = checker.gameOverCheck(board);
//    GameResult selfWin;
//    GameResult otherWin;
//    switch (id) {
//      case Cross:
//        selfWin = GameResult.CrossWin;
//        otherWin = GameResult.CircleWin;
//        break;
//      case Circle:
//        otherWin = GameResult.CrossWin;
//        selfWin = GameResult.CircleWin;
//        break;
//      default:
//        throw new RuntimeException("Invalid player side");
//    }
//    if (result == selfWin) {
//      return 10;
//    }
//    if (result == otherWin) {
//      return -10;
//    }
//
//    if (result == GameResult.Draw) {
//      return 0;
//    }
//    // game not finished, return the mid term evaluation
//    return evalueMidBoard(board);
//  }
//
//  private int evalueMidBoard(GameBoard board) {
//    int score = 0;
//    GameStatusChecker checker = new GameStatusChecker();
//    Side[][] columns = checker.getBoardColoumns(board);
//    Side[][] rows = checker.getRows(board);
//    Side[][] dias = checker.getDiagonals(board);
//    score += evalueArray(columns, id);
//    score += evalueArray(rows, id);
//    score += evalueArray(dias, id);
//    return score;
//  }
//
//  private int evalueArray(Side[][] array, Side symbol) {
//    int score = 0;
//    for (Side[] line : array) {
//      score += evalueOneLine(line, symbol);
//    }
//    return score;
//  }
//
//  private int evalueOneLine(Side[] line, Side symbol) {
//    int score = 0;
//    for (int i = 0; i < 2; i++) {
//      // two match +10
//      if (line[i] == symbol && line[i + 1] == symbol) {
//        score += 10;
//        // one match, one mismatch -5
//      } else if ((line[i] == symbol || line[i + 1] == symbol) && (line[i] != Side.Empty && line[i + 1] != Side.Empty)) {
//        score += -5;
//        // both mis match -10
//      } else if (line[i] != symbol && line[i + 1] != symbol && (line[i] != Side.Empty && line[i + 1] != Side.Empty)) {
//        score += -10;
//      }
//    }
//    return score;
//  }

}
//public class AIPlayer implements Player {
//  private Side id;
//
//  AIPlayer(Side id) {
//    this.id = id;
//  }
//
//  public Position makeOneMove(GameBoard board) {
//    int selfSymbol = id.getInt(id);
//    int otherSymbol = id.getInt(id.getOther(id));
//
//    // search a line or column or diagonal for win
//    Position position = searchSymbolTwoOccupied(board, selfSymbol);
//    if (position != null) {
//      return position;
//    }
//
//    // search a line or column or diagonal for stop the other player win
//    position = searchSymbolTwoOccupied(board, otherSymbol);
//    if (position != null) {
//      return position;
//    }
//
//    // search a line with one self symbol and empty spaces
//    position = checkSymbolPureLine(board, selfSymbol, false);
//    if (position != null) {
//      return position;
//    }
//
//    // search an empty line
//    position = checkSymbolPureLine(board, selfSymbol, true);
//    if (position != null) {
//      return position;
//    }
//
//    // search an empty space
//    for (int i = 0; i < board.getSize(); i++) {
//      for (int j = 0; j < board.getSize(); j++) {
//        if (board.peekPosition(i, j) == Side.Empty) {
//          return new Position(i, j);
//        }
//      }
//    }
//
//    throw new RuntimeException("No empty space left");
//  }
//
//  private Position searchSymbolTwoOccupied(GameBoard board, int symbol) {
//    return checkSymbol(board, symbol, 2);
//  }
//
//  private int checkIntArray(int[] arrayForCheck, int symbol, int countCheck) {
//    int count = 0;
//    int position = 99;
//    for (int i = 0; i < arrayForCheck.length; i++) {
//      if (arrayForCheck[i] == symbol) {
//        count++;
//      } else if (arrayForCheck[i] == 0) {
//        position = i;
//      }
//    }
//    if (countCheck == count) {
//      return position;
//    }
//    return 99;
//  }
//
//  private boolean checkPureIntArray(int[] arrayForCheck, int symbol, boolean enableEmpty) {
//    boolean hasSymbol = false;
//    boolean cleanLine = true;
//    for (int i = 0; i < arrayForCheck.length; i++) {
//      if (arrayForCheck[i] == symbol) {
//        hasSymbol = true;
//      } else if (arrayForCheck[i] != 0) {
//        cleanLine = false;
//      }
//    }
//    if (enableEmpty) {
//      return cleanLine;
//    }
//    return hasSymbol && cleanLine;
//  }
//
//  private Position checkSymbolPureLine(GameBoard board, int symbol, boolean enableEmpty) {
//
//    //  diagonal check 1
//    int[] di_1 = new int[] {board.peekPosition(0, 0),
//        board.peekPosition(1, 1), board.peekPosition(2, 2)};
//    boolean check = checkPureIntArray(di_1, symbol, enableEmpty);
//    if (check) {
//      for (int j = 0; j < di_1.length; j++) {
//        if (di_1[j] == 0) {
//          return new Position(j, j);
//        }
//      }
//    }
//
//    //  diagonal check
//    int[] di_2 = new int[] {board.peekPosition(2, 0),
//        board.peekPosition(1, 1), board.peekPosition(0, 2)};
//    check = checkPureIntArray(di_2, symbol, enableEmpty);
//    if (check) {
//      for (int j = 0; j < di_2.length; j++) {
//        if (di_2[j] == 0) {
//          return new Position(2 - j, j);
//        }
//      }
//    }
//
//    for (int i = 0; i < board.getSize(); i++) {
//      // row check
//      int[] row = board.getRow(i);
//      check = checkPureIntArray(row, symbol, enableEmpty);
//      if (check) {
//        for (int j = 0; j < row.length; j++) {
//          if (row[j] == 0) {
//            return new Position(j, i);
//          }
//        }
//      }
//
//      // coloumn check
//      int[] column = board.getColumn(i);
//      check = checkPureIntArray(column, symbol, enableEmpty);
//      if (check) {
//        for (int j = 0; j < column.length; j++) {
//          if (column[j] == 0) {
//            return new Position(i, j);
//          }
//        }
//      }
//    }
//
//    return null;
//  }
//
//  private Position checkSymbol(GameBoard board, int symbol, int count) {
//    //  diagonal check 1
//    int[] di_1 = new int[] {board.peekPosition(0, 0),
//        board.peekPosition(1, 1), board.peekPosition(2, 2)};
//    int check = checkIntArray(di_1, symbol, count);
//    if (check != 99) {
//      return new Position(check, check);
//    }
//
//    //  diagonal check
//    int[] di_2 = new int[] {board.peekPosition(2, 0),
//        board.peekPosition(1, 1), board.peekPosition(0, 2)};
//    check = checkIntArray(di_2, symbol, count);
//    if (check != 99) {
//      return new Position(2 - check, check);
//    }
//
//    for (int i = 0; i < board.getSize(); i++) {
//      // row check
//      int[] row = board.getRow(i);
//      check = checkIntArray(row, symbol, count);
//      if (check != 99) {
//        return new Position(check, i);
//      }
//
//      // coloumn check
//      int[] column = board.getColumn(i);
//      check = checkIntArray(column, symbol, count);
//      if (check != 99) {
//        return new Position(i, check);
//      }
//    }
//
//    return null;
//  }
//}
