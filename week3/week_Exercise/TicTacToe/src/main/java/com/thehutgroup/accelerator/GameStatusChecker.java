package com.thehutgroup.accelerator;

import java.util.ArrayList;
import java.util.List;

public class GameStatusChecker {

  public Side[][] getBoardColoumns(GameBoard board) {
    Side[][] columns = new Side[3][3];
    for (int i = 0; i < board.getSize(); i++) {
      for (int j = 0; j < board.getSize(); j++) {
        columns[i][j] = board.peekPosition(i, j);
      }
    }
    return columns;
  }

  public Side[][] getRows(GameBoard board) {
    Side[][] rows = new Side[3][3];
    for (int i = 0; i < board.getSize(); i++) {
      for (int j = 0; j < board.getSize(); j++) {
        rows[i][j] = board.peekPosition(j, i);
      }
    }
    return rows;
  }

  public Side[][] getDiagonals(GameBoard board) {
    Side[][] diagonals = new Side[2][3];
    for (int i = 0; i < board.getSize(); i++) {
      diagonals[0][i] = board.peekPosition(i, i);
      diagonals[1][i] = board.peekPosition(2 - i, i);
    }
    return diagonals;
  }

  public GameResult gameOverCheck(GameBoard board) {
    Side[][] columns = getBoardColoumns(board);
    Side[][] rows = getRows(board);
    Side[][] diagonals = getDiagonals(board);
    // check for winning
    for (int i = 0; i < board.getSize(); i++) {
      if (checkLineFullFilled(columns[i], Side.Cross)) {
        return GameResult.CrossWin;
      }
      if (checkLineFullFilled(columns[i], Side.Circle)) {
        return GameResult.CircleWin;
      }
      if (checkLineFullFilled(rows[i], Side.Cross)) {
        return GameResult.CrossWin;
      }
      if (checkLineFullFilled(rows[i], Side.Circle)) {
        return GameResult.CircleWin;
      }
    }
    for (int i = 0; i < 2; i++) {
      if (checkLineFullFilled(diagonals[i], Side.Circle)) {
        return GameResult.CircleWin;
      }
      if (checkLineFullFilled(diagonals[i], Side.Cross)) {
        return GameResult.CrossWin;
      }
    }
    // check for draw
    boolean emptySpace = false;
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (board.peekPosition(i, j) == Side.Empty) {
          emptySpace = true;
        }
      }
    }
    if (emptySpace) {
      return GameResult.Continue;
    } else {
      return GameResult.Draw;
    }
  }

  private boolean checkFilled(Side[] line, Side symbol, int countRequired) {
    int count = 0;
    for (int i = 0; i < line.length; i++) {
      if (line[i] == symbol) {
        count++;
      }
    }
    return (count == countRequired);
  }

  private boolean checkLineFullFilled(Side[] line, Side symbol) {
    return checkFilled(line, symbol, 3);
  }
}
