package com.thehutgroup.accelerator;

public enum GameResult {
  CrossWin, CircleWin, Draw, Abort, Continue
}
