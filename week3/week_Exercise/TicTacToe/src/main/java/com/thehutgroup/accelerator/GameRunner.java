package com.thehutgroup.accelerator;

import java.io.*;

public class GameRunner {
  private int crossWin;
  private int circleWin;
  private Side turn;

  public void runGame() throws IOException {
    GameBoard board = new GameBoard();
    runGame(board, Side.Cross);
  }

  public void runGame(GameBoard board, Side turn) throws IOException {
    this.turn = turn;
    boolean gameContinue = true;
    GameResult result;
    while (gameContinue) {
      result = runBoard(board, turn);
      switch (result) {
        case CrossWin:
          crossWin++;
          break;
        case CircleWin:
          circleWin++;
          break;
        case Abort:
          gameContinue = false;
          break;
      }
    }
  }

  public GameResult runBoard(GameBoard board, Side current) throws IOException {
    GameStatusChecker statusChecker = new GameStatusChecker();
    Player crossPlayer = new HumanPlayer(Side.Cross);
    // Player circlePlayer = new AIPlayer(Side.Circle);
    Player circlePlayer = new AIPlayer(Side.Circle);
    Player turnPlayer = crossPlayer;
    Position selectPosition;
    Side currentTurn = current;
    GameResult gameResult;
    String gameFinishMessage = "";
    do {
      System.out.print("Turn:" + currentTurn + "    ");
      System.out.print("Score Cross-Circle: " + crossWin + "-" + circleWin + "\n");
      Runtime.getRuntime().exec("clear");

      // find the play for this turn
      switch (currentTurn) {
        case Cross:
          turnPlayer = crossPlayer;
          break;
        case Circle:
          turnPlayer = circlePlayer;
          break;
      }
      // present the board
      printBoardMap(board);

      // player select a move, keep looping as long as the target is not empty
      do {
        System.out.print("\nPlease select one position for your move:");
        selectPosition = turnPlayer.makeOneMove(board);

        // save the current game into file
        if (selectPosition.getY() == -1 && selectPosition.getX() == -1) {
          saveGame(board, currentTurn, crossWin, circleWin);
        }
      }
      while (board.peekPosition(selectPosition) != Side.Empty);

      //update board, take the move
      takeMove(board, selectPosition, currentTurn);

      // change turn
      currentTurn = currentTurn.getOther(currentTurn);
    } while (statusChecker.gameOverCheck(board) == GameResult.Continue);

    // print the game message
    gameResult = statusChecker.gameOverCheck(board);
    switch (gameResult) {
      case CrossWin:
        gameFinishMessage = "Cross win";
        break;
      case CircleWin:
        gameFinishMessage = "Circle win";
        break;
      case Draw:
        gameFinishMessage = "Drawn";
        break;
      default:
        throw new RuntimeException("Game should not finished");
    }
    System.out.println("\nGame finished: " + gameFinishMessage);
    return gameResult;
  }

  private void takeMove(GameBoard board, Position move, Side turn) {
    board.updateMap(move, turn);
  }

  public void saveGame(GameBoard board, Side current, int crossWin, int circleWin) {
    String map = "";
    switch (current) {
      case Circle:
        map += 2;
        break;
      case Cross:
        map += 1;
        break;
      default:
        throw new RuntimeException("The game has invalid turn");
    }
    // convert the map
    for (int y = 0; y < board.getSize(); y++) {
      for (int x = 0; x < board.getSize(); x++) {
        map += getIntFromSide(board.peekPosition(x, y));
      }
    }
    String result = crossWin + ";" + circleWin;

    String fileName = "gameSave";

    try {
      BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
      out.write(map);
      out.newLine();
      out.write(result);
      out.close();
    } catch (IOException excep) {
      excep.printStackTrace();
    }
  }

  public void restorGame() {
    String fileName = "gameSave";
    String[] line;
    try {
      BufferedReader in = new BufferedReader(new FileReader(fileName));
      line = in.readLine().split("");
      Side[][] boardMap = {
          {
              getSideFromString(line[1]), getSideFromString(line[2]), getSideFromString(line[3])
          },
          {
            getSideFromString(line[4]), getSideFromString(line[5]), getSideFromString(line[6])
          },
          {
            getSideFromString(line[7]), getSideFromString(line[8]), getSideFromString(line[9])
          },
      };
      Side turn = getSideFromString(line[0]);
      GameBoard board = new GameBoard(boardMap);
      line = in.readLine().split(";");
      crossWin = Integer.parseInt(line[0]);
      circleWin = Integer.parseInt(line[1]);
      in.close();
      runGame(board, turn);
    } catch (IOException excep) {
      excep.printStackTrace();
    }
  }

  public void printBoardMap(GameBoard board) {
    for (int i = 0; i < board.getSize(); i++) {
      System.out.print("  " + i + " ");
    }
    System.out.print("\n");
    for (int y = 0; y < board.getSize(); y++) {
      System.out.print(y);
      for (int x = 0; x < board.getSize(); x++) {
        Side slot = board.peekPosition(x, y);
        String slotSymbol;
        switch (slot) {
          case Empty:
            slotSymbol = "   ";
            break;
          case Cross:
            slotSymbol = " X ";
            break;
          case Circle:
            slotSymbol = " O ";
            break;
          default:
            throw new RuntimeException("Invalid board");
        }
        System.out.print(slotSymbol);
        if (x < 2) {
          System.out.print("|");
        }
      }
      if (y < 2) {
        System.out.print("\n" + "  --  --  --\n");
      }
    }
  }

  private Side getSideFromString(String intSymbol) {
    switch (intSymbol) {
      case "1":
        return Side.Cross;
      case "2":
        return Side.Circle;
      case "0":
        return Side.Empty;
      default:
        throw new RuntimeException("Invalid int for board");
    }
  }

  private int getIntFromSide(Side sideSymbol) {
    switch (sideSymbol) {
      case Circle:
        return 2;
      case Cross:
        return 1;
      case Empty:
        return 0;
      default:
        throw new RuntimeException("Invalid side symbol for board");
    }
  }
}
