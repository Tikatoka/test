package com.thehutgroup.accelerator;

public interface Player {
  public Position makeOneMove(GameBoard board);
}
