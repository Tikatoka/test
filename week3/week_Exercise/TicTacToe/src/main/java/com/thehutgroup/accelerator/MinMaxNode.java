package com.thehutgroup.accelerator;

import java.lang.reflect.Array;
import java.util.*;

public class MinMaxNode {
  private int score = 0;
  private GameBoard board;
  private List<MinMaxNode> childStates = new LinkedList<>();
  private Side side;
  private MinMax type;
  private Side maxSide;

  public MinMaxNode(GameBoard board, Side side, MinMax symbol, Side maxSide) {
    this.board = board;
    this.side = side;
    this.type = symbol;
    this.maxSide = maxSide;
  }

  public void search() {
    // the game is finished at this stage. Do not search further
    GameStatusChecker checker = new GameStatusChecker();
    if (checker.gameOverCheck(board) != GameResult.Continue) {
      return;
    }
    Set<Position> emptyPositions = findEmptyPosition();
    for (Position pos : emptyPositions) {
      GameBoard newBoard = updataBoardReturnNewBoard(pos, side);
      MinMaxNode oneChildState = new MinMaxNode(newBoard, Side.getOther(side), MinMax.getOther(type), maxSide);
      oneChildState.search();
      childStates.add(oneChildState);
    }
    this.evalue();
  }

  public GameBoard getBest() {
    for (MinMaxNode node : childStates) {
      if (node.score == this.score) {
        return node.board;
      }
    }
    throw new RuntimeException("try to get result before evalue");
  }

  private GameBoard updataBoardReturnNewBoard(Position position, Side symbol) {
    GameBoard newBoard = board.clone();
    newBoard.updateMap(position, symbol);
    return newBoard;
  }

  private Set<Position> findEmptyPosition() {
    Set<Position> positions = new HashSet<>();
    for (int i = 0; i < board.getSize(); i++) {
      for (int j = 0; j < board.getSize(); j++) {
        if (board.peekPosition(i, j) == Side.Empty) {
          positions.add(new Position(i, j));
        }
      }
    }
    return positions;
  }

  private MinMaxResult evalue() {
    // evalue the whole tree and return the board with most score
    GameStatusChecker checker = new GameStatusChecker();
    GameResult selfWin;
    GameResult otherWin;
    switch (maxSide) {
      case Circle:
        selfWin = GameResult.CircleWin;
        otherWin = GameResult.CrossWin;
        break;
      case Cross:
        selfWin = GameResult.CrossWin;
        otherWin = GameResult.CircleWin;
        break;
      default:
        throw new RuntimeException("Invalid side");
    }
    if (!childStates.isEmpty()) {
      List<Integer> scores = new ArrayList<>();
      int childScore;
      for (MinMaxNode node : childStates) {
        MinMaxResult result = node.evalue();
        scores.add(result.getScore());
      }
      switch (type) {
        case Max:
          childScore = Collections.max(scores);
          break;
        case Min:
          childScore = Collections.min(scores);
          break;
        default:
          throw new RuntimeException("Invalid type");


      }
      // there are children states, evalue the children state
//      int childScore = -10;
//      //GameBoard desireBoard = new GameBoard();
//      MinMaxResult result;
//      for (MinMaxNode node : childStates) {
//        switch (type) {
//          case Max:
//            result = node.evalue();
//            if (result.getScore() >= childScore) {
//              childScore = result.getScore();
//              //desireBoard = result.getBoard();
//            }
//            break;
//          case Min:
//            result = node.evalue();
//            if (result.getScore() <= childScore) {
//              childScore = result.getScore();
//              desireBoard = result.getBoard();
//            }
//            break;
//          default:
//            throw new RuntimeException("should not be here");
//        }
//      }
      this.score = childScore;
      return new MinMaxResult(board, score);
    } else {
      // self win
      if (checker.gameOverCheck(board) == selfWin) {
        this.score = 10;
        return new MinMaxResult(board.clone(), 10);
      }

      // self win
      if (checker.gameOverCheck(board) == otherWin) {
        this.score = -10;
        return new MinMaxResult(board.clone(), -10);
      }

      // draw game
      if (checker.gameOverCheck(board) == GameResult.Draw) {
        this.score = 0;
        return new MinMaxResult(board.clone(), 0);
      }
      // should not go there
      throw new RuntimeException("Search failed");
    }
  }
}
