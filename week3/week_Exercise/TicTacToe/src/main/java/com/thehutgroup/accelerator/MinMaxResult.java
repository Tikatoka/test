package com.thehutgroup.accelerator;

public class MinMaxResult {
  private GameBoard board;

  public GameBoard getBoard() {
    return board;
  }

  public int getScore() {
    return score;
  }

  private int score;

  MinMaxResult(GameBoard board, int score) {
    this.board = board;
    this.score = score;
  }
}
