package com.thehutgroup.accelerator;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class HumanPlayer implements Player {
  private Side id;

  HumanPlayer(Side id) {
    this.id = id;
  }

  @Override
  public Position makeOneMove(GameBoard board) {
    do {
      try {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        String[] choice = s.split(" ");
        int x = Integer.parseInt(choice[0]);
        int y = Integer.parseInt(choice[1]);
        return new Position(x, y);
      } catch (Exception e) {
        System.out.print("Invalid input! Please try again:");
      }
    } while (true);
  }

}
