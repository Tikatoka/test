package com.thehutgroup.accelerator;

public class DomesticCar extends CarClass {
  DomesticCar(int cNumber, double enginePower) {
    super(cNumber, enginePower);
    this.setCarType("Domestic Car");
  }

  public double taxCalculation(double power, String carType, int chasisInfor) {
    if (power <= 1.5) {
      return 20;
    }
    return 90;
  }
}
