package com.thehutgroup.accelerator;

public class SportsCar extends CarClass {
  SportsCar(int cNumber, double enginePower) {
    super(cNumber, enginePower);
    this.setCarType("Sports Car");
  }

  public double taxCalculation(double power, String carType, int chasisInfor) {
    if (power <= 1.5) {
      return 150;
    }
    return 250;
  }
}
