package com.thehutgroup.accelerator;

public interface Car {
  void setBodyNumber(int chasisdouNumber);

  int getBodyNumber();

  void setCarType(String type);

  String getCarType();

  double getEnginePower();

  void setEnginePower(double enginePower);
}
