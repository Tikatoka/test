package com.thehutgroup.accelerator;

public class BusinessCar extends CarClass {
  BusinessCar(int cNumber, double enginePower) {
    super(cNumber, enginePower);
    this.setCarType("Business Car");
  }

  public double taxCalculation(double power, String carType, int chasisInfor) {
    if (power <= 1.5) {
      return 200;
    }
    return 250;
  }
}
