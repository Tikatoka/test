package com.thehutgroup.accelerator;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {
    DomesticCar d_car = new DomesticCar(1, 3);
    d_car.printInfor();

    BusinessCar b_car = new BusinessCar(1, 2);
    b_car.printInfor();

    SportsCar c_car = new SportsCar(1, 1);
    c_car.printInfor();

  }

}
