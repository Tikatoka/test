package com.thehutgroup.accelerator;

public interface Taxation extends Car {
  double taxCalculation(double power, String carType, int chasisInfor);
}
