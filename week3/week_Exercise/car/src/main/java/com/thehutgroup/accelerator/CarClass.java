package com.thehutgroup.accelerator;

abstract class CarClass implements Car, Taxation {
  private int chasisdouNumber;
  private String carType;
  private double enginePower;

  CarClass(int cNumber, double enginePower) {
    this.chasisdouNumber = cNumber;
    this.enginePower = enginePower;
  }

  public int getBodyNumber() {
    return chasisdouNumber;
  }

  public void setBodyNumber(int chasisdouNumber) {
    this.chasisdouNumber = chasisdouNumber;
  }

  public String getCarType() {
    return carType;
  }

  public void setCarType(String carType) {
    this.carType = carType;
  }

  public double getEnginePower() {
    return enginePower;
  }

  public void setEnginePower(double enginePower) {
    this.enginePower = enginePower;
  }

  public double getTax() {
    double power = this.getEnginePower();
    String type = this.getCarType();
    int chasis = this.getBodyNumber();
    return this.taxCalculation(power, type, chasis);
  }

  public void printInfor() {
    double tax = this.getTax();
    System.out.println("This is a " + carType + " car with the following specs:"
        + "\nChasis Number: " + chasisdouNumber
        + "\nEngine Power: " + enginePower +
        "\nTax per year: " + tax);
  }

}
